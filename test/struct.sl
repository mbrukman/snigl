struct: Basic _ ('id Str) {
  Basic new put: ('.id :: "foo") let: 'b1
  @b1 call @b1 test==

  Basic new put: ('.id :: "foo") let: 'b2
  @b1 @b2 test=

  @b2 _put: ('.id :: "bar")
  @b1 @b2 test!=
  @b1 @b2 > test

  @b2 _clone .id _pop
  @b2 .id "bar" test=
}

struct: Derived Basic ('val) {
  let: (%(Derived new) 'd)
  35 "foo" @d _put: ('.id %(7 +) 'val)
  @d .id "foo" test=
  @d .val 42 test=
}

struct: Custom _ ('id Str) {
  Custom new put: ('.id :: "foo") let: 'c1
  func: call Custom Int 42
  @c1 call 42 test=

  Custom new put: ('.id :: "bar") let: 'c2

  func: = (Custom Custom) Int 42
  @c1 @c2 = 42 test=

  func: clone Custom Int 42
  @c1 clone 42 test=
}

struct: Foobar Seq _
func: iter Foobar Iter (drop "foobar" iter)
Foobar new #, join "f,o,o,b,a,r" test=