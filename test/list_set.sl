nil new-list-set
dup 2 _add dup 3 _add dup 1 _add
[1 2 3] test=

nil new-list-set
dup "foo" _add dup "bar" _add dup "baz" _add dup "baz" _remove
["bar" "foo"] test=

{
  &first new-list-set let: 'm
  @m 2 :: 'b _add
  @m 1 :: 'a _add
  @m 3 :: 'c _add
  @m 2 find 2 :: 'b test=
  @m 4 find nil test=
  @m [1::'a 2::'b 3::'c] test=
} 

{
  nil new-list-set let: 's
  @s 1 _add
  @s 3 _add
  @s 4 _add
  @s 2 find-iter not test
  2 insert
  @s [1 2 3 4] test=
} 

{
  nil new-list-set let: 's
  @s 1 _add
  @s 3 _add
  @s 4 _add
  @s 3 find-iter test
  2 insert
  @s [1 2 3 4] test=
}