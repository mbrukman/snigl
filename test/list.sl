[1 2 3] [1 2 2] test!=
[1 2 3] [1 2 3] test=

[1 2 3] dup test==
[1 2 3] clone test!==

[1 2] [1 2] < not test
[1 2] [1 2] <= test
[1 2 3] [1 2 2] > test

[] peek nil test=
[] dup 42 push pop 42 test=

[1 2] dup pop swap pop + 3 test=
[1 2] dup peek swap peek + 4 test=
[1 2 3] clone _pop len 3 test=
[1 2 3] dup list _pop len 3 test=
7 list len 7 test=
[1 2 3] dup iter dup _next dup _next dup _delete 42 insert [1 42 3] test=
41 ['foo %(++) 'bar] ['foo 42 'bar] test=
42 ['foo [%_ 'bar %%_].. 'baz] ['foo 'bar 42 'baz] test=