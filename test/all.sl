debug

"Source: `%(source: (1 2 +))`" "Source: `1 2 +`" test=
"`%(source:, 1 2 +) %_ =`" "`1 2 + 3 =`" test=

1 2 + 3 test=
1, 2 + 3 test=
(1 2) + 3 test=
{1 2} + 3 test=

0 if-else: 1 2 2 test=
1 if-else: 2 3 2 test=

1 3 min 1 test=
3 1 min 1 test=
1 3 max 3 test=
3 1 max 3 test=

1 2 &+ call 3 test=
&(42) call 42 test=
&{42} call 42 test=

'foo type Sym test=

['foo _: 'bar 'baz] ['foo 'baz] test=

0 7 for: + 21 test=
0 3 times: ++ 3 test=
0 while: (++ dup 42 <) 42 test=

{1 2 3 let: ('c 'b 'a) @a @b + @c -} 0 test=
{42 let: 'foo &{@foo}} call 42 test=
{2 let: ('foo :: 1 'bar 'baz :: 3) [@foo @bar @baz]} [1 2 3] test=
{41 let: (%(++) 'foo) @foo} 42 test=

{&let: 'bar 42 swap call @bar} 42 test=
{42 let: 'y &{or: @y}} f swap call 42 test=

{
  &(35 7 +) let: 'y
  &{or:, @y call}
} f swap call 42 test=

{
  &(35 @z +) let: 'y
  &{7 let: 'z or:, @y call}
} f swap call 42 test=

try: _ nil test=
try: (42 throw) catch 42 test=

(pre:, 1 2 +) 3 test=

flush-stdout
'foo say "bar" say 42 say stdout-str "foo\nbar\n42\n" test=

flush-stdout
'foo say
new-buf stdout: ('bar say stdout-str "bar" test=)
42 say
stdout-str "foo\n42" test=

flush-stdout
'foo say
new-buf stdout: ('bar say flush-stdout)
42 say
stdout-str "foo\nbar\n42" test=

[{
  'before
  defer: 'defer1
  defer: 'defer2
  'after
}] ['before 'after 'defer2 'defer1] test=

"1 2 +" eval 3 test=

include: (
  "cond.sl" "doc.sl"
  "fix.sl" "func.sl"
  "hash_map.sl"
  "list.sl" "list_set.sl"
  "pair.sl" "ref.sl"
  "stack.sl" "str.sl" "struct.sl"
  "type.sl"
)

nil say