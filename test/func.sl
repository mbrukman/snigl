func: thirtyfive () Int 35
7 thirtyfive + 42 test=

func: add-thirtyfive Int Int (35 +)
7 add-thirtyfive 42 test=

func: my-add (Int Int) Int +
7 35 my-add 42 test=

func: is-42? 42 Bool t
func: is-42? T Bool f
41 is-42? not test
42 is-42? test

func: is-3? (1 2 +) Bool t
func: is-3? (3 type) Bool f
1 is-3? not test
3 is-3? test

func: fib-rec (0) Int _
func: fib-rec (1) Int _
func: fib-rec (Int) Int (-- dup fib-rec swap -- fib-rec +)
20 fib-rec 6765 test=

func: fib-tail (0 Int Int) Int (rotr drop2)
func: fib-tail (1 Int Int) Int (rotl drop2)
func: fib-tail (Int Int Int) Int (rotr -- rotl dup rotl + recall: fib-tail)
20 0 1 fib-tail 6765 test=

func: iff (A A A) _ (drop2 call)
func: iff (A A f) _ (rotr drop2 call)
func: if (A A A) _ (rotr bool recall: iff)
t 1 2 if 1 test=
f 1 &(2) if 2 test=

{42 let: 'foo func: closure () Int {@foo}}
closure 42 test=

&(35 +) 6 curry: %(++) call 42 test=