42 ref get-set: dup 42 test=
42 ref 42 ref test=
42 ref get 42 test=
42 ref dup get-set: ++ 43 ref test=

{
  &0.0 clone let: ('c 'f)
  @c &(@f @c get 1.8 * 32.0 + set t) watch
  @f &(@c @f get 32.0 - 1.8 / set t) watch

  @c 30.0 set
  @f get 86.0 test=

  @f 100.0 set
  @c get 37.7 test=
}

{
  let: ('net :: &0.0 'gross :: &0.0 'tax-rate :: &0.0)
  func: get-gross()(Fix) (@net get dup @tax-rate get * +)
  &(@gross get-gross set t) watch: (get-gross drop)
  
  @net 100.0 set
  @tax-rate .2 set
  @gross get 120.0 test=

  @net 200.0 set
  @gross get 240.0 test=

  @tax-rate .3 set
  @gross get 260.0 test=
}
