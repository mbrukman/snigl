"foo" and: 42 42 test=
"" and: 42 nil test=

"foo" or: 42 "foo" test=
"" or: 42 42 test=

[1 t if: 2] [1 2] test=
[1 f if: 2] [1] test=

[1 t else: 2] [1] test=
[1 f else: 2] [1 2] test=

t if-else: 1 2 1 test=
f if-else: 1 2 2 test=
