func: iff (A A A) _ (
  drop2 call
)

func: iff (A A f) _ (
  rotr drop2 call
)

func: if (A A A) _ (
  rotr bool recall: iff
)

10 bench: (100000 times:,
  42 &_ &('fail throw) if
) say

10 bench: (100000 times:,
  42 else: ('fail throw)
) say