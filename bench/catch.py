from bench import bench

print(bench(10, '''
def test():
  try:
    raise Exception(42)
  except Exception as e:
    return e
''', '''
for _ in range(100000):
  test()
'''))
