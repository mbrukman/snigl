struct: Foo _ ('bar 'baz 'qux Int)

10 bench: (100000 times:,
  Foo new
  put: ('.Foo/bar :: 40)
  dup .Foo/bar ++ swap put: '.Foo/baz
  dup .Foo/baz ++ swap _put: '.Foo/qux
) say