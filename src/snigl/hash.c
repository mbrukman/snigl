#include <assert.h>
#include <stdlib.h>

#include "snigl/pool.h"
#include "snigl/sgl.h"
#include "snigl/hash.h"
#include "snigl/util.h"

struct sgl_hash *sgl_hash_init(struct sgl_hash *h) {
  h->nslots = h->len = 0;
  h->slots = NULL;
  h->full = false;

  h->fn = NULL;
  h->is = NULL;
  return h;
}

struct sgl_hash *sgl_hash_deinit(struct sgl_hash *h, struct sgl *sgl) {
  struct sgl_ls *ss = h->slots;
  if (!ss) { return h; }
  
  for (struct sgl_ls *sp = ss; sp < ss + h->nslots; sp++) {
    sgl_ls_do(sp, struct sgl_hash_node, ls, n) {
      sgl_free(&sgl->hash_node_pool, n);
    }
  }
  
  if (h->nslots == SGL_MIN_HASH_SLOTS) {
    sgl_free(&sgl->hash_slots_pool, h->slots);
  } else {
    free(h->slots);
  }

  return h;
}

bool sgl_hash_init_slots(struct sgl_hash *h, struct sgl *sgl) {
  struct sgl_ls *ss = h->slots;
  if (ss && !h->full) { return false; }

  struct sgl_ls *prev = h->slots;
  sgl_int_t prev_nslots = h->nslots, ns = prev_nslots;
  
  if (ns) {
    ns = h->nslots = ns * 3;
    ss = h->slots = malloc(sizeof(struct sgl_ls) * ns);
  } else {
    ns = h->nslots = SGL_MIN_HASH_SLOTS;
    ss = h->slots = sgl_malloc(&sgl->hash_slots_pool);
  }

  for (struct sgl_ls *sp = ss; sp < ss + ns; sp++) { sgl_ls_init(sp); }
  if (!prev) { return true; }
  
  for (struct sgl_ls *s = prev; s < prev + prev_nslots; s++) {
    sgl_ls_do(s, struct sgl_hash_node, ls, n) {
      sgl_ls_insert(h->slots + n->hkey % ns, &n->ls);
    }
  }
  
  if (prev_nslots == SGL_MIN_HASH_SLOTS) {
    sgl_free(&sgl->hash_slots_pool, prev);
  } else {
    free(prev);
  }
  
  h->full = false;
  return false;
}

sgl_uint_t sgl_hash_key(struct sgl_hash *h, void *key) {
  return h->fn ? h->fn(key) : (sgl_uint_t)key;
}

struct sgl_ls *sgl_hash_slot(struct sgl_hash *h, sgl_uint_t hkey) {
  struct sgl_ls *ss = h->slots;
  return ss ? ss + hkey % h->nslots : NULL;
}

struct sgl_hash_node *sgl_hash_find(struct sgl_hash *h,
                                    struct sgl_ls *slot,
                                    sgl_uint_t hkey,
                                    void *key) {
  sgl_int_t niter = 1;
  struct sgl_hash_node *node = NULL;
    
  sgl_ls_do(slot, struct sgl_hash_node, ls, n) {
    if (h->is ? (hkey == n->hkey && h->is(n->key, key)) : key == n->key) {
      node = n;
      break;
    }

    ++niter;
  }

  if (niter > SGL_MAX_HASH_ITER) { h->full = true; }
  return node;
}

struct sgl_hash_node *sgl_hash_add(struct sgl_hash *h,
                                   struct sgl *sgl,
                                   struct sgl_ls *slot,
                                   sgl_uint_t hkey,
                                   void *key,
                                   void *val) {
  struct sgl_hash_node *n = sgl_malloc(&sgl->hash_node_pool);
  sgl_ls_insert(slot, &n->ls);
  n->hkey = hkey;
  n->key = key;
  n->val = val;
  h->len++;
  return n;
}


