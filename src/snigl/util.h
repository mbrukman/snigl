#ifndef SNIGL_UTIL_H
#define SNIGL_UTIL_H

#include <stdbool.h>
#include <stddef.h>

#include "snigl/int.h"

#define _SGL_CID(x, y)                          \
  x ## y                                        \
  
#define SGL_CID(x, y)                           \
  _SGL_CID(x, y)                                \
  
#define SGL_CUID(prefix)                        \
  SGL_CID(prefix, __COUNTER__)                  \

#define sgl_baseof(type, field, ptr)                          \
  ((type *)((unsigned char *)(ptr) - offsetof(type, field)))  \

sgl_int_t sgl_abs(sgl_int_t x);
sgl_int_t sgl_min(sgl_int_t x, sgl_int_t y);
sgl_int_t sgl_max(sgl_int_t x, sgl_int_t y);
sgl_int_t sgl_pow(sgl_int_t x, sgl_int_t exp);

char *sgl_strndup(const char *in, sgl_int_t len);
char *sgl_strdup(const char *in);
sgl_uint_t sgl_hash_cs(const char *in);
sgl_uint_t sgl_hash_ncs(const char *in, sgl_int_t len);
char *sgl_sprintf(const char *spec, ...);

#endif
