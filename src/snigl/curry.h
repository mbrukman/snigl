#ifndef SNIGL_CURRY_H
#define SNIGL_CURRY_H

#include "snigl/ls.h"

struct sgl;
struct sgl_list;
struct sgl_val;

struct sgl_curry {
  struct sgl_ls ls;
  struct sgl_val *target;
  struct sgl_list *args;
  sgl_int_t nrefs;
};

struct sgl_curry *sgl_curry_new(struct sgl *sgl,
                                struct sgl_val *target,
                                struct sgl_list *args);

void sgl_curry_deref(struct sgl_curry *c, struct sgl *sgl);

#endif
