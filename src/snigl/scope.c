#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "snigl/pool.h"
#include "snigl/scope.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"

struct sgl_scope *sgl_scope_new(struct sgl *sgl, struct sgl_scope *parent) {
  return sgl_scope_init(sgl_malloc(&sgl->scope_pool), parent);
}

struct sgl_scope *sgl_scope_init(struct sgl_scope *s, struct sgl_scope *parent) {
  s->parent = parent;
  if (parent) { parent->nrefs++; }
  sgl_hash_init(&s->vars);
  sgl_ls_init(&s->defers);
  s->nrefs = 1;
  return s;
}

struct sgl_scope *sgl_scope_deinit(struct sgl_scope *s, struct sgl *sgl) {
  sgl_ls_do(&s->defers, struct sgl_val, ls, v) { sgl_val_free(v, sgl); }
  if (s->parent) { sgl_scope_deref(s->parent, sgl); }

  sgl_hash_slots_do(&s->vars, hs) {
    sgl_ls_do(hs, struct sgl_hash_node, ls, n) { sgl_val_free(n->val, sgl); }
  }
  
  sgl_hash_deinit(&s->vars, sgl);
  return s;
}

void sgl_scope_free(struct sgl_scope *s, struct sgl *sgl) {
  sgl_free(&sgl->scope_pool, sgl_scope_deinit(s, sgl));
}

void sgl_scope_deref(struct sgl_scope *s, struct sgl *sgl) {
  assert(s->nrefs > 0);
  if (!--s->nrefs) { sgl_scope_free(s, sgl); }
}

void sgl_scope_end(struct sgl_scope *s, struct sgl *sgl) {
  sgl_ls_rdo(&s->defers, struct sgl_op_defer, ls, op) {
    sgl_run_task(sgl, sgl->task, op->beg_pc->next, op->end_pc->next);
  }

  sgl_ls_init(&s->defers);
}
