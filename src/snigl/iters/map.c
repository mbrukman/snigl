#include <stdio.h>

#include "snigl/iters/map.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/util.h"
#include "snigl/val.h"

static struct sgl_val *next_val_imp(struct sgl_iter *i,
                                    struct sgl *sgl,
                                    struct sgl_pos *pos,
                                    struct sgl_ls *root) {
  struct sgl_map_iter *mi = sgl_baseof(struct sgl_map_iter, iter, i);

  for (;;) {
    if (!sgl_iter_next_val(mi->in, sgl, pos, sgl_stack(sgl))) { return NULL; }
    struct sgl_op *pc = sgl->pc;
    sgl_val_call(mi->fn, sgl, pos, pc, true);
    sgl->pc = pc;
    struct sgl_val *v = sgl_pop(sgl);
    if (!v) { break; }
  
    if (v->type == sgl->Nil) {
      sgl_val_free(v, sgl);
      continue;
    }
    
    if (root) { sgl_ls_push(root, &v->ls); }
    return v;
  }

  return NULL;
}

static bool skip_vals_imp(struct sgl_iter *i,
                          struct sgl *sgl,
                          struct sgl_pos *pos,
                          sgl_int_t nvals) {
  struct sgl_map_iter *mi = sgl_baseof(struct sgl_map_iter, iter, i);
  return sgl_iter_skip_vals(mi->in, sgl, pos, nvals);
}

static void free_imp(struct sgl_iter *i, struct sgl *sgl) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->MapIter);
  struct sgl_map_iter *mi = sgl_baseof(struct sgl_map_iter, iter, i);
  sgl_iter_deref(mi->in, sgl);
  sgl_val_free(mi->fn, sgl);
  sgl_free(&rt->pool, mi);
}

struct sgl_map_iter *sgl_map_iter_new(struct sgl *sgl,
                                      struct sgl_iter *in,
                                      struct sgl_val *fn) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->MapIter);
  struct sgl_map_iter *i = sgl_malloc(&rt->pool);
  sgl_iter_init(&i->iter);
  i->iter.next_val = next_val_imp;
  i->iter.skip_vals = skip_vals_imp;
  i->iter.free = free_imp;
  i->in = in;
  i->fn = fn;
  return i;
}
