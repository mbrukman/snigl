#ifndef SNIGL_ITERS_INT_H
#define SNIGL_ITERS_INT_H

#include "snigl/iter.h"

struct sgl_int_iter {
  struct sgl_iter iter;
  sgl_int_t max, pos;
};

struct sgl_int_iter *sgl_int_iter_new(struct sgl *sgl, sgl_int_t max);

#endif
