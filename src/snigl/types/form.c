#include <stdio.h>

#include "snigl/buf.h"
#include "snigl/call.h"
#include "snigl/form.h"
#include "snigl/lib.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"
#include "snigl/type.h"
#include "snigl/types/form.h"
#include "snigl/val.h"

static enum sgl_cmp cmp_val(struct sgl_val *val, const struct sgl_val *rhs) {
  return sgl_ptr_cmp(val->as_form, rhs->as_form);
}

static void deinit_val(struct sgl_val *val, struct sgl *sgl) {
  sgl_form_deref(val->as_form, sgl);
}

static void dump_val(struct sgl_val *val, struct sgl_buf *out) {
  struct sgl_form *f = val->as_form;
  
  sgl_buf_printf(out,
                 "(Form %s %" SGL_INT " %" SGL_INT ")",
                 f->type->id, f->pos.row, f->pos.col);
}

static void dup_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *out) {
  struct sgl_form *f = out->as_form = val->as_form;
  f->nrefs++;
}

static bool is_val(struct sgl_val *val, struct sgl_val *rhs) {
  return val->as_form == rhs->as_form;
}

static sgl_uint_t hash_val(struct sgl_val *v) { return (sgl_uint_t)v->as_form; }

struct sgl_type *sgl_form_type_new(struct sgl *sgl,
                                     struct sgl_pos *pos,
                                     struct sgl_lib *lib,
                                     struct sgl_sym *id,
                                     struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_type_new(sgl, pos, lib, id, parents);
  t->cmp_val = cmp_val;
  t->deinit_val = deinit_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->is_val = is_val;
  t->hash_val = hash_val;
  return t;
}
