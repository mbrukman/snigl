#include <stdio.h>

#include "snigl/buf.h"
#include "snigl/call.h"
#include "snigl/curry.h"
#include "snigl/form.h"
#include "snigl/lib.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"
#include "snigl/type.h"
#include "snigl/types/curry.h"
#include "snigl/val.h"

static struct sgl_op *call_val(struct sgl_val *v,
                               struct sgl *sgl,
                               struct sgl_pos *pos,
                               struct sgl_op *return_pc,
                               bool now) {
  struct sgl_curry *c = v->as_curry;
  struct sgl_ls *s = sgl_stack(sgl);
  sgl_ls_do(&c->args->root, struct sgl_val, ls, a) { sgl_val_dup(a, sgl, s); }
  return sgl_val_call(c->target, sgl, pos, return_pc, now);
}

static void dump_val(struct sgl_val *val, struct sgl_buf *out) {
  struct sgl_curry *c = val->as_curry;
  struct sgl_ls *root = &c->args->root;
  sgl_buf_putc(out, '(');

  sgl_ls_do(&c->args->root, struct sgl_val, ls, a) {
    if (&a->ls != root) { sgl_buf_putc(out, ' '); }
    sgl_val_dump(a, out);
  }
  
  sgl_buf_putc(out, ' ');
  sgl_val_dump(c->target, out);
  sgl_buf_putc(out, ')');
}

static void dup_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *out) {
  struct sgl_curry *c = out->as_curry = val->as_curry;
  c->nrefs++;
}

static bool is_val(struct sgl_val *val, struct sgl_val *rhs) {
  return val->as_curry == rhs->as_curry;
}

static sgl_uint_t hash_val(struct sgl_val *v) {
  struct sgl_curry *c = v->as_curry;
  sgl_uint_t h = 0;
  h ^= sgl_val_hash(c->target);
  sgl_ls_do(&c->args->root, struct sgl_val, ls, a) { h ^= sgl_val_hash(a); }  
  return h;
}

struct sgl_type *sgl_curry_type_new(struct sgl *sgl,
                                    struct sgl_pos *pos,
                                    struct sgl_lib *lib,
                                    struct sgl_sym *id,
                                    struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_ref_type_new(sgl, pos, lib, id, parents,
                                        sizeof(struct sgl_curry),
                                        offsetof(struct sgl_curry, ls));
  t->call_val = call_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->is_val = is_val;
  t->hash_val = hash_val;
  return t;
}
