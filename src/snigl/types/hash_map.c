#include <stdlib.h>

#include "snigl/cemit.h"
#include "snigl/buf.h"
#include "snigl/lib.h"
#include "snigl/hash_map.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"
#include "snigl/type.h"
#include "snigl/types/hash_map.h"
#include "snigl/val.h"

static bool cemit_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      const char *id,
                      const char *root,
                      struct sgl_cemit *out) {
  struct sgl_hash_map *m = val->as_hash_map;
  struct sgl_hash *h = &m->hash;
  
  sgl_cemit_line(out,
                 "%s = sgl_val_new(sgl, sgl->HashMap, %s);",
                 id, root ? root : "NULL");
  
  sgl_cemit_line(out, "%s->as_hash_map = sgl_hash_map_new(sgl);", id);

  if (m->hash.len) {
    struct sgl_sym *kid = sgl_gsym(sgl), *vid = sgl_gsym(sgl);
    sgl_cemit_line(out, "struct sgl_val *%s = NULL, *%s = NULL;", kid->id, vid->id);

    sgl_hash_slots_do(h, hs) {
      sgl_ls_do(hs, struct sgl_hash_node, ls, n) {
        if (!sgl_val_cemit(n->key, sgl, pos, kid->id, NULL, out)) { return false; }
        if (!sgl_val_cemit(n->val, sgl, pos, vid->id, NULL, out)) { return false; }

        sgl_cemit_line(out, "sgl_hash_map_put(%s, sgl, %s, %s);",
                       id, kid->id, vid->id);
      }
    }
  }
  
  return true;
}

static bool clone_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      struct sgl_val *out) {
  struct sgl_hash_map *dst_map = out->as_hash_map = sgl_hash_map_new(sgl);
  struct sgl_hash *src = &val->as_hash_map->hash;
  if (!src->len) { return true; }
  struct sgl_hash *dst = &dst_map->hash;
  sgl_hash_init_slots(dst, sgl);
 
  sgl_hash_slots_do(src, hs) {
    sgl_ls_do(hs, struct sgl_hash_node, ls, n) {
      sgl_uint_t hk = n->hkey;
      struct sgl_ls *hs = sgl_hash_slot(dst, hk);
      
      sgl_hash_add(dst, sgl, hs, hk,
                   sgl_val_clone(n->key, sgl, pos, NULL),
                   sgl_val_clone(n->val, sgl, pos, NULL));
    }
  }

  return true;
}

static void deinit_val(struct sgl_val *v, struct sgl *sgl) {
  sgl_hash_map_deref(v->as_hash_map, sgl);
}

static void dump_val(struct sgl_val *v, struct sgl_buf *out) {
  sgl_buf_putcs(out, "(HashMap");

  sgl_hash_slots_do(&v->as_hash_map->hash, hs) {
    sgl_ls_do(hs, struct sgl_hash_node, ls, n) {
      sgl_buf_putc(out, ' ');
      sgl_val_dump(n->key, out);
      sgl_buf_putcs(out, ": ");
      sgl_val_dump(n->val, out);
    }
  }

  sgl_buf_putc(out, ')');
}

static void dup_val(struct sgl_val *v, struct sgl *sgl, struct sgl_val *out) {
  struct sgl_hash_map *m = v->as_hash_map;
  out->as_hash_map = m;
  m->nrefs++;
}

static bool eq_val(struct sgl_val *v, struct sgl *sgl, struct sgl_val *rhs) {
  struct sgl_hash *vh = &v->as_hash_map->hash, *rh = &rhs->as_hash_map->hash;
  if (vh->len != rh->len) { return false; }
  if (!vh->len) { return true; }
  
  for (struct sgl_ls *vs = vh->slots, *rs = rh->slots;
       vs < vh->slots + vh->len;
       vs++, rs++) {
    for (struct sgl_ls *vi = vs, *ri = rs;; vi = vi->next, ri = ri->next) {
      if (vi == vs && ri == rs) { break; }
      if (vi == vs || ri == rs) { return false; }
      struct sgl_hash_node
        *vn = sgl_baseof(struct sgl_hash_node, ls, vi),
        *rn = sgl_baseof(struct sgl_hash_node, ls, ri);

      if (!sgl_val_eq(vn->key, sgl, rn->key) ||
          !sgl_val_eq(vn->val, sgl, rn->val)) { return false; }
    }
  }

  return true;
}

static bool is_val(struct sgl_val *v, struct sgl_val *rhs) {
  return v->as_hash_map == rhs->as_hash_map;
}

static sgl_uint_t hash_val(struct sgl_val *v) {
  return (sgl_uint_t)v->as_hash_map;
}

struct sgl_type *sgl_hash_map_type_new(struct sgl *sgl,
                                       struct sgl_pos *pos,
                                       struct sgl_lib *lib,
                                       struct sgl_sym *id,
                                       struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_ref_type_new(sgl, pos, lib, id, parents,
                                        sizeof(struct sgl_hash_map),
                                        offsetof(struct sgl_hash_map, ls));

  t->cemit_val = cemit_val;
  t->deinit_val = deinit_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->clone_val = clone_val;
  t->eq_val = eq_val;
  t->is_val = is_val;
  t->hash_val = hash_val;
  return t;
}
