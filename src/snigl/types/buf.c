#include <string.h>

#include "snigl/buf.h"
#include "snigl/lib.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/buf.h"
#include "snigl/type.h"
#include "snigl/types/buf.h"
#include "snigl/val.h"

static bool bool_val(struct sgl_val *val, struct sgl *sgl) {
  return val->as_buf->len;
}

static bool clone_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      struct sgl_val *out) {
  struct sgl_buf *vb = val->as_buf, *ob = sgl_buf_new(sgl);
  sgl_buf_grow(ob, vb->len);
  memcpy(ob->data, vb->data, vb->len);
  ob->len = vb->len;
  out->as_buf = ob;
  return true;
}

static void deinit_val(struct sgl_val *val, struct sgl *sgl) {
  sgl_buf_deref(val->as_buf, sgl);
}

static void dump_val(struct sgl_val *val, struct sgl_buf *out) {
  sgl_buf_putcs(out, "(Buf");
  struct sgl_buf *b = val->as_buf;

  if (b->len) {
    for (unsigned char *c = b->data; c < b->data + b->len; c++) {
      sgl_buf_printf(out, " %d", *c);
    }
  }

  sgl_buf_putc(out, ')');
}

static void dup_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *out) {
  struct sgl_buf *b = val->as_buf;
  out->as_buf = b;
  b->nrefs++;
}

static bool eq_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *rhs) {
  struct sgl_buf *vb = val->as_buf, *rb = rhs->as_buf;
  return memcmp(sgl_buf_cs(vb), sgl_buf_cs(rb), sgl_min(vb->len, rb->len)) == 0;
}

static bool is_val(struct sgl_val *val, struct sgl_val *rhs) {
  return val->as_buf == rhs->as_buf;
}

static sgl_uint_t hash_val(struct sgl_val *v) {
  return (sgl_uint_t)v->as_buf;
}

static void print_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      struct sgl_buf *out) {
  struct sgl_buf *b = val->as_buf;
  sgl_buf_nputcs(out, sgl_buf_cs(b), b->len);
}

struct sgl_type *sgl_buf_type_new(struct sgl *sgl,
                                  struct sgl_pos *pos,
                                  struct sgl_lib *lib,
                                  struct sgl_sym *id,
                                  struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_ref_type_new(sgl, pos, lib, id, parents,
                                        sizeof(struct sgl_buf),
                                        offsetof(struct sgl_buf, ls));
  t->bool_val = bool_val;
  t->clone_val = clone_val;
  t->deinit_val = deinit_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->eq_val = eq_val;
  t->is_val = is_val;
  t->hash_val = hash_val;
  t->print_val = print_val;
  return t;
}
