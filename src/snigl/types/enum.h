#ifndef SNIGL_TYPES_ENUM_H
#define SNIGL_TYPES_ENUM_H

#include "snigl/vec.h"

#define sgl_enum_type_new(sgl, pos, lib, id, parents, ...)    \
  _sgl_enum_type_new(sgl, pos, lib, id, parents,              \
                     (struct sgl_sym *[]){__VA_ARGS__, NULL}) \

struct sgl_lib;
struct sgl_sym;
struct sgl_type;
struct sgl_val;

struct sgl_enum_type {
  struct sgl_type type;
  struct sgl_vec alts;
};

struct sgl_type *_sgl_enum_type_new(struct sgl *sgl,
                                    struct sgl_pos *pos,
                                    struct sgl_lib *lib,
                                    struct sgl_sym *id,
                                    struct sgl_type *parents[],
                                    struct sgl_sym *alts[]);

struct sgl_enum_type *sgl_enum_type(struct sgl_type *t);

struct sgl_enum_iter {
  struct sgl_iter iter;
  struct sgl_enum_type *type;
  sgl_int_t pos;
};

struct sgl_enum_iter *sgl_enum_iter_new(struct sgl *sgl, struct sgl_enum_type *type);

#endif
