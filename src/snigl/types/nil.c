#include "snigl/buf.h"
#include "snigl/lib.h"
#include "snigl/sgl.h"
#include "snigl/type.h"
#include "snigl/types/nil.h"
#include "snigl/val.h"

static bool bool_val(struct sgl_val *val, struct sgl *sgl) { return false; }

static void dump_val(struct sgl_val *val, struct sgl_buf *out) {
  sgl_buf_putcs(out, "nil");
}

static void dup_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *out) { }

static bool is_val(struct sgl_val *val, struct sgl_val *rhs) { return true; }

static sgl_uint_t hash_val(struct sgl_val *v) { return 0; }

static void print_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      struct sgl_buf *out) { }

struct sgl_type *sgl_nil_type_new(struct sgl *sgl,
                                  struct sgl_pos *pos,
                                  struct sgl_lib *lib,
                                  struct sgl_sym *id,
                                  struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_type_new(sgl, pos, lib, id, parents);
  t->bool_val = bool_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->is_val = is_val;
  t->hash_val = hash_val;
  t->print_val = print_val;
  return t;
}
