#include "snigl/cemit.h"
#include "snigl/buf.h"
#include "snigl/lib.h"
#include "snigl/ref.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"
#include "snigl/type.h"
#include "snigl/types/ref.h"
#include "snigl/val.h"

static bool bool_val(struct sgl_val *v, struct sgl *sgl) {
  return v->as_ref->val->type != sgl->Nil;
}

static bool cemit_val(struct sgl_val *v,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      const char *id,
                      const char *root,
                      struct sgl_cemit *out) {
  sgl_cemit_line(out,
                 "%s = sgl_val_new(sgl, sgl->Ref, %s);",
                 id, root ? root : "NULL");

  struct sgl_sym *vid = sgl_gsym(sgl);
  sgl_val_cemit(v->as_ref->val, sgl, pos, vid->id, NULL, out); 
  sgl_cemit_line(out, "%s->as_ref = sgl_ref_new(sgl, %s);", id, vid->id);
  return true;
}

static bool clone_val(struct sgl_val *v,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      struct sgl_val *out) {
  struct sgl_ref *src = v->as_ref;
  out->as_ref = sgl_ref_new(sgl, sgl_val_clone(src->val, sgl, pos, NULL));
  return true;
}

static enum sgl_cmp cmp_val(struct sgl_val *lhs, const struct sgl_val *rhs) {
  return sgl_val_cmp(lhs->as_ref->val, rhs->as_ref->val);
}

static void deinit_val(struct sgl_val *v, struct sgl *sgl) {
  sgl_ref_deref(v->as_ref, sgl);
}

static void dump_val(struct sgl_val *v, struct sgl_buf *out) {
  sgl_buf_putcs(out, "(Ref ");
  sgl_val_dump(v->as_ref->val, out);
  sgl_buf_putc(out, ')');
}

static void dup_val(struct sgl_val *v, struct sgl *sgl, struct sgl_val *out) {
  struct sgl_ref *r = out->as_ref = v->as_ref;
  r->nrefs++;
}

static bool eq_val(struct sgl_val *lhs, struct sgl *sgl, struct sgl_val *rhs) {
  return sgl_val_eq(lhs->as_ref->val, sgl, rhs->as_ref->val);
}

static bool is_val(struct sgl_val *lhs, struct sgl_val *rhs) {
  return lhs->as_ref == rhs->as_ref;
}

static sgl_uint_t hash_val(struct sgl_val *v) { return (sgl_uint_t)v->as_ref; }

static void print_val(struct sgl_val *v,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      struct sgl_buf *out) {
  sgl_val_print(v->as_ref->val, sgl, pos, out);
}

struct sgl_type *sgl_vref_type_new(struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_lib *lib,
                                   struct sgl_sym *id,
                                   struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_ref_type_new(sgl, pos, lib, id, parents,
                                        sizeof(struct sgl_ref),
                                        offsetof(struct sgl_ref, ls));
  t->bool_val = bool_val;
  t->cemit_val = cemit_val;
  t->deinit_val = deinit_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->clone_val = clone_val;
  t->cmp_val = cmp_val;
  t->eq_val = eq_val;
  t->is_val = is_val;
  t->hash_val = hash_val;
  t->print_val = print_val;
  return t;
}
