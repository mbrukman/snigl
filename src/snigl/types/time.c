#include "snigl/cemit.h"
#include "snigl/buf.h"
#include "snigl/lib.h"
#include "snigl/sgl.h"
#include "snigl/time.h"
#include "snigl/type.h"
#include "snigl/types/time.h"
#include "snigl/val.h"

static bool bool_val(struct sgl_val *val, struct sgl *sgl) {
  return val->as_time > 0;
}

static bool cemit_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      const char *id,
                      const char *root,
                      struct sgl_cemit *out) {
  sgl_cemit_line(out,
                 "%s = sgl_val_new(sgl, sgl->Time, %s);",
                 id, root ? root : "NULL");
  
  sgl_cemit_line(out, "%s->as_time = %ju;", id, (uintmax_t)val->as_time);
  return true;
}

static enum sgl_cmp cmp_val(struct sgl_val *val, const struct sgl_val *rhs) {
  return sgl_time_cmp(val->as_time, rhs->as_time);
}

static void dump_val(struct sgl_val *val, struct sgl_buf *out) {
  char buf[20] = {0};
  strftime(buf, 20, "%Y-%m-%d %H:%M:%S", localtime(&val->as_time));
  sgl_buf_putcs(out, buf);
}

static void dup_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *out) {
  out->as_time = val->as_time;
}

static bool is_val(struct sgl_val *val, struct sgl_val *rhs) {
  return val->as_time == rhs->as_time;
}

static sgl_uint_t hash_val(struct sgl_val *v) { return v->as_time; }

struct sgl_type *sgl_time_type_new(struct sgl *sgl,
                                  struct sgl_pos *pos,
                                  struct sgl_lib *lib,
                                  struct sgl_sym *id,
                                  struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_type_new(sgl, pos, lib, id, parents);
  t->bool_val = bool_val;
  t->cemit_val = cemit_val;
  t->cmp_val = cmp_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->is_val = is_val;
  t->hash_val = hash_val;
  return t;
}
