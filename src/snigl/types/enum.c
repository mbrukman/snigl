#include <assert.h>
#include <stdio.h>

#include "snigl/buf.h"
#include "snigl/call.h"
#include "snigl/struct.h"
#include "snigl/iter.h"
#include "snigl/lib.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/stack.h"
#include "snigl/sym.h"
#include "snigl/type.h"
#include "snigl/types/enum.h"
#include "snigl/user_type.h"
#include "snigl/val.h"

static enum sgl_cmp cmp_val(struct sgl_val *lhs, const struct sgl_val *rhs) {
  return sgl_int_cmp(lhs->as_enum.i, rhs->as_enum.i);
}

static void dump_val(struct sgl_val *val, struct sgl_buf *out) {
  sgl_buf_printf(out, "%s/%s", val->type->def.id->id, val->as_enum.id->id);
}

static void dup_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *out) {
  out->as_enum = val->as_enum;
}

static bool is_val(struct sgl_val *val, struct sgl_val *rhs) {
  return val->as_enum.i == rhs->as_enum.i;
}

static sgl_uint_t hash_val(struct sgl_val *v) { return v->as_enum.i; }

static struct sgl_iter *iter_val(struct sgl_val *val,
                                 struct sgl *sgl,
                                 struct sgl_type **type) {
  if (type) { *type = sgl->EnumIter; }
  return &sgl_enum_iter_new(sgl, sgl_enum_type(val->as_type))->iter;
}

struct sgl_enum_type *sgl_enum_type(struct sgl_type *t) {
  return sgl_baseof(struct sgl_enum_type, type, t);
}

static void free_imp(struct sgl_type *t, struct sgl *sgl) {
  struct sgl_enum_type *et = sgl_enum_type(t);
  sgl_vec_deinit(&et->alts);
  sgl_free(&sgl->enum_type_pool, et);
}

struct sgl_type *_sgl_enum_type_new(struct sgl *sgl,
                                    struct sgl_pos *pos,
                                    struct sgl_lib *lib,
                                    struct sgl_sym *id,
                                    struct sgl_type *parents[],
                                    struct sgl_sym *alts[]) {
  struct sgl_enum_type *et = sgl_malloc(&sgl->enum_type_pool);
  sgl_vec_init(&et->alts, sizeof(struct sgl_enum));
  struct sgl_type *t = &et->type;

  for (struct sgl_sym **a = alts; *a; a++) {
    struct sgl_enum *e = sgl_vec_push(&et->alts);
    e->i = et->alts.len - 1;
    e->id = *a;
    
    struct sgl_val *v = sgl_val_new(sgl, t, NULL);
    v->as_enum = *e;
    sgl_lib_add_const(lib, sgl, pos, e->id, v);
  }

  sgl_type_init(t, sgl, pos, lib, id, parents);
  sgl_derive(t, sgl->Enum);
  t->call_val = sgl_user_type_call_val;
  t->cmp_val = cmp_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->is_val = is_val;
  t->hash_val = hash_val;
  t->iter_val = sgl_user_type_iter_val;
  t->free = free_imp;

  struct sgl_type *mt = sgl_type_meta(t, sgl);
  sgl_derive(mt, sgl_type_meta(sgl->Enum, sgl));
  mt->iter_val = iter_val;
  
  return t;
}
                    
static struct sgl_val *iter_next_val_imp(struct sgl_iter *i,
                                         struct sgl *sgl,
                                         struct sgl_pos *pos,
                                         struct sgl_ls *root) {
  struct sgl_enum_iter *ei = sgl_baseof(struct sgl_enum_iter, iter, i);
  if (++ei->pos == ei->type->alts.len) { return NULL; }
  struct sgl_val *v = sgl_val_new(sgl, &ei->type->type, root);
  v->as_enum = *(struct sgl_enum *)sgl_vec_get(&ei->type->alts, ei->pos);
  return v;
}

static bool iter_skip_vals_imp(struct sgl_iter *i,
                               struct sgl *sgl,
                               struct sgl_pos *pos,
                               sgl_int_t nvals) {
  struct sgl_enum_iter *ei = sgl_baseof(struct sgl_enum_iter, iter, i);

  if (ei->pos + nvals >= ei->type->alts.len) {
    sgl_error(sgl, pos, sgl_strdup("Iter eof"));
    return false;
  }
  
  ei->pos += nvals;
  return true;
}

static void iter_free_imp(struct sgl_iter *i, struct sgl *sgl) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->EnumIter);
  sgl_free(&rt->pool, sgl_baseof(struct sgl_enum_iter, iter, i));
}

struct sgl_enum_iter *sgl_enum_iter_new(struct sgl *sgl, struct sgl_enum_type *type) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->EnumIter);
  struct sgl_enum_iter *i = sgl_malloc(&rt->pool);
  
  sgl_iter_init(&i->iter);
  i->iter.next_val = iter_next_val_imp;
  i->iter.skip_vals = iter_skip_vals_imp;
  i->iter.free = iter_free_imp;
  i->type = type;
  i->pos = -1;
  return i;
}
