#include "snigl/cemit.h"
#include "snigl/buf.h"
#include "snigl/lib.h"
#include "snigl/rgb.h"
#include "snigl/sgl.h"
#include "snigl/type.h"
#include "snigl/types/rgb.h"
#include "snigl/val.h"


static bool bool_val(struct sgl_val *val, struct sgl *sgl) {
  struct sgl_rgb *c = &val->as_rgb;
  return c->r || c->g || c->b;
}

static bool cemit_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      const char *id,
                      const char *root,
                      struct sgl_cemit *out) {
  struct sgl_rgb *c = &val->as_rgb;
  
  sgl_cemit_line(out,
                 "%s = sgl_val_new(sgl, sgl->RGB, %s);",
                 id, root ? root : "NULL");
  
  sgl_cemit_line(out,
                 "sgl_rgb_init(&%s->as_rgb, "
                 "%" SGL_I32 ", %" SGL_I32 ", %" SGL_I32 ");", 
                 id, c->r, c->g, c->b);
  return true;
}

static enum sgl_cmp cmp_val(struct sgl_val *lhs, const struct sgl_val *rhs) {
  const struct sgl_rgb *lc = &lhs->as_rgb, *rc = &rhs->as_rgb;
  enum sgl_cmp ok = sgl_int_cmp(lc->r, rc->r);
  if (ok != SGL_EQ) { return ok; }
  ok = sgl_int_cmp(lc->g, rc->g);
  if (ok != SGL_EQ) { return ok; }
  return sgl_int_cmp(lc->b, rc->b);
}

static void dump_val(struct sgl_val *val, struct sgl_buf *out) {
  struct sgl_rgb *c = &val->as_rgb;
  
  sgl_buf_printf(out,
                 "(RGB %" SGL_I32 " %" SGL_I32 " %" SGL_I32 ")",
                 c->r, c->g, c->b);
}

static void dup_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *out) {
  out->as_rgb = val->as_rgb;
}

static bool is_val(struct sgl_val *lhs, struct sgl_val *rhs) {
  struct sgl_rgb *lc = &lhs->as_rgb, *rc = &rhs->as_rgb;
  return lc->r == rc->r && lc->g == rc->g && lc->b == rc->b;
}

static sgl_uint_t hash_val(struct sgl_val *v) {
  struct sgl_rgb *c = &v->as_rgb;
  return c->r ^ c->g ^ c->b;
}

struct sgl_type *sgl_rgb_type_new(struct sgl *sgl,
                                  struct sgl_pos *pos,
                                  struct sgl_lib *lib,
                                  struct sgl_sym *id,
                                  struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_type_new(sgl, pos, lib, id, parents);
  t->bool_val = bool_val;
  t->cemit_val = cemit_val;
  t->cmp_val = cmp_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->is_val = is_val;
  t->hash_val = hash_val;
  return t;
}
