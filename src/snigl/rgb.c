#include "snigl/rgb.h"

struct sgl_rgb *sgl_rgb_init(struct sgl_rgb *rgb,
                             sgl_int_t r, sgl_int_t g, sgl_int_t b) {
  rgb->r = r;
  rgb->g = g;
  rgb->b = b;
  return rgb;
}
