#ifndef SNIGL_RGB_H
#define SNIGL_RGB_H

#include "snigl/int.h"

struct sgl_rgb {
  sgl_i32_t r, g, b;
};

struct sgl_rgb *sgl_rgb_init(struct sgl_rgb *rgb,
                             sgl_int_t r, sgl_int_t g, sgl_int_t b);

#endif
