#include "snigl/int.h"

enum sgl_cmp sgl_int_cmp(sgl_int_t lhs, sgl_int_t rhs) {
  if (lhs < rhs) { return SGL_LT; }
  return (lhs > rhs) ? SGL_GT : SGL_EQ;
}

char* sgl_int_str(sgl_int_t val, char* out, int base) {
  if (base < 2 || base > 36) {
    *out = 0;
    return out;
  }
  
  static const char *chars =
    "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz";

  char *beg = out, *end = out;
  sgl_int_t rem = 0;
  
  do {
    rem = val;
    val /= base;
    *end++ =  chars[35 + (rem - val * base)];
  } while (val);
  
  if (rem < 0) { *end++ = '-'; }
  out = end;
  *end-- = 0;
  
  while (beg < end) {
    char c = *end;
    *end-- = *beg;
    *beg++ = c;
  }

  return out;
}
