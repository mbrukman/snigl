#ifndef SNIGL_FILE_H
#define SNIGL_FILE_H

#include <stdio.h>
#include "snigl/int.h"
#include "snigl/iter.h"
#include "snigl/ls.h"

struct sgl;

struct sgl_file {
  struct sgl_ls ls;
  FILE *imp;
  sgl_int_t nrefs;
};

struct sgl_file *sgl_file_new(struct sgl *sgl, FILE *imp);
void sgl_file_deref(struct sgl_file *f, struct sgl *sgl);
struct sgl_file *sgl_file_malloc(struct sgl *sgl);
void sgl_file_free(struct sgl_file *f, struct sgl *sgl);
struct sgl_file *sgl_file_init(struct sgl_file *f, FILE *imp);

struct sgl_file_line_iter {
  struct sgl_iter iter;
  struct sgl_file *in;
  struct sgl_buf buf;
};

struct sgl_file_line_iter *sgl_file_line_iter_new(struct sgl *sgl,
                                                  struct sgl_file *in);

#endif
