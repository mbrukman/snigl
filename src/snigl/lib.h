#ifndef SNIGL_LIB_H
#define SNIGL_LIB_H

#include "snigl/buf.h"
#include "snigl/fimp.h"
#include "snigl/hash.h"
#include "snigl/macro.h"
#include "snigl/pmacro.h"

#define sgl_lib_add_func(lib, sgl, pos, id, rets, ...)        \
  _sgl_lib_add_func(lib, sgl, pos, id,                        \
                    sizeof((struct sgl_arg[]){__VA_ARGS__}) / \
                    sizeof(struct sgl_arg),                   \
                    (struct sgl_arg[]){__VA_ARGS__},          \
                    rets)                                     \
  
#define sgl_lib_add_cfunc(lib, sgl, pos, id, imp, rets, ...)      \
  _sgl_lib_add_cfunc(lib, sgl, pos, id,                           \
                     sizeof((struct sgl_arg[]){__VA_ARGS__}) /    \
                     sizeof(struct sgl_arg),                      \
                     (struct sgl_arg[]){__VA_ARGS__},             \
                     rets, imp)                                   \
  
struct sgl;
struct sgl_def;
struct sgl_pos;
struct sgl_sym;

struct sgl_lib {
  struct sgl_ls ls;
  struct sgl_sym *id;
  struct sgl_hash defs;
  struct sgl_buf docs;
  
  bool (*init)(struct sgl_lib *, struct sgl *, struct sgl_pos *);
  void (*deinit)(struct sgl_lib *, struct sgl *);
  bool init_ok;
};

struct sgl_lib *sgl_lib_init(struct sgl_lib *lib,
                             struct sgl *sgl,
                             struct sgl_pos *pos,
                             struct sgl_sym *id);

struct sgl_lib *sgl_lib_deinit1(struct sgl_lib *lib, struct sgl *sgl);
struct sgl_lib *sgl_lib_deinit2(struct sgl_lib *lib, struct sgl *sgl);
struct sgl_lib *sgl_lib_new(struct sgl *sgl, struct sgl_pos *pos, struct sgl_sym *id);

bool sgl_lib_add(struct sgl_lib *lib,
                 struct sgl *sgl,
                 struct sgl_pos *pos,
                 struct sgl_def *def);

bool sgl_lib_add_const(struct sgl_lib *l,
                       struct sgl *sgl,
                       struct sgl_pos *pos,
                       struct sgl_sym *id,
                       struct sgl_val *val);

struct sgl_pmacro *sgl_lib_add_pmacro(struct sgl_lib *l,
                                      struct sgl *sgl,
                                      struct sgl_pos *pos,
                                      struct sgl_sym *id,
                                      sgl_pmacro_imp_t imp);

struct sgl_macro *sgl_lib_add_macro(struct sgl_lib *l,
                                    struct sgl *sgl,
                                    struct sgl_pos *pos,
                                    struct sgl_sym *id,
                                    sgl_int_t nargs,
                                    sgl_macro_imp_t imp);

struct sgl_fimp *_sgl_lib_add_func(struct sgl_lib *lib,
                                   struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_sym *id,
                                   sgl_int_t nargs,
                                   struct sgl_arg args[],
                                   struct sgl_type *rets[]);

struct sgl_fimp *_sgl_lib_add_cfunc(struct sgl_lib *lib,
                                    struct sgl *sgl,
                                    struct sgl_pos *pos,
                                    struct sgl_sym *id,
                                    sgl_int_t nargs,
                                    struct sgl_arg args[],
                                    struct sgl_type *rets[],
                                    sgl_cimp_t imp);

struct sgl_def *sgl_lib_find(struct sgl_lib *lib,
                             struct sgl *sgl,
                             struct sgl_pos *pos,
                             struct sgl_sym *id);

struct sgl_type *sgl_lib_find_type(struct sgl_lib *lib,
                                   struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_sym *id);

bool sgl_lib_use(struct sgl_lib *lib,
                 struct sgl *sgl,
                 struct sgl_pos *pos,
                 struct sgl_def *def);

#endif
