#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

#include "snigl/buf.h"
#include "snigl/form.h"
#include "snigl/parse.h"
#include "snigl/sgl.h"
#include "snigl/str.h"
#include "snigl/val.h"
#include "snigl/util.h"

bool sgl_parse(const char *in, struct sgl *sgl, struct sgl_ls *out) {
  struct sgl_pos pos = SGL_START_POS;
  pos.load_path = sgl->load_path;
  while ((in = sgl_parse_form(in, sgl, &pos, 0, out)));
  return sgl->errors.next == &sgl->errors;
}

const char *sgl_parse_form(const char *in,
                           struct sgl *sgl,
                           struct sgl_pos *pos,
                           char end_char,
                           struct sgl_ls *out) {
  char c = 0;

  while ((c = *in++)) {
    if (c == end_char) { return in-1; }
    
    switch (c) {
    case ' ':
    case '\t':
      pos->col++;
      break;  
    case '\n':
      pos->row++;
      pos->col = SGL_START_POS.col;
      break;
    case '\'':
      pos->col++;
      return sgl_parse_id(in, sgl, pos, end_char, true, out);
    case '#':
      return sgl_parse_char(in, sgl, pos, out);
    case '\\':
      return sgl_parse_special_char(in, sgl, pos, out);
    case '"':
      return sgl_parse_str(in, sgl, pos, out);
    case '%':
      return sgl_parse_lift(in, sgl, pos, end_char, out);
    case '&':
      return sgl_parse_ref(in, sgl, pos, end_char, out);
    case ',':
      return sgl_parse_rest(in, sgl, pos, end_char, out);
    case '(':
      return sgl_parse_sexpr(in, sgl, pos, out);
    case '{':
      return sgl_parse_scope(in, sgl, pos, out);
    case '[':
      return sgl_parse_list(in, sgl, pos, out);
    default: {
      bool neg = c == '-' && (*in == '.' || isdigit(*in));
      bool is_num = isdigit(c) || (c == '.' && isdigit(*in)) || neg;

      if (is_num) {
        return sgl_parse_num(neg ? in : in - 1, sgl, pos, neg, out);
      } else if (isgraph(c)) {
        return sgl_parse_id(in-1, sgl, pos, end_char, false, out);
      } else {
        sgl_error(sgl, pos, sgl_sprintf("Invalid input: '%c'", c));
      }
    }}
  }

  return NULL;
}

const char *sgl_parse_end(const char *in,
                          struct sgl *sgl,
                          struct sgl_pos *pos,
                          char end_char,
                          struct sgl_ls *out) {
  while ((in = sgl_parse_form(in, sgl, pos, end_char, out))) {
    if (end_char && *in == end_char) {
      pos->col++;
      return in+1;
    }
  }

  return NULL;
}

const char *sgl_parse_char(const char *in,
                           struct sgl *sgl,
                           struct sgl_pos *pos,
                           struct sgl_ls *out) {
  struct sgl_pos start_pos = *pos;
  pos->col++;
  char c = *in;

  if (!c) {
    sgl_error(sgl, &start_pos, sgl_strdup("Invalid char"));
    return NULL;
  }
    
  struct sgl_val *v = sgl_val_new(sgl, sgl->Char, NULL);
  v->as_char = c;
  sgl_form_lit_new(sgl, &start_pos, out, v);
  return in+1;
}

const char *sgl_parse_special_char(const char *in,
                                   struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_ls *out) {
  struct sgl_pos start_pos = *pos;
  pos->col++;
  char c = *in;

  if (!c) {
    sgl_error(sgl, &start_pos, sgl_strdup("Invalid special char"));
    return NULL;
  }

  if (c == 'n') {
    pos->col++;
    struct sgl_val *v = sgl_val_new(sgl, sgl->Char, NULL);
    v->as_char = '\n';
    sgl_form_lit_new(sgl, &start_pos, out, v);
    return in+1;
  }

  if (c == 's') {
    pos->col++;
    struct sgl_val *v = sgl_val_new(sgl, sgl->Char, NULL);
    v->as_char = ' ';
    sgl_form_lit_new(sgl, &start_pos, out, v);
    return in+1;
  }

  if (isdigit(c)) {
    const char *i = in;
    sgl_int_t n = strtoimax(in, (char **)&i, 10);
    struct sgl_val *v = sgl_val_new(sgl, sgl->Char, NULL);
    v->as_char = (char)n;
    sgl_form_lit_new(sgl, &start_pos, out, v);
    pos->col += i-in;
    return i;
  }
  
  sgl_error(sgl, &start_pos, sgl_sprintf("Invalid special char: '%c'", c));
  return NULL;
}

const char *sgl_parse_id(const char *in,
                         struct sgl *sgl,
                         struct sgl_pos *pos,
                         char end_char,
                         bool quote,
                         struct sgl_ls *out) {
  const char *i = in;
  struct sgl_pos start_pos = *pos;
  char c = 0;

  while ((c = *i++) &&
         !isspace(c) &&
         c != end_char &&
         c != ',' && c != '(' && c != '[' && c != '{' &&
         (c != ':' || i == in + 1) &&
         (c != '.' || i <= in + 2));
  
  if (c != ':') { i--; }
  sgl_int_t len = i-in;
  pos->col += len;
  
  if (quote) {
    struct sgl_val *v = sgl_val_new(sgl, sgl->Sym, NULL);
    v->as_sym = sgl_nsym(sgl, in, len);;
    sgl_form_lit_new(sgl, &start_pos, out, v);
  } else {
    struct sgl_pmacro *m = sgl_find_pmacro(sgl, &start_pos, sgl_nsym(sgl, in, len));
    if (m) { return sgl_pmacro_call(m, i, sgl, pos, end_char, out); }
    sgl_form_id_new(sgl, &start_pos, out, sgl_nsym(sgl, in, len));
  }
  
  return i;
}

const char *sgl_parse_lift(const char *in,
                           struct sgl *sgl,
                           struct sgl_pos *pos,
                           char end_char,
                           struct sgl_ls *out) {
  struct sgl_pos start_pos = *pos;
  pos->col++;
  struct sgl_form *f = sgl_form_lift_new(sgl, &start_pos, out);
  const char *end = sgl_parse_form(in, sgl, pos, end_char, &f->as_body.forms);

  if (!end) {
    sgl_error(sgl, &start_pos, sgl_strdup("Invalid lift expr"));
    return NULL;
  }

  return end;
}

const char *sgl_parse_list(const char *in,
                           struct sgl *sgl,
                           struct sgl_pos *pos,
                           struct sgl_ls *out) {
  struct sgl_form *f = sgl_form_list_new(sgl, pos, out);
  pos->col++;
  struct sgl_ls *fs = &f->as_body.forms;
  const char *end = sgl_parse_end(in, sgl, pos, ']', fs);  
  if (!end) { sgl_error(sgl, pos, sgl_strdup("Open list")); }
  return end;
}

const char *sgl_parse_num(const char *in,
                          struct sgl *sgl,
                          struct sgl_pos *pos,
                          bool neg,
                          struct sgl_ls *out) {
  const char *i = in;
  sgl_int_t trunc = strtoimax(in, (char **)&i, 10);
  
  if (*i == '.' && *(i+1) != '.') {
    const char *j = i+1;
    sgl_int_t frac = strtoimax(j, (char **)&j, 10);  
    struct sgl_val *v = sgl_val_new(sgl, sgl->Fix, NULL);
    sgl_fix_init(&v->as_fix, trunc, frac, j - i - 1, neg);
    sgl_form_lit_new(sgl, pos, out, v);
    pos->col += j-in;
    return j;
  }

  struct sgl_val *v = sgl_val_new(sgl, sgl->Int, NULL);
  v->as_int = neg ? -trunc : trunc;
  sgl_form_lit_new(sgl, pos, out, v);
  pos->col += i-in;
  return i;
}

const char *sgl_parse_ref(const char *in,
                          struct sgl *sgl,
                          struct sgl_pos *pos,
                          char end_char,
                          struct sgl_ls *out) {
  struct sgl_pos start_pos = *pos;
  pos->col++;
  in = sgl_parse_form(in, sgl, pos, end_char, out);
  if (!in) { return NULL; }
  struct sgl_ls *target = sgl_ls_pop(out);
  if (!target) { sgl_error(sgl, &start_pos, sgl_strdup("Nothing to ref")); }
  sgl_form_ref_new(sgl, &start_pos, out, sgl_baseof(struct sgl_form, ls, target));
  return in;
}

const char *sgl_parse_rest(const char *in,
                           struct sgl *sgl,
                           struct sgl_pos *pos,
                           char end_char,
                           struct sgl_ls *out) {
  struct sgl_form *f = sgl_form_expr_new(sgl, pos, out);
  pos->col++;
  
  while ((in = sgl_parse_form(in, sgl, pos, end_char, &f->as_body.forms))) {
    if (end_char && *in == end_char) { return in; }
  }

  return NULL;
}

const char *sgl_parse_scope(const char *in,
                            struct sgl *sgl,
                            struct sgl_pos *pos,
                            struct sgl_ls *out) {
  struct sgl_form *f = sgl_form_scope_new(sgl, pos, out);
  pos->col++;
  
  const char *end = sgl_parse_end(in, sgl, pos, '}', &f->as_body.forms);
  if (!end) { sgl_error(sgl, pos, sgl_strdup("Open scope")); }
  return end;
}

const char *sgl_parse_sexpr(const char *in,
                            struct sgl *sgl,
                            struct sgl_pos *pos,
                            struct sgl_ls *out) {
  struct sgl_pos start_pos = *pos;
  pos->col++;

  struct sgl_form *f = sgl_form_expr_new(sgl, &start_pos, out);
  
  const char *end = sgl_parse_end(in, sgl, pos, ')', &f->as_body.forms);
  if (!end) { sgl_error(sgl, &start_pos, sgl_strdup("Open sexpr")); }
  return end;
}

const char *sgl_parse_str(const char *in,
                         struct sgl *sgl,
                         struct sgl_pos *pos,
                         struct sgl_ls *out) {
  const char *i = in;
  struct sgl_pos start_pos = *pos;
  pos->col++;
  char c = 0, pc = 0;
  struct sgl_buf buf;
  sgl_buf_init(&buf);
  bool ok = false;
  struct sgl_form *s = NULL;

  while ((c = *i++)) {
    if (c == '"' && pc != '\\') { break; }

    if (c == '%' && pc != '\\') {
      if (!s) { s = sgl_form_str_new(sgl, &start_pos, out); }

      if (buf.len) {
        struct sgl_val *v = sgl_val_new(sgl, sgl->Str, NULL);
        v->as_str = sgl_str_new(sgl, sgl_buf_cs(&buf), buf.len);
        sgl_form_lit_new(sgl, pos, &s->as_body.forms, v);
        buf.len = 0;
      }
        
      pos->col++;
      i = sgl_parse_form(i, sgl, pos, 0, &s->as_body.forms);
      if (!i) { goto exit; }
      continue;
    }

    if (c == '\n' && pc == '\\') {
      pc = 0;
      pos->col++;
      continue;
    }

    if (c == '\n') {
      pos->row++;
      pos->col = SGL_START_POS.col;;
    }
    
    if (pc == '\\') {
      switch(c) {
      case 'n':
        sgl_buf_putc(&buf, '\n');
        pos->row++;
        pos->col = SGL_START_POS.col;
        break;
      default:
        sgl_buf_putc(&buf, c);
        pos->col++;
      }

      c = 0;
    } else {
      if (c != '\\') { sgl_buf_putc(&buf, c); }
      pos->col++;
    }
    
    pc = c;
  }

  if (!c) {
    sgl_error(sgl, &start_pos, sgl_strdup("Open str"));
    goto exit;
  }

  if (buf.len) {
    struct sgl_val *v = sgl_val_new(sgl, sgl->Str, NULL);
    v->as_str = sgl_str_new(sgl, sgl_buf_cs(&buf), buf.len);
    sgl_form_lit_new(sgl, pos, s ? &s->as_body.forms : out, v);
  } else {
    sgl_form_lit_new(sgl, pos,
                     s ? &s->as_body.forms : out,
                     sgl_val_dup(&sgl->empty_str, sgl, NULL));
  }
  
  ok = true;
 exit:
  sgl_buf_deinit(&buf);
  return ok ? i : NULL;
}
