#include "snigl/pool.h"
#include "snigl/sgl.h"
#include "snigl/try.h"

struct sgl_try *sgl_try_new(struct sgl *sgl, struct sgl_op *end_pc) {
  struct sgl_try *try = sgl_malloc(&sgl->try_pool);
  try->end_pc = end_pc;
  sgl_state_init(&try->state, sgl);
  sgl_ls_push(&sgl->task->tries, &try->ls);
  return try;
}

void sgl_try_free(struct sgl_try *try, struct sgl *sgl) {
  sgl_free(&sgl->try_pool, try);
}
