#include <assert.h>

#include "snigl/ref.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/stack.h"
#include "snigl/sym.h"
#include "snigl/util.h"

struct sgl_ref *sgl_ref_new(struct sgl *sgl, struct sgl_val *val) {
  struct sgl_ref_type *rl = sgl_baseof(struct sgl_ref_type, type, sgl->Ref);
  struct sgl_ref *r = sgl_malloc(&rl->pool);
  r->val = val;
  r->is_changing = false;
  r->nrefs = 1;
  
  sgl_ls_init(&r->watchers);
  return r;
}

void sgl_ref_deref(struct sgl_ref *r, struct sgl *sgl) {
  assert(r->nrefs > 0);
  
  if (!--r->nrefs) {
    sgl_val_free(r->val, sgl);
    sgl_ls_do(&r->watchers, struct sgl_val, ls, w) { sgl_val_free(w, sgl); }
    struct sgl_ref_type *rl = sgl_baseof(struct sgl_ref_type, type, sgl->Ref);
    sgl_free(&rl->pool, r);
  }
}

struct sgl_val *sgl_ref_get(struct sgl_ref *r, struct sgl *sgl) {
  if (!r->is_changing) {
    sgl_ls_do(&sgl->task->watchers, struct sgl_val, ls, w) {
      sgl_val_dup(w, sgl, &r->watchers);
    }
  }

  return r->val;
}

bool sgl_ref_set(struct sgl_ref *r, struct sgl *sgl, struct sgl_val *val) {
  if (r->is_changing || sgl_val_eq(val, sgl, r->val)) {
    sgl_val_free(val, sgl);
    return true;
  }
  
  sgl_val_free(r->val, sgl);
  r->val = val;  
  r->is_changing = true;
  struct sgl_op *ret_pc = sgl->pc->prev;
  bool ok = true;

  sgl_ls_do(&r->watchers, struct sgl_val, ls, w) {
    if (sgl_val_call(w, sgl, sgl_pos(sgl), ret_pc, true) != ret_pc->next) {
      ok = false;
      break;
    }

    struct sgl_val *res = sgl_pop(sgl);
    if (!res) { break; }

    if (res->type != sgl->Bool) {      
      sgl_error(sgl, sgl_pos(sgl),
                sgl_sprintf("Invalid watch result: %s", sgl_type_id(res->type)->id));

      ok = false;
      break;
    }

    if (!res->as_bool) {
      sgl_ls_delete(&w->ls);
      sgl_val_free(w, sgl);
    }
  }

  r->is_changing = false;
  return ok;
}
