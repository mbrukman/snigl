#ifndef SNIGL_HASH_MAP_H
#define SNIGL_HASH_MAP_H

#include "snigl/hash.h"

struct sgl;
struct sgl_val;

struct sgl_hash_map {
  struct sgl_ls ls;
  struct sgl_hash hash;
  sgl_int_t nrefs;
};

struct sgl_hash_map *sgl_hash_map_new(struct sgl *sgl);
void sgl_hash_map_deref(struct sgl_hash_map *m, struct sgl *sgl);

bool sgl_hash_map_put(struct sgl_hash_map *m,
                      struct sgl *sgl,
                      struct sgl_val *key,
                      struct sgl_val *val);

#endif
