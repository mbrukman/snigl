#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "snigl/buf.h"
#include "snigl/call.h"
#include "snigl/cemit.h"
#include "snigl/curry.h"
#include "snigl/form.h"
#include "snigl/list.h"
#include "snigl/op.h"
#include "snigl/ref.h"
#include "snigl/sgl.h"
#include "snigl/stack.h"
#include "snigl/str.h"
#include "snigl/struct.h"
#include "snigl/sym.h"
#include "snigl/trace.h"
#include "snigl/timer.h"
#include "snigl/try.h"
#include "snigl/types/struct.h"
#include "snigl/val.h"

struct sgl_op_type SGL_NOP,
  SGL_OP_BENCH,
  SGL_OP_CALL, SGL_OP_COUNT, SGL_OP_CURRY,
  SGL_OP_DEREF_BEG, SGL_OP_DEREF_END, SGL_OP_DEFER, SGL_OP_DISPATCH, SGL_OP_DROP,
  SGL_OP_DUP, SGL_OP_DUPL,
  SGL_OP_EVAL_BEG, SGL_OP_EVAL_END,
  SGL_OP_FIMP, SGL_OP_FIMP_CALL, SGL_OP_FIMP_RETS, SGL_OP_FOR,
  SGL_OP_GET_FIELD, SGL_OP_GET_VAR,
  SGL_OP_IDLE, SGL_OP_IF, SGL_OP_ITER,
  SGL_OP_JUMP,
  SGL_OP_LAMBDA, SGL_OP_LET_VAR, SGL_OP_LIST_BEG, SGL_OP_LIST_END,
  SGL_OP_NEW,
  SGL_OP_PAIR, SGL_OP_PEEK_REG, SGL_OP_POP_REG, SGL_OP_PUSH, SGL_OP_PUSH_REG,
  SGL_OP_PUT_FIELD,
  SGL_OP_RECALL, SGL_OP_REF, SGL_OP_RETURN, SGL_OP_ROTL, SGL_OP_ROTR,
  SGL_OP_SCOPE_BEG, SGL_OP_SCOPE_END, SGL_OP_STACK_LIST, SGL_OP_STACK_SWITCH,
  SGL_OP_STDOUT_BEG, SGL_OP_STDOUT_END, SGL_OP_STR_BEG, SGL_OP_STR_END,
  SGL_OP_STR_PUT, SGL_OP_SWAP, SGL_OP_SYNC, 
  SGL_OP_TASK_BEG, SGL_OP_TASK_END, SGL_OP_THROW, SGL_OP_TIMES, SGL_OP_TRY_BEG,
  SGL_OP_TRY_END, SGL_OP_TYPE,
  SGL_OP_YIELD,
  SGL_OP_WATCH_BEG, SGL_OP_WATCH_END;

static bool const_op(struct sgl_op *op) { return false; }

static bool trace_op(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  sgl_trace_clear(t, sgl);
  return false;
}

struct sgl_op_type *sgl_op_type_init(struct sgl_op_type *type, const char *id) {
  type->id = id;
  type->const_op = const_op;
  type->deinit_op = NULL;
  type->deval_op = NULL;
  type->dump_op = NULL;
  type->trace_op = trace_op;
  return type;
}

struct sgl_op *sgl_op_init(struct sgl_op *op,
                           struct sgl *sgl,
                           struct sgl_form *form,
                           struct sgl_op_type *type) {
  op->form = form;
  if (form) { form->nrefs++; }

  op->type = type;
  op->run = NULL;
  op->pc = 0;
  op->cemit_pc = -1;
  op->prev = op->next = NULL;
  
  if (sgl->ops.prev->prev != &sgl->ops) {
    struct sgl_op *prev_op = sgl_baseof(struct sgl_op, ls, sgl->ops.prev->prev);
    prev_op->next = op;
    op->prev = prev_op;
    op->pc = prev_op->pc+1;
  }

  if (sgl->ops.prev != &sgl->ops) {
    op->next = sgl->end_pc;
    sgl->end_pc->prev = op;
    sgl->end_pc->pc++;
  }

  sgl_ls_push(sgl->ops.prev, &op->ls);
  return op;
}

struct sgl_op *sgl_op_deinit(struct sgl_op *op, struct sgl *sgl) {
  if (op->type->deinit_op) { op->type->deinit_op(op, sgl); }
  if (op->form) { sgl_form_deref(op->form, sgl); }
  return op;
}

struct sgl_op *sgl_op_new(struct sgl *sgl,
                          struct sgl_form *form,
                          struct sgl_op_type *type) {
  return sgl_op_init(sgl_malloc(&sgl->op_pool), sgl, form, type);
}

void sgl_op_free(struct sgl_op *op, struct sgl *sgl) {
  sgl_free(&sgl->op_pool, sgl_op_deinit(op, sgl));
}

bool sgl_op_const(struct sgl_op *op) { return op->type->const_op(op); }

struct sgl_op *sgl_op_cinit(struct sgl_op *op,
                            struct sgl *sgl,
                            struct sgl_cemit *out) {
  if (op->cemit_pc != -1) { return op->next; }
  
  op->cemit_pc = out->pc++;
  if (sgl_form_cemit_id(op->form, sgl, out) == -1) { return sgl->end_pc; }

  if (!op->type->cinit_op) { 
    sgl_error(sgl, &op->form->pos,
              sgl_sprintf("cinit not implemented by op: %s", op->type->id));
    
    return sgl->end_pc;
  }

  return op->type->cinit_op(op, sgl, out);
}

struct sgl_op *sgl_op_cemit(struct sgl_op *op,
                            struct sgl *sgl,
                            struct sgl_cemit *out) {  
  if (!op->type->cemit_op) { return op->next; }
  return op->type->cemit_op(op, sgl, out);
}

void sgl_op_dump(struct sgl_op *op, struct sgl_buf *out) {
  sgl_buf_printf(out, "%5" SGL_INT " %-15s", op->pc, op->type->id);
  if (op->type->dump_op) { op->type->dump_op(op, out); }
  sgl_buf_printf(out, " next: %" SGL_INT "\n", op->next->pc);
}

bool sgl_op_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  return op->type->trace_op(op, sgl, t);
}

static bool fuse_jumps(struct sgl_op **pc, struct sgl *sgl, struct sgl_trace *t) {
  bool changed = false;
  
  for (struct sgl_op *op = (*pc)->next; op != sgl->end_pc; op = op->next) {
    if (op->type == &SGL_OP_JUMP) {
      op = *pc = op->as_jump.pc;
      changed = true;
    } else if (op->type != &SGL_NOP) {
      break;
    }
  }

  return changed;
}

static struct sgl_op *nop_cinit(struct sgl_op *op,
                                struct sgl *sgl,
                                struct sgl_cemit *out) { return op->next; }

static bool nop_const(struct sgl_op *op) { return true; }

static bool nop_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  return false;
}

static struct sgl_op *nop_run(struct sgl_op *op, struct sgl *sgl) { return op->next; }

void sgl_op_deval(struct sgl_op *op, struct sgl *sgl, struct sgl_val *val) {
  if (op->type->deval_op) {
    op->type->deval_op(op, sgl, val);
  } else {
    sgl_nop(op, sgl);
  }
}

void sgl_nop(struct sgl_op *op, struct sgl *sgl) {
  if (op->type->deinit_op) { op->type->deinit_op(op, sgl); }
  op->type = &SGL_NOP;
  op->run = nop_run;
  
  struct sgl_op *last = op;
  while (last->next != sgl->end_pc && last->type == &SGL_NOP) { last = last->next; }
  
  struct sgl_op *first = op;
  while (first != sgl->beg_pc) {
    first = first->prev;
    if (first->type != &SGL_NOP) { break; }
  }
  
  for (op = last->prev; ; op = op->prev) {
    op->next = last;
    if (op == first) { break; }
  }
}

struct sgl_op *sgl_nop_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_NOP);
  op->run = nop_run;
  return op;  
}

static struct sgl_op *bench_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *reps = sgl_pop(sgl);
  if (!reps) { return sgl->end_pc; }

  if (reps->type != sgl->Int) {
    sgl_error(sgl, &op->form->pos,
              sgl_sprintf("Invalid reps: %s", reps->type->def.id->id));
    
    return sgl->end_pc;
  }

  struct sgl_op
    *start_op = op->next,
    *end_op = op->as_bench.end_pc->next;

  struct sgl_task *task = sgl->task;
  struct sgl_ls *stack = sgl_reg_stack(sgl);
  sgl_val_dup(reps, sgl, stack);
  if (!sgl_run_task(sgl, task, start_op, end_op)) { return sgl->end_pc; }

  struct sgl_timer t;
  sgl_ls_push(stack, &reps->ls);
  sgl_timer_init(&t);
  if (!sgl_run_task(sgl, task, start_op, end_op)) { return sgl->end_pc; }
  sgl_push(sgl, sgl->Int)->as_int = sgl_timer_ms(&t);
  return op->as_bench.end_pc->next;
}

struct sgl_op *sgl_op_bench_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_BENCH);
  op->run = bench_run;
  op->as_bench.end_pc = NULL;
  return op;
}

static void call_deinit(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *v = op->as_call.val;
  if (v) { sgl_val_free(v, sgl); }
}

static void call_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_val *v = op->as_call.val;

  if (v) {
    sgl_buf_putc(out, ' ');
    sgl_val_dump(v, out);
  }
}

static bool call_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_op_call *c = &op->as_call;
  bool changed = false;
  if (c->val) { goto exit; }
  struct sgl_val *v = c->val = sgl_trace_pop(t, sgl);

  if (v) {
    assert(v->op);
    sgl_op_deval(v->op, sgl, v);
    changed = true;
  }
 exit:
  sgl_trace_clear(t, sgl);
  return changed;
}

static struct sgl_op *call_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_call *c = &op->as_call;
  struct sgl_val *v = c->val;
  if (!v && !(v = sgl_pop(sgl))) { return NULL; }
  struct sgl_fimp *prev_cfimp = sgl->c_fimp;
  sgl->c_fimp = sgl->call_fimp;
  struct sgl_op *pc = sgl_val_call(v, sgl, &op->form->pos, op, false);
  sgl->c_fimp = prev_cfimp;
  if (!c->val) { sgl_val_free(v, sgl); }
  return pc;
}

struct sgl_op *sgl_op_call_new(struct sgl *sgl,
                               struct sgl_form *form,
                               struct sgl_val *val) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_CALL);
  op->run = call_run;
  op->as_call.val = val;
  return op;
}

void sgl_op_call_reuse(struct sgl_op *op, struct sgl *sgl) {
  if (op->type->deinit_op) { op->type->deinit_op(op, sgl); }
  op->type = &SGL_OP_CALL;
  op->run = call_run;
  op->as_call.val = NULL;
}

static struct sgl_op *count_cinit(struct sgl_op *op,
                                  struct sgl *sgl,
                                  struct sgl_cemit *out) {
  sgl_int_t form_id = op->form->cemit_id;
  
  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT " = "
                 "sgl_op_count_new(sgl, form%" SGL_INT ", op%" SGL_INT ");",
                 op->cemit_pc, form_id, op->as_count.pc->cemit_pc);
  
  return op->next;
}

static bool count_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  sgl_trace_clear(t, sgl);
  return false;
}

static struct sgl_op *count_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *v = sgl_peek_reg(sgl, false, NULL);
  
  if (!--v->as_int) {
    sgl_ls_delete(&v->ls);
    sgl_val_free(v, sgl);
    return op->next;
  }
  
  return op->as_count.pc->next;
}

struct sgl_op *sgl_op_count_new(struct sgl *sgl,
                                struct sgl_form *form,
                                struct sgl_op *pc) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_COUNT);
  op->run = count_run;
  op->as_count.pc = pc;
  return op;
}

static void curry_deinit(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *as = op->as_curry.args;
  if (as) { sgl_val_free(as, sgl); }
}

static void curry_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_val *as = op->as_curry.args;

  if (as) {
    sgl_buf_putc(out, ' ');
    sgl_val_dump(as, out);
  }
}

static bool curry_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  sgl_trace_clear(t, sgl);
  return false;
}

static struct sgl_op *curry_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_pos *pos = &op->form->pos;
  struct sgl_op_curry *c = &op->as_curry;
  struct sgl_val *args = c->args;

  if (!args) {
    args = sgl_pop(sgl);

    if (!args) {
      sgl_error(sgl, pos, sgl_strdup("Missing curry args"));
      return sgl->end_pc;
    }

    assert(args->type == sgl->List);

    if (!sgl_list_len(args->as_list)) {
      sgl_error(sgl, pos, sgl_strdup("Missing curry args"));
      return sgl->end_pc;
    }
  }

  struct sgl_val *target = sgl_pop(sgl);

  if (!target) {
    sgl_error(sgl, pos, sgl_strdup("Missing curry target"));
    return sgl->end_pc;
  }

  struct sgl_list *l = args->as_list;

  if (c->args) {
    struct sgl_list *src = l;
    l = sgl_list_new(sgl);
    
    sgl_ls_do(&src->root, struct sgl_val, ls, a) {
      sgl_val_clone(a, sgl, pos, &l->root);
    }
  } else {
    l->nrefs++;
  }
  
  sgl_push(sgl, sgl->Curry)->as_curry = sgl_curry_new(sgl, target, l);
  if (!c->args) { sgl_val_free(args, sgl); }
  return op->next;
}

struct sgl_op *sgl_op_curry_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_CURRY);
  op->run = curry_run;
  op->as_curry.args = NULL;
  return op;
}

static struct sgl_op *deref_beg_cinit(struct sgl_op *op,
                                  struct sgl *sgl,
                                  struct sgl_cemit *out) {
  sgl_int_t form_id = op->form->cemit_id;
  
  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT " = "
                 "sgl_op_deref_beg_new(sgl, form%" SGL_INT ");",
                 op->cemit_pc, form_id);
  
  return op->next;
}

static void deref_beg_deinit(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *v = op->as_deref_beg.val;
  if (v) { sgl_val_free(v, sgl); }
}

static void deref_beg_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_val *v = op->as_deref_beg.val;

  if (v) {
    sgl_buf_putc(out, ' ');
    sgl_val_dump(v, out);
  }
}

static bool deref_beg_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_op_deref_beg *d = &op->as_deref_beg;
  struct sgl_pos *pos = &op->form->pos;
  if (d->val) { return false; }
  struct sgl_val *v = sgl_trace_pop(t, sgl);
  if (!v) { return false; }

  if (v->type != sgl->Ref) {
    sgl_error(sgl, pos,
              sgl_sprintf("Expected Ref, was: %s", sgl_type_id(v->type)->id));
    
    return sgl->end_pc;
  }

  assert(v->op);
  sgl_op_deval(v->op, sgl, v);
  d->val = v;
  return true;
}

static struct sgl_op *deref_beg_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_deref_beg *d = &op->as_deref_beg;
  struct sgl_pos *pos = &op->form->pos;
  struct sgl_val *v = d->val ? d->val : sgl_pop(sgl);

  if (!v) {
    sgl_error(sgl, pos, sgl_strdup("Nothing to deref_beg"));
    return sgl->end_pc;
  }

  if (v->type != sgl->Ref) {
    sgl_error(sgl, pos,
              sgl_sprintf("Expected Ref, was: %s", sgl_type_id(v->type)->id));
    
    return sgl->end_pc;
  }

  sgl_val_dup(sgl_ref_get(v->as_ref, sgl), sgl, sgl_stack(sgl));

  if (d->val) {
    sgl_val_dup(v, sgl, sgl_reg_stack(sgl));
  } else {
    sgl_ls_push(sgl_reg_stack(sgl), &v->ls);
  }
  
  return op->next;
}

struct sgl_op *sgl_op_deref_beg_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_DEREF_BEG);
  op->run = deref_beg_run;
  op->as_deref_beg.val = NULL;
  return op;
}

static struct sgl_op *deref_end_cinit(struct sgl_op *op,
                                struct sgl *sgl,
                                struct sgl_cemit *out) {
  sgl_int_t form_id = op->form->cemit_id;
  
  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT " = "
                 "sgl_op_deref_end_new(sgl, form%" SGL_INT ");",
                 op->cemit_pc, form_id);
  
  return op->next;
}

static void deref_end_deinit(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *v = op->as_deref_end.val;
  if (v) { sgl_val_free(v, sgl); }
}

static bool deref_end_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_op_deref_end *r = &op->as_deref_end;
  if (r->val) { return false; }
  struct sgl_val *v = sgl_trace_pop(t, sgl);
  if (!v) { return false; }
  assert(v->op);
  sgl_op_deval(v->op, sgl, v);
  r->val = v;
  return true;
}

static struct sgl_op *deref_end_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_deref_end *r = &op->as_deref_end;
  struct sgl_pos *pos = &op->form->pos;
  struct sgl_val *v = r->val ? sgl_val_dup(r->val, sgl, NULL) : sgl_pop(sgl);

  if (!v) {
    sgl_error(sgl, pos, sgl_strdup("Nothing to deref_end"));
    return sgl->end_pc;
  }

  struct sgl_val *rv = sgl_pop_reg(sgl);
  assert(rv);
  if (!sgl_ref_set(rv->as_ref, sgl, v)) { return sgl->end_pc; }
  sgl_val_free(rv, sgl);
  return op->next;
}

struct sgl_op *sgl_op_deref_end_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_DEREF_END);
  op->run = deref_end_run;
  op->as_deref_end.val = NULL;
  return op;
}

static struct sgl_op *defer_cinit(struct sgl_op *op,
                                struct sgl *sgl,
                                struct sgl_cemit *out) {
  sgl_int_t form_id = op->form->cemit_id;
  
  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT " = "
                 "sgl_op_defer_new(sgl, form%" SGL_INT ");",
                 op->cemit_pc, form_id);

  struct sgl_op *end_pc = op->as_defer.end_pc;
  sgl_op_cinit(end_pc, sgl, out);  
               
  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT "->as_defer.end_pc = op%" SGL_INT ";",
                 op->cemit_pc, end_pc->cemit_pc);

  return op->next;
}

static bool defer_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  return fuse_jumps(&op->as_defer.end_pc, sgl, t);
}

static struct sgl_op *defer_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_defer *d = &op->as_defer;
  sgl_defer(sgl, d);
  return d->end_pc->next;
}

struct sgl_op *sgl_op_defer_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_DEFER);
  op->run = defer_run;
  op->as_defer = (struct sgl_op_defer){.beg_pc = op, .end_pc = NULL};
  return op;
}

static struct sgl_op *dispatch_cinit(struct sgl_op *op,
                                     struct sgl *sgl,
                                     struct sgl_cemit *out) {
  struct sgl_pos *pos = &op->form->pos;
  struct sgl_op_dispatch *d = &op->as_dispatch;
  
  sgl_int_t func_id = sgl_func_cemit_id(d->func, sgl, pos, out);
  if (func_id == -1) { return sgl->end_pc; }

  sgl_int_t fimp_id = d->fimp
    ? sgl_fimp_cemit_id(d->fimp, sgl, pos, out)
    : out->fimp_id++;

  if (fimp_id == -1) { return sgl->end_pc; }
  sgl_int_t form_id = op->form->cemit_id;

  if (!d->fimp) {
    sgl_cemit_line(out, "struct sgl_fimp *fimp%" SGL_INT " = NULL;", fimp_id);
  }
  
  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT " = "
                 "sgl_op_dispatch_new(sgl, form%" SGL_INT ", "
                 "func%" SGL_INT ", "
                 "fimp%" SGL_INT");",
                 op->cemit_pc, form_id, func_id, fimp_id);
  
  return op->next;
}

static void dispatch_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_op_dispatch *d = &op->as_dispatch;
  sgl_buf_printf(out, " %s", d->func->def.id->id);
  if (d->fimp) { sgl_buf_printf(out, "(%s)", d->fimp->id->id); }
}

static bool dispatch_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_op_dispatch *d = &op->as_dispatch;
  struct sgl_func *func = d->func;
  struct sgl_fimp *fimp = d->fimp;

  if (t->len >= func->nargs) {
    if (sgl_trace_is_undef(t, sgl, func->nargs)) {
      if (fimp) {
        if (!sgl_fimp_match_types(fimp, sgl, &t->stack)) { goto fail; }
      } else if (!(fimp = sgl_func_dispatch_types(func, sgl, &t->stack))) {
        goto fail;
      }
    } else {
      if (fimp) {
        if (!sgl_fimp_match(d->fimp, sgl, &t->stack)) { goto fail; }
      } else if (!(fimp = sgl_func_dispatch(func, sgl, &t->stack))) {
        goto fail;
      }
    }
  } else {
    goto fail;
  }

  if (!sgl_fimp_compile(fimp, sgl, &op->form->pos)) {
    sgl_trace_clear(t, sgl);
    return false;
  }

  sgl_op_fimp_call_reuse(op, sgl, fimp);
  sgl_op_trace(op, sgl, t);
  return true;

 fail:
  sgl_trace_drop(t, sgl, func->nargs);

  if (fimp) {
    for (struct sgl_type **r = fimp->rets; r < fimp->rets + fimp->nrets; r++) {
      sgl_trace_push_undef(t, sgl, *r);
    }
  } else {
    for (struct sgl_type **r = func->rets; r < func->rets + func->nrets; r++) {
      sgl_trace_push_undef(t, sgl, *r);
    }
  }

  return false; 
}

static struct sgl_op *dispatch_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_ls *stack = sgl_stack(sgl);
  struct sgl_op_dispatch *d = &op->as_dispatch;
  struct sgl_fimp *fimp = d->fimp;
  
  if (fimp) {
    if (!sgl_fimp_match(fimp, sgl, stack)) {
      sgl_error(sgl, &op->form->pos,
                sgl_sprintf("Fimp not applicable: %s (%s)",
                            d->func->def.id->id, fimp->id->id));
      
      return sgl->end_pc;
    }
  } else {
    fimp = sgl_func_dispatch(d->func, sgl, stack);
    
    if (!fimp) {
      sgl_error(sgl, &op->form->pos,
                sgl_sprintf("Func not applicable: %s", d->func->def.id->id));
      
      return sgl->end_pc;
    }
  }
    
  struct sgl_op *pc = sgl_fimp_call(fimp, sgl, &op->form->pos, op);
  return pc ? pc : sgl->end_pc;
}

struct sgl_op *sgl_op_dispatch_new(struct sgl *sgl,
                                   struct sgl_form *form,
                                   struct sgl_func *func,
                                   struct sgl_fimp *fimp) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_DISPATCH);
  op->run = dispatch_run;
  op->as_dispatch = (struct sgl_op_dispatch){.func = func, .fimp = fimp};
  return op;
}

static bool drop_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_op_drop *d = &op->as_drop;
  bool changed = false;
  
  while (d->nvals) {
    struct sgl_val *v = sgl_trace_pop(t, sgl);
    if (!v) { break; }
    sgl_op_deval(v->op, sgl, v);
    d->nvals--;
    changed = true;
  }

  if (d->nvals) {
    sgl_trace_drop(t, sgl, d->nvals);
  } else {
    sgl_nop(op, sgl);
    changed = true;
  }
  
  return changed;
}

static struct sgl_op *drop_run(struct sgl_op *op, struct sgl *sgl) {
  sgl_int_t nvals = op->as_drop.nvals;
  
  return (sgl_stack_drop(sgl_stack(sgl), sgl, nvals, false) == nvals)
    ? op->next
    : sgl->end_pc;
}

struct sgl_op *sgl_op_drop_new(struct sgl *sgl,
                               struct sgl_form *form,
                               sgl_int_t nvals) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_DROP);
  op->run = drop_run;
  op->as_drop.nvals = nvals;
  return op;
}

static struct sgl_op *dup_cinit(struct sgl_op *op,
                                struct sgl *sgl,
                                struct sgl_cemit *out) {
  sgl_int_t form_id = op->form->cemit_id;
  
  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT " = "
                 "sgl_op_dup_new(sgl, form%" SGL_INT ");",
                 op->cemit_pc, form_id);
  
  return op->next;
}

static bool dup_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  if (op->next->type == &SGL_OP_ROTL) {
    sgl_op_dupl_reuse(op->next, sgl);
    sgl_op_trace(op->next, sgl, t);
    sgl_nop(op, sgl);
    return true;
  }

  sgl_trace_dup(t, sgl, op);
  return false;
}

static struct sgl_op *dup_run(struct sgl_op *op, struct sgl *sgl) {
  return sgl_stack_dup(sgl_stack(sgl), sgl, false) ? op->next : sgl->end_pc;
}

struct sgl_op *sgl_op_dup_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_DUP);
  op->run = dup_run;
  return op;
}

static struct sgl_op *dupl_cinit(struct sgl_op *op,
                                struct sgl *sgl,
                                struct sgl_cemit *out) {
  sgl_int_t form_id = op->form->cemit_id;
  
  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT " = "
                 "sgl_op_dupl_new(sgl, form%" SGL_INT ");",
                 op->cemit_pc, form_id);
  
  return op->next;
}

static void dupl_deval(struct sgl_op *op, struct sgl *sgl, struct sgl_val *val) {
  sgl_op_rotl_reuse(op, sgl);
}

static bool dupl_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  sgl_trace_dupl(t, sgl, op);
  return false;
}

static struct sgl_op *dupl_run(struct sgl_op *op, struct sgl *sgl) {
  return sgl_stack_dupl(sgl_stack(sgl), sgl, false) ? op->next : sgl->end_pc;
}

struct sgl_op *sgl_op_dupl_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_DUPL);
  op->run = dupl_run;
  return op;
}

void sgl_op_dupl_reuse(struct sgl_op *op, struct sgl *sgl) {
  if (op->type->deinit_op) { op->type->deinit_op(op, sgl); }
  op->type = &SGL_OP_DUPL;
  op->run = dupl_run;
}

static void eval_beg_dump(struct sgl_op *op, struct sgl_buf *out) {
  sgl_buf_printf(out, " end: %" SGL_INT, op->as_eval_beg.end_pc->pc);
}

static bool eval_beg_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_op_eval_beg *fb = &op->as_eval_beg;
  struct sgl_op *beg_pc = op;
  bool nop = true, changed = false;
  
  for (op = beg_pc->next; op != fb->end_pc; op = op->next) {
    nop &=
      op->type != &SGL_OP_FIMP &&
      op->type != &SGL_OP_LAMBDA &&
      op->type != &SGL_OP_TASK_BEG;
  }

  if (nop) {
    for (op = beg_pc; op != fb->end_pc->next; op = op->next) { sgl_nop(op, sgl); }
    changed = true;
  } else {
    for (op = fb->end_pc->next; t->len && op != sgl->end_pc; op = op->next) {
      changed |= sgl_op_trace(op, sgl, t);
    }
    
    sgl_trace_clear(t, sgl);
  }
  
  return changed;
}

static struct sgl_op *eval_beg_run(struct sgl_op *op, struct sgl *sgl) {
  return op->as_eval_beg.end_pc->next;
}

struct sgl_op *sgl_op_eval_beg_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_EVAL_BEG);
  op->run = eval_beg_run;
  op->as_eval_beg.end_pc = NULL;
  return op;
}

static struct sgl_op *eval_end_run(struct sgl_op *op, struct sgl *sgl) {
  return op->next;
}

struct sgl_op *sgl_op_eval_end_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_EVAL_END);
  op->run = eval_end_run;
  return op;
}

static struct sgl_op *fimp_cinit(struct sgl_op *op,
                                 struct sgl *sgl,
                                 struct sgl_cemit *out) {
  struct sgl_fimp *f = op->as_fimp.fimp;
  struct sgl_pos *pos = &op->form->pos;
  sgl_int_t form_id = op->form->cemit_id;
  sgl_int_t fimp_id = sgl_fimp_cemit_id(f, sgl, pos, out);
  if (fimp_id == -1) { return sgl->end_pc; }

  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT " = "
                 "sgl_op_fimp_new(sgl, form%" SGL_INT ", fimp%" SGL_INT ");",
                 op->cemit_pc, form_id, fimp_id);
  
  sgl_cemit_line(out,
                 "fimp%" SGL_INT "->imp.beg_pc = op%" SGL_INT ";",
                 fimp_id, op->cemit_pc);

  if (f->imp.end_pc == f->imp.beg_pc) {
    sgl_cemit_line(out,
                   "fimp%" SGL_INT "->imp.end_pc = op%" SGL_INT ";",
                   fimp_id, op->cemit_pc);

    return op->next;
  }

  for (op = op->next;
       op != f->imp.end_pc && op != sgl->end_pc;
       op = sgl_op_cinit(op, sgl, out));
  
  if (op == sgl->end_pc) { return sgl->end_pc; }
  struct sgl_op *end_pc = op;
  op = sgl_op_cinit(op, sgl, out);
  if (op == sgl->end_pc) { return sgl->end_pc; }
  
  sgl_cemit_line(out,
                 "fimp%" SGL_INT "->imp.end_pc = op%" SGL_INT ";",
                 fimp_id, end_pc->cemit_pc);

  return op;
}

static void fimp_dump(struct sgl_op *op, struct sgl_buf *out) {
  sgl_buf_printf(out, " end: %" SGL_INT, op->as_fimp.fimp->imp.end_pc->pc);
}

static bool fimp_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_fimp *f = op->as_fimp.fimp;
  bool changed = false;
  
  if (t->len) {
    for (op = f->imp.end_pc->next;
         t->len && op != sgl->end_pc;
         op = op->next) { changed |= sgl_op_trace(op, sgl, t); }

    sgl_trace_clear(t, sgl);
  }

  return changed;
}

static struct sgl_op *fimp_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_fimp *f = op->as_fimp.fimp;
  struct sgl_op *next = op->next;
  
  if (next->type == &SGL_OP_SCOPE_BEG && !next->as_scope_beg.parent) {
    struct sgl_scope *s = sgl_scope(sgl);
    next->as_scope_beg.parent = s;
    s->nrefs++;
  }

  return f->imp.end_pc->next;
}

struct sgl_op *sgl_op_fimp_new(struct sgl *sgl,
                               struct sgl_form *form,
                               struct sgl_fimp *fimp) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_FIMP);
  op->run = fimp_run;
  op->as_fimp.fimp = fimp;
  return op;
}

static struct sgl_op *fimp_call_cinit(struct sgl_op *op,
                                      struct sgl *sgl,
                                      struct sgl_cemit *out) {
  struct sgl_pos *pos = &op->form->pos;
  sgl_int_t fimp_id = sgl_fimp_cemit_id(op->as_fimp_call.fimp, sgl, pos, out);
  if (fimp_id == -1) { return sgl->end_pc; }
  sgl_int_t form_id = op->form->cemit_id;
  
  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT " = "
                 "sgl_op_fimp_call_new(sgl, form%" SGL_INT ", fimp%" SGL_INT ");",
                 op->cemit_pc, form_id, fimp_id);
  
  return op->next;
}

static void fimp_call_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_fimp *f = op->as_fimp_call.fimp;
  sgl_buf_printf(out, " %s(%s)", f->func->def.id->id, f->id->id);
}

static bool fimp_call_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_op_fimp_call *c = &op->as_fimp_call;
  struct sgl_fimp *f = c->fimp;

  if (f == sgl->call_fimp) {
    sgl_op_call_reuse(op, sgl);
    sgl_op_trace(op, sgl, t);
    return true;
  }
  
  struct sgl_imp *i = &f->imp;
  f->trace_depth++;
  bool changed = false;
  
  if (f->cimp || i->end_pc == i->beg_pc) {
    sgl_trace_drop(t, sgl, f->func->nargs);

    if (f->nrets == -1) {
      sgl_trace_clear(t, sgl);
    } else {
      for (struct sgl_type **r = f->rets; r < f->rets + f->nrets; r++) {
        sgl_trace_push_undef(t, sgl, *r);
      }
    }
  } else {
    for (struct sgl_arg *a = f->args + t->len; a < f->args + f->func->nargs; a++) {
      sgl_trace_push_undef(t, sgl, a->type); 
    }

    if (f->trace_depth == 1) {  
      for (op = i->beg_pc->next;
           op != i->end_pc;
           op = op->next) { changed |= sgl_op_trace(op, sgl, t); }

      sgl_trace_undef(t, sgl, (f->nrets == -1) ? t->len : f->nrets);
    } else {
      sgl_trace_clear(t, sgl);
    }

    for (struct sgl_type **r = f->rets + t->len; r < f->rets + f->nrets; r++) {
      sgl_trace_push_undef(t, sgl, *r);
    }
  }
  
  f->trace_depth--;
  return changed;
}

static struct sgl_op *fimp_call_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op *pc = sgl_fimp_call(op->as_fimp_call.fimp, sgl, &op->form->pos, op);
  return pc ? pc : sgl->end_pc;
}

struct sgl_op *sgl_op_fimp_call_new(struct sgl *sgl,
                                    struct sgl_form *form,
                                    struct sgl_fimp *fimp) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_FIMP_CALL);
  op->run = fimp_call_run;
  op->as_fimp_call = (struct sgl_op_fimp_call){.fimp = fimp};
  return op;
}

static struct sgl_op *fimp_rets_cinit(struct sgl_op *op,
                                      struct sgl *sgl,
                                      struct sgl_cemit *out) {
  struct sgl_pos *pos = &op->form->pos;
  struct sgl_op_fimp_rets *fr = &op->as_fimp_rets;  
  struct sgl_fimp *f = fr->fimp;
  sgl_int_t fimp_id = sgl_fimp_cemit_id(f, sgl, pos, out);
  if (fimp_id == -1) { return sgl->end_pc; }
  sgl_int_t form_id = op->form->cemit_id;
  
  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT " = "
                 "sgl_op_fimp_rets_new(sgl, form%" SGL_INT ", fimp%" SGL_INT ");",
                 op->cemit_pc, form_id, fimp_id);

  for (sgl_int_t i = 0; i < f->nrets; i++) {
    if (fr->check[i]) { continue; }
    
    sgl_cemit_line(out,
                   "*op%" SGL_INT "->as_fimp_rets.check[%" SGL_INT "] = false;",
                   op->cemit_pc, i);
  }

  return op->next;
}

static void fimp_rets_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_op_fimp_rets *fr = &op->as_fimp_rets;  
  struct sgl_fimp *f = fr->fimp;
  sgl_buf_putcs(out, " (");
  bool *c = fr->check;
  
  for (struct sgl_type **r = f->rets; r < f->rets + f->nrets; r++, c++) {
    if (r > f->rets) { sgl_buf_putc(out, ' '); }

    if (*c) {
      sgl_buf_putcs(out, sgl_type_id((*r))->id);
    } else {
      sgl_buf_putc(out, '_');
    }
  }

  sgl_buf_putc(out, ')');
}

static bool fimp_rets_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_op_fimp_rets *fr = &op->as_fimp_rets;
  struct sgl_fimp *f = fr->fimp;
  struct sgl_type **r = f->rets + f->nrets - 1;
  struct sgl_ls *s = &t->stack, *vl = s->prev;
  sgl_int_t ncheck = 0;
  bool changed = false;
  
  for (bool *c = fr->check + f->nrets - 1;
       c >= fr->check;
       c--, r--, vl = vl->prev) {
    if (vl == s) { return changed; }
    
    if (*c) {
      if (sgl_derived(sgl_baseof(struct sgl_val, ls, vl)->type, *r)) {
        *c = false;
        changed = true;
      } else {
        ncheck++;
      }
    }
  }

  if (!ncheck) { sgl_nop(op, sgl); }
  return changed;
}

static struct sgl_op *fimp_rets_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_pos *pos = &op->form->pos;
  struct sgl_op_fimp_rets *fr = &op->as_fimp_rets;  
  struct sgl_fimp *f = fr->fimp;
  struct sgl_ls *s = sgl_stack(sgl), *vl = s->prev;
  struct sgl_type **r = f->rets + f->nrets - 1;
  
  for (bool *c = fr->check + f->nrets - 1; c >= fr->check; c--, r--, vl = vl->prev) {
    if (vl == s) {
      sgl_error(sgl, pos, sgl_sprintf("Missing func result: %s", sgl_type_id((*r))));
      return sgl->end_pc;
    }

    if (!c) { continue; }    
    struct sgl_val *v = sgl_baseof(struct sgl_val, ls, vl);

    if (!sgl_derived(v->type, *r)) {
      sgl_error(sgl, pos,
                sgl_sprintf("Invalid func result, expected %s: %s",
                            sgl_type_id((*r))->id, sgl_type_id(v->type)->id));
      
      return sgl->end_pc;
    }
  }
  
  return op->next;
}

struct sgl_op *sgl_op_fimp_rets_new(struct sgl *sgl,
                                    struct sgl_form *form,
                                    struct sgl_fimp *fimp) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_FIMP_RETS);
  op->run = fimp_rets_run;
  op->as_fimp_rets = (struct sgl_op_fimp_rets){.fimp = fimp, .check = {1}};
  return op;
}

void sgl_op_fimp_call_reuse(struct sgl_op *op,
                            struct sgl *sgl,
                            struct sgl_fimp *fimp) {
  if (op->type->deinit_op) { op->type->deinit_op(op, sgl); }
  op->type = &SGL_OP_FIMP_CALL;
  op->run = fimp_call_run;
  op->as_fimp_call = (struct sgl_op_fimp_call){.fimp = fimp};
}

static void for_deinit(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *s = op->as_for.src;
  if (s) { sgl_val_free(s, sgl); }
}

static void for_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_val *s = op->as_for.src;

  if (s) {
    sgl_buf_putc(out, ' ');
    sgl_val_dump(s, out);
  }
}

static bool for_trace(struct sgl_op *op,
                      struct sgl *sgl,
                      struct sgl_trace *t) {
  struct sgl_op_for *f = &op->as_for;
  struct sgl_trace *t_iter = sgl_trace_new(sgl);
  sgl_trace_copy(t, sgl, t_iter);
  sgl_trace_push_undef(t, sgl, sgl->T);
  bool ok = false, changed = false;

  for (struct sgl_op *iop = op->next;
       (!ok || t_iter->len) && iop != sgl->end_pc;
       iop = iop->next) {
    if (iop->type == &SGL_OP_ITER && iop->as_iter.pc == op) {
      sgl_trace_sync(t, sgl, t_iter);
      sgl_trace_undef(t_iter, sgl, t_iter->len);
      ok = true;
      continue;
    }
    
    changed |= sgl_op_trace(iop, sgl, t_iter);
  }
  
  sgl_trace_free(t_iter, sgl);
  if (f->src) { return changed; }
  struct sgl_val *s = f->src = sgl_trace_pop(t, sgl);
  if (!s) { return changed; }
  assert(s->op);
  sgl_op_deval(s->op, sgl, s);
  return true;
}

static struct sgl_op *for_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_for *f = &op->as_for; 
  struct sgl_val *s = op->as_for.src;

  if (!s) {
    s = sgl_pop(sgl);
    if (!s) { return sgl->end_pc; }
  }
  
  struct sgl_type *it = NULL;
  struct sgl_iter *i = sgl_val_iter(s, sgl, &it);

  if (!i) {
    if (!f->src) { sgl_val_free(s, sgl); }
    return sgl->end_pc;
  }

  if (f->src) {
    s = sgl_val_new(sgl, it, sgl_reg_stack(sgl));
    s->as_iter = i;
  } else {
    sgl_val_reuse(s, sgl, it, true)->as_iter = i;
    sgl_ls_push(sgl_reg_stack(sgl), &s->ls);
  }

  if (!sgl_iter_next_val(i, sgl, &op->form->pos, sgl_stack(sgl))) {
    sgl_ls_delete(&s->ls);
    sgl_val_free(s, sgl);
    return op->as_for.end_pc->next;
  }
  
  return op->next;
}

struct sgl_op *sgl_op_for_new(struct sgl *sgl,
                              struct sgl_form *form,
                              struct sgl_op *end_pc) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_FOR);
  op->run = for_run;
  op->as_for = (struct sgl_op_for){.end_pc = end_pc, .src = NULL};
  return op;
}

static bool init_field(struct sgl_op_field *f,
                       struct sgl *sgl,
                       struct sgl_pos *pos,
                       struct sgl_sym *id) {
  char
    *start = id->id,
    *div = strchr(start, '/');
  
  if (div) {
    struct sgl_sym *tid = sgl_nsym(sgl, start, div - start);
    f->id = sgl_sym(sgl, div+1);
    
    f->struct_type = sgl_find_type(sgl, pos, tid);
    
    if (!f->struct_type) {
      sgl_error(sgl, pos, sgl_sprintf("Unknown type: %s", tid->id));
      return false;
    }
    
    struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, f->struct_type);
    struct sgl_struct_type *st = sgl_baseof(struct sgl_struct_type, ref_type, rt);
    f->field = sgl_struct_type_find(st, f->id);
    
    if (!f->field) {
      sgl_error(sgl, pos, sgl_sprintf("Unknown field: %s", f->id->id));
      return false;
    }
  } else {
    f->struct_type = NULL;
    f->field = NULL;
    f->id = id;
  }

  f->val = NULL;
  f->use_reg = false;
  return true;
}

static void field_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_op_field *f = &op->as_field;
  
  if (f->field) {
    sgl_buf_printf(out, " %s.%s",
                   sgl_type_id(f->struct_type)->id,
                   f->field->id->id);
  } else {
    sgl_buf_printf(out, " %s", f->id->id);
  }

  if (f->val) {
    sgl_buf_putc(out, ' ');
    sgl_val_dump(f->val, out);
  }

  if (f->use_reg) { sgl_buf_putcs(out, " use-reg"); }
}

static bool get_field_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_field *f = op->as_field.field;
  sgl_trace_push_undef(t, sgl, f ? f->type : sgl->A);
  return false;
}

static struct sgl_op *get_field_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_field *gf = &op->as_field;
  struct sgl_val *sv = gf->use_reg ? sgl_peek_reg(sgl, true, NULL) : sgl_pop(sgl);
  if (!sv) { return sgl->end_pc; }
  struct sgl_type *t = sv->type, *st = gf->struct_type;
  struct sgl_field *f = gf->field;
  
  if (!f) {
    if (!sgl_derived(t, sgl->Struct)) {
      sgl_error(sgl, &op->form->pos,
                sgl_sprintf("Expected struct, was: %s", t->def.id->id));
      
      return sgl->end_pc;
    }


    struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, t);
    struct sgl_struct_type *st = sgl_baseof(struct sgl_struct_type, ref_type, rt);
    struct sgl_sym *id = gf->id;
    f = sgl_struct_type_find(st, id);
    
    if (!f) {
      sgl_error(sgl, &op->form->pos, sgl_sprintf("Unknown field: %s", id->id));
      return sgl->end_pc;
    }
  } else if (t != st) {
    sgl_error(sgl, &op->form->pos,
              sgl_sprintf("Expected %s, was: %s", st->def.id->id, t->def.id->id));
    
    return sgl->end_pc;
  }

  struct sgl_val **v = sv->as_struct->fields + f->i;

  if (*v) {
    sgl_val_dup(*v, sgl, sgl_stack(sgl));
  } else {
    sgl_push(sgl, sgl->Nil);
  }
  
  sgl_val_free(sv, sgl);
  return op->next;
}

struct sgl_op *sgl_op_get_field_new(struct sgl *sgl,
                                    struct sgl_form *form,
                                    struct sgl_sym *id) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_GET_FIELD);
  op->run = get_field_run;

  if (!init_field(&op->as_field, sgl, &form->pos, id)) {
    sgl_op_free(op, sgl);
    return NULL;
  }
  
  return op;
}

static void get_var_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_op_get_var *gv = &op->as_get_var;
  sgl_buf_printf(out, " %s", gv->id->id);

  if (gv->val) {
    sgl_buf_putc(out, ' ');
    sgl_val_dump(gv->val, out);
  }
}

static bool get_var_const(struct sgl_op *op) { return op->as_get_var.val; }

static bool get_var_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_op_get_var *gv = &op->as_get_var;
  struct sgl_val *v = gv->val;
  
  if (v) {
    sgl_trace_push(t, sgl, op, gv->val);
  } else {
    sgl_trace_push_undef(t, sgl, sgl->T);
  }
  
  return false;
}

static struct sgl_op *get_var_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_get_var *gv = &op->as_get_var;
  struct sgl_val *v = gv->val;

  if (!v) {
    v = sgl_get_var(sgl, gv->id);    
  
    if (!v) {
      sgl_error(sgl, &op->form->pos, sgl_sprintf("Unknown var: %s", gv->id->id));
      return sgl->end_pc;
    }
  }

  sgl_val_dup(v, sgl, sgl_stack(sgl));
  return op->next;
}

struct sgl_op *sgl_op_get_var_new(struct sgl *sgl,
                                  struct sgl_form *form,
                                  struct sgl_sym *id) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_GET_VAR);
  op->run = get_var_run;
  op->as_get_var = (struct sgl_op_get_var){.id = id, .val = NULL};
  return op;
}

static bool iter_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  sgl_trace_clear(t, sgl);
  return false;
}

static struct sgl_op *iter_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *v = sgl_peek_reg(sgl, false, NULL);

  if (!v) {
    sgl_error(sgl, &op->form->pos, sgl_strdup("Missing iter"));
    return sgl->end_pc;
  }
  
  if (!sgl_iter_next_val(v->as_iter, sgl, &op->form->pos, sgl_stack(sgl))) {
    sgl_ls_delete(&v->ls);
    sgl_val_free(v, sgl);
    return op->next;
  }
  
  return op->as_iter.pc->next;
}

struct sgl_op *sgl_op_iter_new(struct sgl *sgl,
                               struct sgl_form *form,
                               struct sgl_op *pc) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_ITER);
  op->run = iter_run;
  op->as_iter.pc = pc;
  return op;
}

static struct sgl_op *idle_run(struct sgl_op *op, struct sgl *sgl) {
  return (sgl->ntasks == 1) ? op->as_idle.end_pc->next : op->next;
}

struct sgl_op *sgl_op_idle_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_IDLE);
  op->run = idle_run;
  op->as_idle.end_pc = NULL;
  return op;
}

static void if_deinit(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_if *i = &op->as_if;
  if (i->cond) { sgl_val_free(i->cond, sgl); }
  if (i->val) { sgl_val_free(i->val, sgl); }
}

static void if_deval(struct sgl_op *op, struct sgl *sgl, struct sgl_val *val) {
  struct sgl_op_if *i = &op->as_if;

  if (i->cond && sgl_val_eq(val, sgl, i->cond)) {
    sgl_val_free(i->cond, sgl);
    i->cond = NULL;
  } else if (i->val && sgl_val_eq(val, sgl, i->val)) {
    sgl_val_free(i->val, sgl);
    i->val = NULL;
  } else {
    assert(false);
  }
}

static void if_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_op_if *i = &op->as_if;

  if (i->cond) {
    sgl_buf_putc(out, ' ');
    sgl_val_dump(i->cond, out);
  }

  if (i->val) {
    sgl_buf_putcs(out, " val: ");
    sgl_val_dump(i->val, out);
  }

  sgl_buf_printf(out, " end: %" SGL_INT, i->pc->next->pc);

  if (i->neg) { sgl_buf_putcs(out, " neg"); }
  if (i->pop) { sgl_buf_putcs(out, " pop"); }
}

static bool if_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_op_if *i = &op->as_if;
  bool changed = fuse_jumps(&i->pc, sgl, t);

  if (!i->cond) {
    i->cond = sgl_trace_pop(t, sgl);

    if (i->cond) {
      bool ok = sgl_val_bool(i->cond, sgl);
      if (i->neg) { ok = !ok; }
      assert(i->cond->op);
      sgl_op_deval(i->cond->op, sgl, i->cond);
      changed = true;
    }
  }

  if (i->cond && !i->pop) { sgl_trace_push(t, sgl, op, i->cond); }  
  if (!t->len) { return changed; }
  struct sgl_trace *t_if = sgl_trace_new(sgl);
  sgl_trace_move(t, t_if);
  if (i->val) { sgl_trace_push(t_if, sgl, op, i->val); }
  
  for (op = op->next; t_if->len && op != sgl->end_pc; op = op->next) {
    if (op == i->pc->next) {
      sgl_trace_sync(t, sgl, t_if);
      continue;
    }
    
    changed |= sgl_op_trace(op, sgl, t_if);
  }

  sgl_trace_free(t_if, sgl);
  return changed;
}

static struct sgl_op *if_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_pos *pos = &op->form->pos;
  struct sgl_op_if *i = &op->as_if;
  struct sgl_val *cond = i->cond;

  if (!cond) {
    cond = i->pop ? sgl_pop(sgl) : sgl_peek(sgl);
    if (!cond) { return sgl->end_pc; }
  }
  
  bool ok = sgl_val_bool(cond, sgl);
  if (i->neg) { ok = !ok; }
  struct sgl_op *pc = ok ? i->pc->next : op->next;

  if (ok) {
    struct sgl_ls *stack = sgl_stack(sgl);
    if (!i->pop && i->cond) { sgl_val_clone(i->cond, sgl, pos, stack); }
    if (i->val) { sgl_val_clone(i->val, sgl, pos, stack); }
  } else {
    sgl_ls_delete(&cond->ls);
  }
  
  if (!i->cond && (!ok || i->pop)) { sgl_val_free(cond, sgl); }
  return pc;
}

struct sgl_op *sgl_op_if_new(struct sgl *sgl,
                             struct sgl_form *form,
                             struct sgl_op *pc) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_IF);
  op->run = if_run;
  op->as_if = (struct sgl_op_if){.neg = false, .pc = pc, .pop = true, .cond = NULL,
                                 .val = NULL};
  return op;
}

static void jump_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_op_jump *j = &op->as_jump;
  sgl_buf_printf(out, " %" SGL_INT, j->pc->next->pc);
}

static bool jump_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  bool changed = fuse_jumps(&op->as_jump.pc, sgl, t);
  
  if (t->len) {
    for (op = op->as_jump.pc->next; t->len && op != sgl->end_pc; op = op->next) {
      changed |= sgl_op_trace(op, sgl, t);
    }
  }

  sgl_trace_clear(t, sgl);
  return changed;
}

static struct sgl_op *jump_run(struct sgl_op *op, struct sgl *sgl) {
  return op->as_jump.pc->next;
}

struct sgl_op *sgl_op_jump_new(struct sgl *sgl,
                               struct sgl_form *form,
                               struct sgl_op *pc) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_JUMP);
  op->run = jump_run;
  op->as_jump.pc = pc;
  return op;
}

static void let_var_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_op_let_var *lv = &op->as_let_var;
  sgl_buf_printf(out, " %s", lv->id->id);

  if (lv->val) {
    sgl_buf_putc(out, ' ');
    sgl_val_dump(lv->val, out);
  }
}

static bool let_var_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_pos *pos = &op->form->pos;
  struct sgl_op_let_var *lv = &op->as_let_var;
  struct sgl_val *v = lv->val;
  bool changed = false;
  
  if (!v) {
    v = sgl_trace_pop(t, sgl);
    if (!v) { return false; }
    assert(v->op);
    sgl_op_deval(v->op, sgl, v);
    op->as_let_var.val = v;
    changed = true;
  }

  if (!v) { return changed; }  
  sgl_int_t func_depth = 1, scope_depth = 1, task_depth = 1, shadow_depth = 0;
  
  for (op = op->next;  op != sgl->end_pc; op = op->next) {
    if (op->type == &SGL_OP_LET_VAR) {
      if (op->as_let_var.id == lv->id) {
        if (!scope_depth) {
          sgl_error(sgl, pos, sgl_sprintf("Dup var: %s", lv->id->id));
          return false;
        }
        
        if (!shadow_depth) { shadow_depth = scope_depth; }
      }
    } else if (op->type == &SGL_OP_GET_VAR && !shadow_depth) {
      struct sgl_op_get_var *gv = &op->as_get_var;
      
      if (gv->id == lv->id && !gv->val) {
        gv->val = sgl_val_dup(lv->val, sgl, NULL);
        changed = true;
      }
    } else if (op->type == &SGL_OP_SCOPE_BEG) {
      scope_depth++;
    } else if (op->type == &SGL_OP_SCOPE_END) {
      if (shadow_depth == scope_depth) { shadow_depth = 0; }
      if (!--scope_depth) { break; }
    } else if (op->type == &SGL_OP_TASK_BEG) {
      task_depth++;
    } else if (op->type == &SGL_OP_TASK_END) {
      if (!--task_depth) { break; }
    } else if (op->type == &SGL_OP_FIMP) {
      struct sgl_imp *f = &op->as_fimp.fimp->imp;
      if (f->end_pc != f->beg_pc) { func_depth++; }
    } else if (op->type == &SGL_OP_LAMBDA) {
      struct sgl_op_lambda *l = &op->as_lambda;
      if (l->end_pc != l->beg_pc) { func_depth++; }
    } else if (op->type == &SGL_OP_RETURN) {
      if (!--func_depth) { break; }
    }
  }

  return changed;
}

static struct sgl_op *let_var_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_let_var *sv = &op->as_let_var;
  struct sgl_val *val = sv->val ? sgl_val_dup(sv->val, sgl, NULL) : NULL;
  
  if (!val) {
    val = sgl_pop(sgl);  
    if (!val) { return sgl->end_pc; }
  }

  return sgl_let_var(sgl, &op->form->pos, sv->id, val) ? op->next : sgl->end_pc;
}

static void let_var_deinit(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *val = op->as_let_var.val;
  if (val) { sgl_val_free(val, sgl); }
}

struct sgl_op *sgl_op_let_var_new(struct sgl *sgl,
                                  struct sgl_form *form,
                                  struct sgl_sym *id) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_LET_VAR);
  op->run = let_var_run;
  op->as_let_var = (struct sgl_op_let_var){.id = id, .val = NULL};
  return op;
}

static void lambda_deval(struct sgl_op *op, struct sgl *sgl, struct sgl_val *val) {
  op->as_lambda.push_val = false;
}

static void lambda_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_op_lambda *l = &op->as_lambda;
  sgl_buf_printf(out, " end: %" SGL_INT, l->end_pc->pc);
  if (l->push_val) { sgl_buf_putcs(out, " push"); }
}

static bool lambda_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_op_lambda *l = &op->as_lambda;
  bool changed = false;

  if (l->push_val) {
    sgl_trace_push_new(t, sgl, op, sgl->Lambda)->as_lambda = l;
    
    for (op = l->end_pc->next; t->len && op != sgl->end_pc; op = op->next) {
      changed |= sgl_op_trace(op, sgl, t);
    }
  }
  
  sgl_trace_clear(t, sgl);
  return changed;
}

static struct sgl_op *lambda_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_lambda *l = &op->as_lambda;
  if (l->push_val) { sgl_push(sgl, l->type)->as_lambda = l; }
  struct sgl_op *next = op->next;  

  if (next->type == &SGL_OP_SCOPE_BEG && !next->as_scope_beg.parent) {
    struct sgl_scope *s = sgl_scope(sgl);
    next->as_scope_beg.parent = s;
    s->nrefs++;
  }
  
  return l->end_pc->next;
}

struct sgl_op *sgl_op_lambda_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_LAMBDA);
  op->run = lambda_run;
  
  op->as_lambda = (struct sgl_op_lambda){.beg_pc = op,
                                         .end_pc = op,
                                         .type = sgl->Lambda,
                                         .push_val = true};

  return op;
}

static bool list_beg_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_trace *t_list = sgl_trace_new(sgl);
  struct sgl_op *beg_pc = op;
  bool changed = false;
  sgl_int_t depth = 1;
  
  for (op = beg_pc->next; depth && op != sgl->end_pc; op = op->next) {
    changed |= op->type != &SGL_OP_LIST_END && sgl_op_trace(op, sgl, t_list);

    if (!t_list->len && (op != beg_pc->next || op->type != &SGL_OP_LIST_END)) {
      goto exit;
    }
               
    if (op->type == &SGL_OP_LIST_BEG) {
      depth++;
    } else if (op->type == &SGL_OP_LIST_END) {
      depth--;
    } else if (!sgl_op_const(op)) {
      break;
    }
  }
  
  if (depth) { goto exit; }
  struct sgl_op *end_pc = op->prev;

  if (!sgl_trace_is_undef(t_list, sgl, t_list->len)) {
    struct sgl_val *v = sgl_val_new(sgl, sgl->List, NULL);
    struct sgl_list *l = v->as_list = sgl_list_new(sgl);
    sgl_trace_move_ls(t_list, &l->root);
  
    sgl_ls_do(&l->root, struct sgl_val, ls, lv) {
      assert(lv->op);
      sgl_op_deval(lv->op, sgl, lv);
    }

    sgl_op_push_reuse(beg_pc, sgl, v);
    sgl_nop(end_pc, sgl);
    changed = true;
  }

 exit:
  sgl_trace_free(t_list, sgl);
  return changed;
}

static struct sgl_op *list_beg_run(struct sgl_op *op, struct sgl *sgl) {
  sgl_beg_stack(sgl);
  return op->next;
}

struct sgl_op *sgl_op_list_beg_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_LIST_BEG);
  op->run = list_beg_run;
  return op;
}

static bool list_end_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  sgl_trace_clear(t, sgl);
  sgl_trace_push_undef(t, sgl, sgl->List);
  return false;
}

static struct sgl_op *list_end_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_list *l = sgl_end_stack(sgl);
  sgl_push(sgl, sgl->List)->as_list = l;
  return op->next;
}

struct sgl_op *sgl_op_list_end_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_LIST_END);
  op->run = list_end_run;
  return op;
}

static struct sgl_op *new_cinit(struct sgl_op *op,
                                struct sgl *sgl,
                                struct sgl_cemit *out) {
  sgl_int_t form_id = op->form->cemit_id;
  
  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT " = "
                 "sgl_op_new_new(sgl, form%" SGL_INT ");",
                 op->cemit_pc, form_id);
  
  return op->next;
}

static void new_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_type *t = op->as_new.type;
  if (t) { sgl_buf_printf(out, " %s", sgl_type_id(t)->id); }
}

static bool new_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_pos *pos = &op->form->pos;
  struct sgl_op_new *n = &op->as_new;
  bool changed = false;
  
  if (!n->type) {
    struct sgl_val *v = sgl_trace_pop(t, sgl);
    if (!v) { return false; }
    
    if (!sgl_derived(v->type, sgl->StructMeta)) {
      sgl_error(sgl, pos,
                sgl_sprintf("Invalid struct type: %s", sgl_type_id(v->type)->id));
      
      return sgl->end_pc;
    }

    n->type = v->as_type;
    assert(v->op);
    sgl_op_deval(v->op, sgl, v);
    sgl_val_free(v, sgl);
    changed = true;
  }

  sgl_trace_push_undef(t, sgl, n->type ? n->type : sgl->Struct);
  return changed;
}

static struct sgl_op *new_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_pos *pos = &op->form->pos;
  struct sgl_type *t = op->as_new.type;

  if (!t) {
    struct sgl_val *tv = sgl_pop(sgl);

    if (!tv) {
      sgl_error(sgl, pos, sgl_strdup("Missing struct type"));
      return sgl->end_pc;
    }

    if (!sgl_derived(tv->type, sgl->StructMeta)) {
      sgl_error(sgl, pos,
                sgl_sprintf("Invalid struct type: %s", sgl_type_id(tv->type)->id));
      
      return sgl->end_pc;
    }

    t = tv->as_type;
    sgl_val_free(tv, sgl);
  }
  
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, t);
  struct sgl_struct_type *st = sgl_baseof(struct sgl_struct_type, ref_type, rt);
  sgl_push(sgl, t)->as_struct = sgl_struct_new(st);
  return op->next;
}

struct sgl_op *sgl_op_new_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_NEW);
  op->run = new_run;
  op->as_new.type = NULL;
  return op;
}

static bool pair_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_val *lst = sgl_trace_pop(t, sgl), *fst = sgl_trace_pop(t, sgl);

  if (!fst || !lst) {
    sgl_trace_push_undef(t, sgl, sgl->Pair);
    return false;
  }

  assert(fst->op);
  sgl_op_deval(fst->op, sgl, fst);

  assert(lst->op);
  sgl_op_deval(lst->op, sgl, lst);

  struct sgl_val *v = sgl_val_new(sgl, sgl->Pair, NULL);
  sgl_pair_init(&v->as_pair, fst, lst);
  sgl_op_push_reuse(op, sgl, v);
  sgl_op_trace(op, sgl, t);
  return true;
}

static struct sgl_op *pair_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *last = sgl_pop(sgl);
  if (!last) { return sgl->end_pc; }
  struct sgl_val *first = sgl_pop(sgl);
  if (!first) { return sgl->end_pc; }
  sgl_pair_init(&sgl_push(sgl, sgl->Pair)->as_pair, first, last); 
  return op->next;
}

struct sgl_op *sgl_op_pair_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_PAIR);
  op->run = pair_run;
  return op;
}

static bool peek_reg_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  sgl_trace_push_undef(t, sgl, sgl->T);
  return false;
}

static struct sgl_op *peek_reg_run(struct sgl_op *op, struct sgl *sgl) {
  return sgl_peek_reg(sgl, true, sgl_stack(sgl)) ? op->next : sgl->end_pc;
}

struct sgl_op *sgl_op_peek_reg_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_PEEK_REG);
  op->run = peek_reg_run;
  return op;
}

static bool pop_reg_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  if (!op->as_pop_reg.drop) { sgl_trace_push_undef(t, sgl, sgl->T); }
  return false;
}

static struct sgl_op *pop_reg_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *v = sgl_pop_reg(sgl);
  if (!v) { return sgl->end_pc; }

  if (op->as_pop_reg.drop) {
    sgl_val_free(v, sgl);
  } else {
    sgl_ls_push(sgl_stack(sgl), &v->ls);
  }
  
  return op->next;
}

struct sgl_op *sgl_op_pop_reg_new(struct sgl *sgl, struct sgl_form *form, bool drop) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_POP_REG);
  op->run = pop_reg_run;
  op->as_pop_reg.drop = drop;
  return op;
}

static bool push_const(struct sgl_op *op) { return true; }

static void push_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_op_push *p = &op->as_push;
  sgl_buf_putc(out, ' ');
  sgl_val_dump(p->val, out);
}

static bool push_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  sgl_trace_push(t, sgl, op, op->as_push.val);
  return false;
}

static struct sgl_op *push_cinit(struct sgl_op *op,
                                 struct sgl *sgl,
                                 struct sgl_cemit *out) {
  sgl_int_t fid = op->form->cemit_id;

  if (op->form->type == &SGL_FORM_LIT) {
    sgl_cemit_line(out,
                   "v = sgl_val_dup(form%" SGL_INT "->as_lit.val, sgl, NULL);",
                   fid);
  } else {
    sgl_val_cemit(op->as_push.val, sgl, &op->form->pos, "v", "NULL", out);
  }

  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT " = "
                 "sgl_op_push_new(sgl, form%" SGL_INT ", v);",
                 op->cemit_pc, fid);

  return op->next;
}

static void push_deinit(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *val = op->as_push.val;
  if (val) { sgl_val_free(val, sgl); }
}

static struct sgl_op *push_run(struct sgl_op *op, struct sgl *sgl) {
  return sgl_val_clone(op->as_push.val, sgl, &op->form->pos, sgl_stack(sgl))
    ? op->next
    : sgl->end_pc;
}

struct sgl_op *sgl_op_push_new(struct sgl *sgl,
                               struct sgl_form *form,
                               struct sgl_val *val) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_PUSH);
  op->run = push_run;
  op->as_push.val = val;
  return op;
}

void sgl_op_push_reuse(struct sgl_op *op, struct sgl *sgl, struct sgl_val *val) {
  if (op->type->deinit_op) { op->type->deinit_op(op, sgl); }
  op->type = &SGL_OP_PUSH;
  op->run = push_run;
  op->as_push.val = val;
}

static bool push_reg_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  sgl_trace_drop(t, sgl, 1);
  return false;
}

static struct sgl_op *push_reg_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *v = sgl_pop(sgl);
  if (!v) { return sgl->end_pc; }
  sgl_ls_push(sgl_reg_stack(sgl), &v->ls);
  return op->next;
}

struct sgl_op *sgl_op_push_reg_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_PUSH_REG);
  op->run = push_reg_run;
  return op;
}

static struct sgl_op *put_field_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_field *pf = &op->as_field;

  struct sgl_val
    *sv = pf->use_reg ? sgl_peek_reg(sgl, true, NULL) : sgl_pop(sgl),
    *v = pf->val ? sgl_val_dup(pf->val, sgl, NULL) : sgl_pop(sgl);

  if (!v || !sv) { return sgl->end_pc; }
  
  struct sgl_type *t = sv->type, *st = pf->struct_type;
  struct sgl_field *f = pf->field;
  
  if (!f) {
    if (!sgl_derived(t, sgl->Struct)) {
      sgl_error(sgl, &op->form->pos,
                sgl_sprintf("Expected struct, was: %s", t->def.id->id));
      
      return sgl->end_pc;
    }

    struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, t);
    struct sgl_struct_type *st = sgl_baseof(struct sgl_struct_type, ref_type, rt);
    struct sgl_sym *id = pf->id;    
    f = sgl_struct_type_find(st, id);
    
    if (!f) {
      sgl_error(sgl, &op->form->pos, sgl_sprintf("Unknown field: %s", id->id));
      return sgl->end_pc;
    }
  } else if (t != st) {
    sgl_error(sgl, &op->form->pos,
              sgl_sprintf("Expected %s, was: %s", st->def.id->id, t->def.id->id));
    
    return sgl->end_pc;
  }

  if (!pf->val && !sgl_derived(v->type, f->type)) {
    sgl_error(sgl, &op->form->pos,
              sgl_sprintf("Expected %s, was: %s",
                          f->type->def.id->id, v->type->def.id->id));
    
    return sgl->end_pc;
  }

  struct sgl_val **fv = sv->as_struct->fields + f->i;
  if (*fv) { sgl_val_free(*fv, sgl); }
  *fv = v;
  sgl_val_free(sv, sgl);
  return op->next;
}

struct sgl_op *sgl_op_put_field_new(struct sgl *sgl,
                                    struct sgl_form *form,
                                    struct sgl_sym *id,
                                    struct sgl_val *val) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_PUT_FIELD);
  op->run = put_field_run;

  if (!init_field(&op->as_field, sgl, &form->pos, id)) {
    sgl_op_free(op, sgl);
    return NULL;
  }

  op->as_field.val = val;
  return op;
}

static struct sgl_op *recall_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_pos *pos = &op->form->pos;
  struct sgl_ls *cs = &sgl->task->calls;
  struct sgl_call *call = NULL;
  
  for (struct sgl_ls *cl = cs->prev; cl != cs; cl = cl->prev) {
    struct sgl_call *c = sgl_baseof(struct sgl_call, ls, cl);

    if (c->beg_pc) {
      call = c;
      break;
    }
  }
  
  if (!call) {
    sgl_error(sgl, pos, sgl_strdup("No call in progress"));
    return sgl->end_pc;
  }
  
  sgl_state_restore(&call->state, sgl);
  struct sgl_func *func = op->as_recall.func;

  if (func) {
    struct sgl_fimp *fimp = sgl_func_dispatch(func, sgl, sgl_stack(sgl));
    
    if (!fimp) {
      sgl_error(sgl, pos, sgl_sprintf("Func not applicable: %s", func->def.id->id));
      return sgl->end_pc;
    }

    struct sgl_imp *i = &fimp->imp;
    if (!sgl_fimp_compile(fimp, sgl, pos)) { return sgl->end_pc; }
    sgl_call_init(call, sgl, pos, i->beg_pc, call->return_pc);
    return i->beg_pc->next;
  }
  
  return call->beg_pc->next;
}

struct sgl_op *sgl_op_recall_new(struct sgl *sgl,
                                 struct sgl_form *form,
                                 struct sgl_func *func) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_RECALL);
  op->run = recall_run;
  op->as_recall.func = func;
  return op;
}

static bool ref_trace(struct sgl_op *op,
                             struct sgl *sgl,
                             struct sgl_trace *t) {
  struct sgl_val *v = sgl_trace_pop(t, sgl);

  if (!v) {
    sgl_trace_push_undef(t, sgl, sgl->Ref);
    return false;
  }
  
  assert(v->op);
  sgl_op_deval(v->op, sgl, v);
  
  struct sgl_val *rv = sgl_val_new(sgl, sgl->Ref, NULL);
  rv->as_ref = sgl_ref_new(sgl, v);
  sgl_op_push_reuse(op, sgl, rv);
  sgl_op_trace(op, sgl, t);
  return true;
}

static struct sgl_op *ref_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *v = sgl_pop(sgl);

  if (!v) {
    sgl_error(sgl, &op->form->pos, sgl_strdup("Nothing to ref"));
    return sgl->end_pc;
  }

  sgl_push(sgl, sgl->Ref)->as_ref = sgl_ref_new(sgl, v);
  return op->next;
}

struct sgl_op *sgl_op_ref_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_REF);
  op->run = ref_run;
  return op;
}

static struct sgl_op *return_cinit(struct sgl_op *op,
                                   struct sgl *sgl,
                                   struct sgl_cemit *out) {
  sgl_int_t form_id = op->form->cemit_id;
  
  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT " = "
                 "sgl_op_return_new(sgl, form%" SGL_INT ", op%" SGL_INT ");",
                 op->cemit_pc, form_id, op->as_return.beg_pc->cemit_pc);
  
  return op->next;
}

static struct sgl_op *return_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_call *call = sgl_call(sgl);
  
  if (!call) {
    sgl_error(sgl, &op->form->pos, sgl_strdup("No calls in progress"));
    return sgl->end_pc;
  }
  
  struct sgl_op *return_pc = call->return_pc->next;
  sgl_call_free(call, sgl);
  return return_pc;
}

struct sgl_op *sgl_op_return_new(struct sgl *sgl,
                                 struct sgl_form *form,
                                 struct sgl_op *beg_pc) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_RETURN);
  op->run = return_run;
  op->as_return.beg_pc = beg_pc;
  return op;
}

static bool rotl_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  sgl_trace_rotl(t, sgl);
  return false;
}

static struct sgl_op *rotl_run(struct sgl_op *op, struct sgl *sgl) {
  return sgl_stack_rotl(sgl_stack(sgl), sgl, false) ? op->next : sgl->end_pc;
}

struct sgl_op *sgl_op_rotl_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_ROTL);
  op->run = rotl_run;
  return op;
}

void sgl_op_rotl_reuse(struct sgl_op *op, struct sgl *sgl) {
  if (op->type->deinit_op) { op->type->deinit_op(op, sgl); }
  op->type = &SGL_OP_ROTL;
  op->run = rotl_run;
}

static bool rotr_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  sgl_trace_rotr(t, sgl);
  return false;
}

static struct sgl_op *rotr_run(struct sgl_op *op, struct sgl *sgl) {
  return sgl_stack_rotr(sgl_stack(sgl), sgl, false) ? op->next : sgl->end_pc;
}

struct sgl_op *sgl_op_rotr_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_ROTR);
  op->run = rotr_run;
  return op;
}

static void scope_beg_deinit(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_scope_beg *sb = &op->as_scope_beg;  
  struct sgl_scope *ps = sb->parent;
  if (ps) { sgl_scope_deref(ps, sgl); }
}

static struct sgl_op *scope_beg_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_scope_beg *sb = &op->as_scope_beg;
  struct sgl_scope *ps = sb->parent;
  sgl_beg_scope(sgl, ps ? ps : sgl_scope(sgl));
  return op->next;
}

struct sgl_op *sgl_op_scope_beg_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_SCOPE_BEG);
  op->run = scope_beg_run;
  op->as_scope_beg = (struct sgl_op_scope_beg){.parent = NULL};
  return op;
}

static struct sgl_op *scope_end_run(struct sgl_op *op, struct sgl *sgl) {
  sgl_end_scope(sgl);
  return op->next;
}

struct sgl_op *sgl_op_scope_end_new(struct sgl *sgl,
                                    struct sgl_form *form,
                                    struct sgl_op *beg_pc) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_SCOPE_END);
  op->run = scope_end_run;
  op->as_scope_end.beg_pc = beg_pc;
  return op;
}

static struct sgl_op *stack_list_cinit(struct sgl_op *op,
                                       struct sgl *sgl,
                                       struct sgl_cemit *out) {
  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT " = "
                 "sgl_op_stack_list_new(sgl, form%" SGL_INT ");",
                 op->cemit_pc, op->form->cemit_id);
  
  return op->next;
}

static bool stack_list_trace(struct sgl_op *op,
                             struct sgl *sgl,
                             struct sgl_trace *t) {
  sgl_trace_clear(t, sgl);
  sgl_trace_push_undef(t, sgl, sgl->List);
  return false;
}

static struct sgl_op *stack_list_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_ls *stack = sgl_stack(sgl);
  struct sgl_list *l = sgl_list_new(sgl);
  sgl_ls_assign(&l->root, stack);
  sgl_ls_init(stack);
  sgl_val_new(sgl, sgl->List, stack)->as_list = l;
  return op->next;
}

struct sgl_op *sgl_op_stack_list_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_STACK_LIST);
  op->run = stack_list_run;
  return op;
}

static void stack_switch_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_op_stack_switch *ss = &op->as_stack_switch;
  sgl_buf_printf(out, " n: %" SGL_INT " %s", ss->n, ss->pop ? "pop" : "");
}

static struct sgl_op *stack_switch_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_stack_switch *ss = &op->as_stack_switch;
  struct sgl_val *v = ss->pop ? sgl_peek(sgl) : NULL;
  struct sgl_list *s = sgl_switch_stack(sgl, op->as_stack_switch.n);
  
  if (!s) {
    if (v) {
      sgl_ls_delete(&v->ls);
      sgl_val_free(v, sgl);
    }
    
    return sgl->end_pc;
  }

  if (v) {
    sgl_ls_delete(&v->ls);
    sgl_ls_push(&s->root, &v->ls);
  }
  
  return op->next;
}

struct sgl_op *sgl_op_stack_switch_new(struct sgl *sgl,
                                       struct sgl_form *form,
                                       sgl_int_t n,
                                       bool pop) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_STACK_SWITCH);
  op->run = stack_switch_run;
  op->as_stack_switch = (struct sgl_op_stack_switch){.n = n, .pop = pop};
  return op;
}

static void stdout_beg_deinit(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *b = op->as_stdout_beg.buf;
  if (b) { sgl_val_free(b, sgl); }
}

static void stdout_beg_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_val *b = op->as_stdout_beg.buf;

  if (b) {
    sgl_buf_putc(out, ' ');
    sgl_val_dump(b, out);
  }
}

static bool stdout_beg_trace(struct sgl_op *op,
                             struct sgl *sgl,
                             struct sgl_trace *t) {
  struct sgl_op_stdout_beg *sb = &op->as_stdout_beg;
  if (sb->buf) { return false; }
  struct sgl_val *b = sb->buf = sgl_trace_pop(t, sgl);
  if (!b) { return false; }
  
  if (b) {
    assert(b->op);
    sgl_op_deval(b->op, sgl, b);
  }

  return true;
}

static struct sgl_op *stdout_beg_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_stdout_beg *sb = &op->as_stdout_beg;
  struct sgl_val *b = sb->buf;

  if (!b) {
    b = sgl_pop(sgl);
    if (!b) { return sgl->end_pc; }
  }
  
  sgl_beg_stdout(sgl, b->as_buf);
  if (!sb->buf) { sgl_val_free(b, sgl); }
  return op->next;
}

struct sgl_op *sgl_op_stdout_beg_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_STDOUT_BEG);
  op->run = stdout_beg_run;
  op->as_stdout_beg.buf = NULL;
  return op;
}

static struct sgl_op *stdout_end_run(struct sgl_op *op, struct sgl *sgl) {
  sgl_buf_deref(sgl_end_stdout(sgl), sgl);
  return op->next;
}

struct sgl_op *sgl_op_stdout_end_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_STDOUT_END);
  op->run = stdout_end_run;
  return op;
}

static bool str_beg_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_op *beg_pc = op;
  bool changed = false;
  sgl_int_t depth = 1;
  
  for (op = beg_pc->next; depth && op != sgl->end_pc; op = op->next) {
    changed |= op->type != &SGL_OP_STR_END && sgl_op_trace(op, sgl, t);

    if (op->type == &SGL_OP_STR_BEG) {
      depth++;
    } else if (op->type == &SGL_OP_STR_END) {
      depth--;
    } else if (op->type == &SGL_OP_STR_PUT) {
      if (!op->as_str_put.val) { break; }
    } else if (!sgl_op_const(op)) {
      break;
    }
  }

  sgl_trace_clear(t, sgl);
  if (depth) { return changed; }
  struct sgl_op *end_pc = op->prev;
  struct sgl_buf buf;
  sgl_buf_init(&buf);
  
  for (op = beg_pc->next; op != end_pc; op = op->next) {
    if (op->type == &SGL_OP_STR_PUT) {
      struct sgl_val *v = op->as_str_put.val;
      assert(v);
      sgl_val_print(v, sgl, &op->form->pos, &buf);
      sgl_nop(op, sgl);
    }
  }
  
  bool is_short = buf.len < SGL_MAX_STR_LEN;

  struct sgl_str *s =
    sgl_str_new(sgl, is_short ? sgl_buf_cs(&buf) : NULL, buf.len);
  
  if (!is_short) {
    char *d = (char *)buf.data;
    d[buf.len] = 0;
    s->as_long = d;
    buf.data = NULL;
    buf.cap = 0;
  }

  buf.len = 0;
  struct sgl_val *v = sgl_val_new(sgl, sgl->Str, NULL);
  v->as_str = s;
  sgl_op_push_reuse(beg_pc, sgl, v);
  sgl_nop(end_pc, sgl);
  sgl_buf_deinit(&buf);
  return true;
}

static struct sgl_op *str_beg_run(struct sgl_op *op, struct sgl *sgl) {
  sgl_push_reg(sgl, sgl->Buf)->as_buf = sgl_buf_new(sgl);
  return op->next;
}

struct sgl_op *sgl_op_str_beg_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_STR_BEG);
  op->run = str_beg_run;
  return op;
}

static bool str_end_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  sgl_trace_push_undef(t, sgl, sgl->Str);
  return false;
}

static struct sgl_op *str_end_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *v = sgl_pop_reg(sgl);
  struct sgl_buf *b = v->as_buf;
  bool is_short = b->len < SGL_MAX_STR_LEN;
  struct sgl_str *s = sgl_str_new(sgl, is_short ? sgl_buf_cs(b) : NULL, b->len);

  if (!is_short) {
    s->as_long = (char *)b->data;
    b->data = NULL;
    b->cap = 0;
  }

  b->len = 0;
  sgl_val_init(v, sgl, sgl->Str, sgl_stack(sgl))->as_str = s;
  sgl_buf_deref(b, sgl);
  return op->next;
}

struct sgl_op *sgl_op_str_end_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_STR_END);
  op->run = str_end_run;
  return op;
}

static void str_put_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_val *v = op->as_str_put.val;

  if (v) {
    sgl_buf_putc(out, ' ');
    sgl_val_dump(v, out);
  }
}

static bool str_put_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  if (op->as_str_put.val) { return false; }
  struct sgl_val *v = sgl_trace_pop(t, sgl);
  if (!v) { return false; }
  op->as_str_put.val = v;
  assert(v->op);
  sgl_op_deval(v->op, sgl, v);
  return true;
}

static void str_put_deinit(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *val = op->as_str_put.val;
  if (val) { sgl_val_free(val, sgl); }
}

static struct sgl_op *str_put_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_str_put *sp = &op->as_str_put;
  struct sgl_val *v = sp->val;
  if (!v && !(v = sgl_pop(sgl))) { return sgl->end_pc; }
  sgl_val_print(v, sgl, &op->form->pos, sgl_peek_reg(sgl, false, NULL)->as_buf);
  if (!sp->val) { sgl_val_free(v, sgl); }
  return op->next;
}

struct sgl_op *sgl_op_str_put_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_STR_PUT);
  op->run = str_put_run;
  op->as_str_put.val = NULL;
  return op;
}

static struct sgl_op *swap_cinit(struct sgl_op *op,
                                 struct sgl *sgl,
                                 struct sgl_cemit *out) {
  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT " = "
                 "sgl_op_swap_new(sgl, form%" SGL_INT ");",
                 op->cemit_pc, op->form->cemit_id);
  
  return op->next;
}

static bool swap_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  sgl_trace_swap(t, sgl);
  return false;
}

static struct sgl_op *swap_run(struct sgl_op *op, struct sgl *sgl) {
  return sgl_stack_swap(sgl_stack(sgl), sgl, false) ? op->next : sgl->end_pc;
}

struct sgl_op *sgl_op_swap_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_SWAP);
  op->run = swap_run;
  return op;
}

static struct sgl_op *sync_cinit(struct sgl_op *op,
                                 struct sgl *sgl,
                                 struct sgl_cemit *out) {
  sgl_int_t form_id = op->form->cemit_id;
  
  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT " = "
                 "sgl_op_sync_new(sgl, form%" SGL_INT ", %" SGL_INT ");",
                 op->cemit_pc, form_id, op->as_sync.n);
  
  return op->next;
}

static bool sync_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  return false;
}

static struct sgl_op *sync_run(struct sgl_op *op, struct sgl *sgl) {
  sgl->sync_depth += op->as_sync.n;
  return op->next;
}

struct sgl_op *sgl_op_sync_new(struct sgl *sgl, struct sgl_form *form, sgl_int_t n) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_SYNC);
  op->run = sync_run;
  op->as_sync.n = n;
  return op;
}

static void task_beg_dump(struct sgl_op *op, struct sgl_buf *out) {
  sgl_buf_printf(out, " end: %" SGL_INT, op->as_task_beg.end_pc->pc);
}

static bool task_beg_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_op_task_beg *tb = &op->as_task_beg;
  bool changed = false;
    
  if (t->len) {
    for (op = tb->end_pc->next;
         t->len && op != sgl->end_pc;
         op = op->next) { changed |= sgl_op_trace(op, sgl, t); }

    sgl_trace_clear(t, sgl);
  }

  return fuse_jumps(&tb->end_pc, sgl, t) || changed;
}

static struct sgl_op *task_beg_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op *end_pc = op->as_task_beg.end_pc;
  sgl_task_new(sgl, sgl_lib(sgl), sgl_scope(sgl), op, end_pc);
  return end_pc->next;
}

struct sgl_op *sgl_op_task_beg_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_TASK_BEG);
  op->run = task_beg_run;
  op->as_task_beg = (struct sgl_op_task_beg){.end_pc = NULL};
  return op;
}

static struct sgl_op *task_end_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_task *t = sgl->task;
  sgl_ls_delete(&t->ls);
  sgl->ntasks--;
  struct sgl_op *pc = sgl_yield(sgl);
  sgl_task_free(t, sgl);
  return pc->next;
}

struct sgl_op *sgl_op_task_end_new(struct sgl *sgl,
                                   struct sgl_form *form,
                                   struct sgl_op *beg_pc) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_TASK_END);
  op->run = task_end_run;
  op->as_task_end.beg_pc = beg_pc;
  return op;
}

static void throw_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_val *v = op->as_throw.val;

  if (v) {
    sgl_buf_putc(out, ' ');
    sgl_val_dump(v, out);
  }
}

static bool throw_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  bool changed = false;
  
  if (!op->as_throw.val) {
    struct sgl_val *v = op->as_throw.val = sgl_trace_pop(t, sgl);

    if (v) {
      assert(v->op);
      sgl_op_deval(v->op, sgl, v);
      changed = true;
    }
  }

  sgl_trace_clear(t, sgl);
  return changed;
}

static void throw_deinit(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *v = op->as_throw.val;
  if (v) { sgl_val_free(v, sgl); }
}

static struct sgl_op *throw_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *v = op->as_throw.val;
  
  return sgl_throw(sgl, &op->form->pos, NULL,
                   v ? sgl_val_dup(v, sgl, NULL) : sgl_pop(sgl));
}

struct sgl_op *sgl_op_throw_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_THROW);
  op->run = throw_run;
  op->as_throw.val = NULL;
  return op;
}

static struct sgl_op *times_cinit(struct sgl_op *op,
                                  struct sgl *sgl,
                                  struct sgl_cemit *out) {
  sgl_int_t form_id = op->form->cemit_id;
  struct sgl_val *reps = op->as_times.reps;
  if (reps) { sgl_val_cemit(reps, sgl, &op->form->pos, "v", NULL, out); }
  
  sgl_cemit_line(out,
                 "struct sgl_op *op%" SGL_INT " = "                
                 "sgl_op_times_new(sgl, form%" SGL_INT ", %s);",
                 op->cemit_pc, form_id, reps ? "v" : "NULL");
  
  return op->next;
}

static void times_dump(struct sgl_op *op, struct sgl_buf *out) {
  struct sgl_val *reps = op->as_times.reps;
  if (reps) { sgl_buf_printf(out, " %" SGL_INT, reps->as_int); }
}

static bool times_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_op *start_op = op;
  struct sgl_val *v = start_op->as_times.reps ? NULL : sgl_trace_pop(t, sgl);
  struct sgl_trace *t_iter = sgl_trace_new(sgl);
  sgl_trace_copy(t, sgl, t_iter);
  bool ok = false, changed = false;

  for (op = start_op->next;
       (!ok || t_iter->len) && op != sgl->end_pc;
       op = op->next) {
    if (op->type == &SGL_OP_COUNT && op->as_count.pc == start_op) {
      sgl_trace_sync(t, sgl, t_iter);
      sgl_trace_undef(t_iter, sgl, t_iter->len);
      ok = true;
      continue;
    }
    
    changed |= sgl_op_trace(op, sgl, t_iter);
  }
  
  sgl_trace_free(t_iter, sgl);
  if (start_op->as_times.reps || !v) { return changed; }
  
  if (v->type != sgl->Int) {
    sgl_error(sgl, &start_op->form->pos,
              sgl_sprintf("Invalid reps: %s", v->type->def.id->id));
    
    return false;
  }

  start_op->as_times.reps = v;
  assert(v->op);
  sgl_op_deval(v->op, sgl, v);
  return true;
}

static struct sgl_op *times_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_op_times *t = &op->as_times;
  struct sgl_val *reps = t->reps ? sgl_val_dup(t->reps, sgl, NULL) : NULL;

  if (!reps) {
    reps = sgl_pop(sgl);
    if (!reps) { return sgl->end_pc; }

    if (reps->type != sgl->Int) {
      sgl_error(sgl, &op->form->pos,
                sgl_sprintf("Invalid reps: %s", reps->type->def.id->id));
      
      return sgl->end_pc;
    }
  }

  sgl_ls_push(sgl_reg_stack(sgl), &reps->ls);
  return op->next;
}

struct sgl_op *sgl_op_times_new(struct sgl *sgl,
                                struct sgl_form *form,
                                struct sgl_val *reps) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_TIMES);
  op->run = times_run;
  op->as_times.reps = reps;
  return op;
}

static bool try_beg_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  return false;
}

static struct sgl_op *try_beg_run(struct sgl_op *op, struct sgl *sgl) {
  sgl_try_new(sgl, op->as_try_beg.end_pc);
  return op->next;
}

struct sgl_op *sgl_op_try_beg_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_TRY_BEG);
  op->run = try_beg_run;
  op->as_try_beg.end_pc = NULL;
  return op;
}

static bool try_end_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  sgl_trace_push_undef(t, sgl, sgl->Error);
  return false;
}

static struct sgl_op *try_end_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_ls *found = sgl_ls_pop(&sgl->task->tries);
  assert(found);
  struct sgl_try *try = sgl_baseof(struct sgl_try, ls, found);
  sgl_try_free(try, sgl);
  return op->next;
}

struct sgl_op *sgl_op_try_end_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_TRY_END);
  op->run = try_end_run;
  return op;
}

static bool type_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t) {
  struct sgl_val *v = sgl_trace_pop(t, sgl);
  if (!v) { return false; }
  struct sgl_type *vt = v->type;
  sgl_op_push_reuse(v->op, sgl, sgl_val_reuse(v, sgl, sgl_type_meta(vt, sgl), true));
  v->as_type = vt;
  sgl_nop(op, sgl);
  return true;
}

static struct sgl_op *type_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *v = sgl_peek(sgl);

  if (!v) {
    sgl_error(sgl, &op->form->pos, sgl_strdup("Missing value"));
    return sgl->end_pc;
  }

  struct sgl_type *t = v->type;
  sgl_val_reuse(v, sgl, sgl_type_meta(t, sgl), true)->as_type = t;
  return op->next;
}

struct sgl_op *sgl_op_type_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_TYPE);
  op->run = type_run;
  return op;
}

static struct sgl_op *yield_run(struct sgl_op *op, struct sgl *sgl) {
  return sgl_yield(sgl)->next;
}

struct sgl_op *sgl_op_yield_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_YIELD);
  op->run = yield_run;
  return op;
}

static void watch_beg_deinit(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *w = op->as_watch_beg.watcher;
  if (w) { sgl_val_free(w, sgl); }
}

static struct sgl_op *watch_beg_run(struct sgl_op *op, struct sgl *sgl) {
  struct sgl_val *w = op->as_watch_beg.watcher;
  
  if (w) {
    sgl_val_dup(w, sgl, &sgl->task->watchers);
  } else {
    w = sgl_pop(sgl);

    if (!w) {
      sgl_error(sgl, &op->form->pos, sgl_strdup("Missing watch callback"));
      return sgl->end_pc;
    }

    sgl_ls_push(&sgl->task->watchers, &w->ls);
  }

  return op->next;
}

struct sgl_op *sgl_op_watch_beg_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_WATCH_BEG);
  op->run = watch_beg_run;
  op->as_watch_beg.watcher = NULL;
  return op;
}

static struct sgl_op *watch_end_run(struct sgl_op *op, struct sgl *sgl) {  
  struct sgl_ls *w = sgl_ls_pop(&sgl->task->watchers);
  assert(w);
  sgl_val_free(sgl_baseof(struct sgl_val, ls, w), sgl);
  return op->next;
}

struct sgl_op *sgl_op_watch_end_new(struct sgl *sgl, struct sgl_form *form) {
  struct sgl_op *op = sgl_op_new(sgl, form, &SGL_OP_WATCH_END);
  op->run = watch_end_run;
  return op;
}

void sgl_setup_ops() {
  sgl_op_type_init(&SGL_NOP, "nop");
  SGL_NOP.cinit_op = nop_cinit;
  SGL_NOP.const_op = nop_const;
  SGL_NOP.trace_op = nop_trace;
  
  sgl_op_type_init(&SGL_OP_BENCH, "bench");

  sgl_op_type_init(&SGL_OP_CALL, "call");
  SGL_OP_CALL.deinit_op = call_deinit;
  SGL_OP_CALL.dump_op = call_dump;
  SGL_OP_CALL.trace_op = call_trace;

  sgl_op_type_init(&SGL_OP_COUNT, "count");
  SGL_OP_COUNT.cinit_op = count_cinit;
  SGL_OP_COUNT.trace_op = count_trace;

  sgl_op_type_init(&SGL_OP_CURRY, "curry");
  SGL_OP_CURRY.deinit_op = curry_deinit;
  SGL_OP_CURRY.dump_op = curry_dump;
  SGL_OP_CURRY.trace_op = curry_trace;

  sgl_op_type_init(&SGL_OP_DEREF_BEG, "deref-beg");
  SGL_OP_DEREF_BEG.cinit_op = deref_beg_cinit;
  SGL_OP_DEREF_BEG.deinit_op = deref_beg_deinit;
  SGL_OP_DEREF_BEG.dump_op = deref_beg_dump;
  SGL_OP_DEREF_BEG.trace_op = deref_beg_trace;

  sgl_op_type_init(&SGL_OP_DEREF_END, "deref-end");
  SGL_OP_DEREF_END.cinit_op = deref_end_cinit;
  SGL_OP_DEREF_END.deinit_op = deref_end_deinit;
  SGL_OP_DEREF_END.trace_op = deref_end_trace;

  sgl_op_type_init(&SGL_OP_DEFER, "defer");
  SGL_OP_DEFER.cinit_op = defer_cinit;
  SGL_OP_DEFER.trace_op = defer_trace;

  sgl_op_type_init(&SGL_OP_DISPATCH, "dispatch");
  SGL_OP_DISPATCH.cinit_op = dispatch_cinit;
  SGL_OP_DISPATCH.dump_op = dispatch_dump;
  SGL_OP_DISPATCH.trace_op = dispatch_trace;
  
  sgl_op_type_init(&SGL_OP_DROP, "drop");
  SGL_OP_DROP.trace_op = drop_trace;

  sgl_op_type_init(&SGL_OP_DUP, "dup");
  SGL_OP_DUP.cinit_op = dup_cinit;
  SGL_OP_DUP.trace_op = dup_trace;

  sgl_op_type_init(&SGL_OP_DUPL, "dupl");
  SGL_OP_DUPL.cinit_op = dupl_cinit;
  SGL_OP_DUPL.deval_op = dupl_deval;
  SGL_OP_DUPL.trace_op = dupl_trace;

  sgl_op_type_init(&SGL_OP_EVAL_BEG, "eval-beg");
  SGL_OP_EVAL_BEG.dump_op = eval_beg_dump;
  SGL_OP_EVAL_BEG.trace_op = eval_beg_trace;

  sgl_op_type_init(&SGL_OP_EVAL_END, "eval-end");

  sgl_op_type_init(&SGL_OP_FIMP, "fimp");
  SGL_OP_FIMP.cinit_op = fimp_cinit;
  SGL_OP_FIMP.dump_op = fimp_dump;
  SGL_OP_FIMP.trace_op = fimp_trace;

  sgl_op_type_init(&SGL_OP_FIMP_CALL, "fimp-call");
  SGL_OP_FIMP_CALL.cinit_op = fimp_call_cinit;
  SGL_OP_FIMP_CALL.dump_op = fimp_call_dump;
  SGL_OP_FIMP_CALL.trace_op = fimp_call_trace;

  sgl_op_type_init(&SGL_OP_FIMP_RETS, "fimp-rets");
  SGL_OP_FIMP_RETS.cinit_op = fimp_rets_cinit;
  SGL_OP_FIMP_RETS.dump_op = fimp_rets_dump;
  SGL_OP_FIMP_RETS.trace_op = fimp_rets_trace;

  sgl_op_type_init(&SGL_OP_FOR, "for");
  SGL_OP_FOR.deinit_op = for_deinit;
  SGL_OP_FOR.dump_op = for_dump;
  SGL_OP_FOR.trace_op = for_trace;

  sgl_op_type_init(&SGL_OP_GET_FIELD, "get-field");
  SGL_OP_GET_FIELD.dump_op = field_dump;
  SGL_OP_GET_FIELD.trace_op = get_field_trace;
  
  sgl_op_type_init(&SGL_OP_GET_VAR, "get-var");
  SGL_OP_GET_VAR.dump_op = get_var_dump;
  SGL_OP_GET_VAR.const_op = get_var_const;
  SGL_OP_GET_VAR.trace_op = get_var_trace;
  
  sgl_op_type_init(&SGL_OP_IDLE, "idle");

  sgl_op_type_init(&SGL_OP_IF, "if");
  SGL_OP_IF.deinit_op = if_deinit;
  SGL_OP_IF.deval_op = if_deval;
  SGL_OP_IF.dump_op = if_dump;
  SGL_OP_IF.trace_op = if_trace;

  sgl_op_type_init(&SGL_OP_ITER, "iter");
  SGL_OP_ITER.trace_op = iter_trace;
  
  sgl_op_type_init(&SGL_OP_JUMP, "jump");
  SGL_OP_JUMP.dump_op = jump_dump;
  SGL_OP_JUMP.trace_op = jump_trace;
  
  sgl_op_type_init(&SGL_OP_LET_VAR, "let-var");
  SGL_OP_LET_VAR.dump_op = let_var_dump;
  SGL_OP_LET_VAR.trace_op = let_var_trace;
  SGL_OP_LET_VAR.deinit_op = let_var_deinit;

  sgl_op_type_init(&SGL_OP_LAMBDA, "lambda");
  SGL_OP_LAMBDA.deval_op = lambda_deval;
  SGL_OP_LAMBDA.dump_op = lambda_dump;
  SGL_OP_LAMBDA.trace_op = lambda_trace;
  
  sgl_op_type_init(&SGL_OP_LIST_BEG, "list-beg");
  SGL_OP_LIST_BEG.trace_op = list_beg_trace;

  sgl_op_type_init(&SGL_OP_LIST_END, "list-end");
  SGL_OP_LIST_END.trace_op = list_end_trace;

  sgl_op_type_init(&SGL_OP_NEW, "new");
  SGL_OP_NEW.cinit_op = new_cinit;
  SGL_OP_NEW.dump_op = new_dump;
  SGL_OP_NEW.trace_op = new_trace;

  sgl_op_type_init(&SGL_OP_PAIR, "pair");
  SGL_OP_PAIR.trace_op = pair_trace;

  sgl_op_type_init(&SGL_OP_PEEK_REG, "peek-reg");
  SGL_OP_PEEK_REG.trace_op = peek_reg_trace;

  sgl_op_type_init(&SGL_OP_POP_REG, "pop-reg");
  SGL_OP_POP_REG.trace_op = pop_reg_trace;
  
  sgl_op_type_init(&SGL_OP_PUSH, "push");
  SGL_OP_PUSH.cinit_op = push_cinit;
  SGL_OP_PUSH.const_op = push_const;
  SGL_OP_PUSH.dump_op = push_dump;
  SGL_OP_PUSH.trace_op = push_trace;
  SGL_OP_PUSH.deinit_op = push_deinit;

  sgl_op_type_init(&SGL_OP_PUSH_REG, "push-reg");
  SGL_OP_PUSH_REG.trace_op = push_reg_trace;
  
  sgl_op_type_init(&SGL_OP_PUT_FIELD, "put-field");
  SGL_OP_PUT_FIELD.dump_op = field_dump;

  sgl_op_type_init(&SGL_OP_RECALL, "recall");
  
  sgl_op_type_init(&SGL_OP_REF, "ref");
  SGL_OP_REF.trace_op = ref_trace;

  sgl_op_type_init(&SGL_OP_RETURN, "return");
  SGL_OP_RETURN.cinit_op = return_cinit;
  
  sgl_op_type_init(&SGL_OP_ROTL, "rotl");
  SGL_OP_ROTL.trace_op = rotl_trace;
  
  sgl_op_type_init(&SGL_OP_ROTR, "rotr");
  SGL_OP_ROTR.trace_op = rotr_trace;
  
  sgl_op_type_init(&SGL_OP_SCOPE_BEG, "scope-beg");
  SGL_OP_SCOPE_BEG.deinit_op = scope_beg_deinit;
  SGL_OP_SCOPE_BEG.trace_op = nop_trace;
  
  sgl_op_type_init(&SGL_OP_SCOPE_END, "scope-end");
  SGL_OP_SCOPE_END.trace_op = nop_trace;
  
  sgl_op_type_init(&SGL_OP_STDOUT_BEG, "stdout-beg");
  SGL_OP_STDOUT_BEG.deinit_op = stdout_beg_deinit;
  SGL_OP_STDOUT_BEG.dump_op = stdout_beg_dump;
  SGL_OP_STDOUT_BEG.trace_op = stdout_beg_trace;
  
  sgl_op_type_init(&SGL_OP_STDOUT_END, "stdout-end");
  SGL_OP_STDOUT_END.trace_op = nop_trace;
  
  sgl_op_type_init(&SGL_OP_STACK_LIST, "stack-list");
  SGL_OP_STACK_LIST.cinit_op = stack_list_cinit;
  SGL_OP_STACK_LIST.trace_op = stack_list_trace;

  sgl_op_type_init(&SGL_OP_STACK_SWITCH, "stack-switch");
  SGL_OP_STACK_SWITCH.dump_op = stack_switch_dump;
  
  sgl_op_type_init(&SGL_OP_STR_BEG, "str-beg");
  SGL_OP_STR_BEG.trace_op = str_beg_trace;
  
  sgl_op_type_init(&SGL_OP_STR_END, "str-end");
  SGL_OP_STR_END.trace_op = str_end_trace;
  
  sgl_op_type_init(&SGL_OP_STR_PUT, "str-put");
  SGL_OP_STR_PUT.deinit_op = str_put_deinit;
  SGL_OP_STR_PUT.dump_op = str_put_dump;
  SGL_OP_STR_PUT.trace_op = str_put_trace;
  
  sgl_op_type_init(&SGL_OP_SWAP, "swap");
  SGL_OP_SWAP.cinit_op = swap_cinit;
  SGL_OP_SWAP.trace_op = swap_trace;

  sgl_op_type_init(&SGL_OP_SYNC, "sync");
  SGL_OP_SYNC.cinit_op = sync_cinit;
  SGL_OP_SYNC.trace_op = sync_trace;

  sgl_op_type_init(&SGL_OP_TASK_BEG, "task-beg");
  SGL_OP_TASK_BEG.dump_op = task_beg_dump;
  SGL_OP_TASK_BEG.trace_op = task_beg_trace;

  sgl_op_type_init(&SGL_OP_TASK_END, "task-end");

  sgl_op_type_init(&SGL_OP_THROW, "throw");
  SGL_OP_THROW.dump_op = throw_dump;
  SGL_OP_THROW.trace_op = throw_trace;
  SGL_OP_THROW.deinit_op = throw_deinit;
  
  sgl_op_type_init(&SGL_OP_TIMES, "times");
  SGL_OP_TIMES.cinit_op = times_cinit;
  SGL_OP_TIMES.dump_op = times_dump;
  SGL_OP_TIMES.trace_op = times_trace;
  
  sgl_op_type_init(&SGL_OP_TRY_BEG, "try-beg");
  SGL_OP_TRY_BEG.trace_op = try_beg_trace;

  sgl_op_type_init(&SGL_OP_TRY_END, "try-end");
  SGL_OP_TRY_END.trace_op = try_end_trace;
  
  sgl_op_type_init(&SGL_OP_TYPE, "type");
  SGL_OP_TYPE.trace_op = type_trace;

  sgl_op_type_init(&SGL_OP_YIELD, "yield");
  SGL_OP_YIELD.trace_op = nop_trace;

  sgl_op_type_init(&SGL_OP_WATCH_BEG, "watch-beg");
  SGL_OP_WATCH_BEG.deinit_op = watch_beg_deinit;

  sgl_op_type_init(&SGL_OP_WATCH_END, "watch-end");
  SGL_OP_WATCH_END.trace_op = nop_trace;
}
