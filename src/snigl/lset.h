#ifndef SNIGL_LSET_H
#define SNIGL_LSET_H

#include "snigl/ls.h"
#include "snigl/util.h"

struct sgl_lset {
  sgl_ls_cmp_t cmp;
  struct sgl_ls root;
};

struct sgl_lset *sgl_lset_init(struct sgl_lset *s, sgl_ls_cmp_t cmp);

struct sgl_ls *sgl_lset_find(struct sgl_lset *s,
                             const void *key,
                             void *data,
                             bool *ok);

#endif
