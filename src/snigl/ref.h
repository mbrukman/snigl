#ifndef SNIGL_REF_H
#define SNIGL_REF_H

#include "snigl/int.h"
#include "snigl/ls.h"

struct sgl;
struct sgl_val;

struct sgl_ref {
  struct sgl_ls ls, watchers;
  struct sgl_val *val;
  bool is_changing;
  sgl_int_t nrefs;
};

struct sgl_ref *sgl_ref_new(struct sgl *sgl, struct sgl_val *val);
void sgl_ref_deref(struct sgl_ref *r, struct sgl *sgl);
struct sgl_val *sgl_ref_get(struct sgl_ref *r, struct sgl *sgl);
bool sgl_ref_set(struct sgl_ref *r, struct sgl *sgl, struct sgl_val *val);

#endif
