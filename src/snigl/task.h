#ifndef SNIGL_TASK_H
#define SNIGL_TASK_H

#include "snigl/list.h"
#include "snigl/vec.h"

struct sgl;
struct sgl_lib;
struct sgl_op;

struct sgl_task {
  struct sgl_ls ls;
  struct sgl_ls calls, stacks, reg_stack, scopes, stdouts, tries, watchers;
  struct sgl_vec libs;
  struct sgl_lib *lib;
  struct sgl_list main_stack, *stack;
  struct sgl_op *end_pc, *pc;
};

struct sgl_task *sgl_task_init(struct sgl_task *task,
                               struct sgl *sgl,
                               struct sgl_lib *lib,
                               struct sgl_scope *parent_scope,
                               struct sgl_op *beg_pc,
                               struct sgl_op *end_pc);

struct sgl_task *sgl_task_deinit(struct sgl_task *task, struct sgl *sgl);

struct sgl_task *sgl_task_new(struct sgl *sgl,
                              struct sgl_lib *lib,
                              struct sgl_scope *parent_scope,
                              struct sgl_op *beg_pc,
                              struct sgl_op *end_pc);

void sgl_task_free(struct sgl_task *t, struct sgl *sgl);
void sgl_task_end_scopes(struct sgl_task *t, struct sgl *sgl);

#endif
