#include <assert.h>
#include <stdio.h>

#include "snigl/lib.h"
#include "snigl/pool.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"
#include "snigl/type.h"
#include "snigl/val.h"

struct sgl_val *sgl_val_init(struct sgl_val *val,
                             struct sgl *sgl,
                             struct sgl_type *type,
                             struct sgl_ls *root) {
  val->type = type;
  val->op = NULL;
  
  if (root) {
    sgl_ls_push(root, &val->ls);
  } else {
    sgl_ls_init(&val->ls);
  }
  
  return val;
}

struct sgl_val *sgl_val_reuse(struct sgl_val *val,
                              struct sgl *sgl,
                              struct sgl_type *type,
                              bool deinit) {
  if (deinit) { sgl_val_deinit(val, sgl); }
  val->type = type;
  return val;
}

struct sgl_val *sgl_val_deinit(struct sgl_val *val, struct sgl *sgl) {
  if (val->type->deinit_val) { val->type->deinit_val(val, sgl); }
  return val;
}

struct sgl_val *sgl_val_new(struct sgl *sgl,
                            struct sgl_type *type,
                            struct sgl_ls *root) {
  return sgl_val_init(sgl_malloc(&sgl->val_pool), sgl, type, root);
}

void sgl_val_free(struct sgl_val *val, struct sgl *sgl) {
  sgl_free(&sgl->val_pool, sgl_val_deinit(val, sgl));
}

bool sgl_val_bool(struct sgl_val *val, struct sgl *sgl) {
  return val->type->bool_val ? val->type->bool_val(val, sgl) : true;
}

struct sgl_op *sgl_val_call(struct sgl_val *val,
                            struct sgl *sgl,
                            struct sgl_pos *pos,
                            struct sgl_op *return_pc,
                            bool now) {
  if (!val->type->call_val) {
    sgl_val_dup(val, sgl, sgl_stack(sgl));
    return return_pc->next;
  }

  return val->type->call_val(val, sgl, pos, return_pc, now);
}

struct sgl_val *sgl_val_clone(struct sgl_val *val,
                              struct sgl *sgl,
                              struct sgl_pos *pos,
                              struct sgl_ls *root) {
  struct sgl_val *out = sgl_val_new(sgl, val->type, root);
  
  if (val->type->clone_val) {
    if (!val->type->clone_val(val, sgl, pos, out)) {
      sgl_val_free(out, sgl);
      return NULL;
    }
  } else {
    assert(val->type->dup_val);
    val->type->dup_val(val, sgl, out);
  }
  
  return out;
}

enum sgl_cmp sgl_val_cmp(struct sgl_val *val, const struct sgl_val *rhs) {
  assert(val->type->cmp_val);

  return (sgl_derived(rhs->type, val->type) || sgl_derived(val->type, rhs->type))
    ? val->type->cmp_val(val, rhs)
    : sgl_ptr_cmp(val->type, rhs->type);
}

void sgl_val_dump(struct sgl_val *val, struct sgl_buf *out) {
  assert(val->type->dump_val);
  val->type->dump_val(val, out);
}

struct sgl_val *sgl_val_dup(struct sgl_val *val,
                            struct sgl *sgl,
                            struct sgl_ls *root) {
  assert(val->type->dup_val);
  struct sgl_val *out = sgl_val_new(sgl, val->type, root);
  val->type->dup_val(val, sgl, out);
  return out;
}

bool sgl_val_is(struct sgl_val *v, struct sgl_val *rhs) {
  assert(v->type->is_val);
  return (rhs->type == v->type) ? v->type->is_val(v, rhs) : false;
}

bool sgl_val_eq(struct sgl_val *val, struct sgl *sgl, struct sgl_val *rhs) {
  if (!val->type->eq_val) { return sgl_val_is(val, rhs); }

  if (!sgl_derived(rhs->type, val->type) && !sgl_derived(val->type, rhs->type)) {
    return false;
  }

  return val->type->eq_val(val, sgl, rhs);
}

struct sgl_iter *sgl_val_iter(struct sgl_val *val,
                              struct sgl *sgl,
                              struct sgl_type **type) {
  if (!val->type->iter_val) {
    sgl_error(sgl, sgl_pos(sgl),
              sgl_sprintf("Not iterable: %s", sgl_type_id(val->type)->id));
    
    return NULL;
  }
  
  return val->type->iter_val(val, sgl, type);
}

sgl_uint_t sgl_val_hash(struct sgl_val *v) {
  assert(v->type->hash_val);
  return v->type->hash_val(v);
}

void sgl_val_print(struct sgl_val *val,
                   struct sgl *sgl,
                   struct sgl_pos *pos,
                   struct sgl_buf *out) {
  if (val->type->print_val) {
    val->type->print_val(val, sgl, pos, out);
  } else {
    sgl_val_dump(val, out);
  }
}

bool sgl_val_cemit(struct sgl_val *val,
                   struct sgl *sgl,
                   struct sgl_pos *pos,
                   const char *id,
                   const char *root,
                   struct sgl_cemit *out) {
  struct sgl_type *t = val->type;
  
  if (!t->cemit_val) {
    sgl_error(sgl, pos,
              sgl_sprintf("cemit not implemented by type: %s", sgl_type_id(t)->id));

    return false;
  }

  return t->cemit_val(val, sgl, pos, id, root, out);
}
