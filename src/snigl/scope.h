#ifndef SNIGL_SCOPE_H
#define SNIGL_SCOPE_H

#include "snigl/config.h"
#include "snigl/hash.h"

struct sgl;

struct sgl_scope {
  struct sgl_ls ls;
  struct sgl_scope *parent;
  struct sgl_hash vars;
  struct sgl_ls defers;
  sgl_int_t nrefs;
};

struct sgl_scope *sgl_scope_new(struct sgl *sgl, struct sgl_scope *parent);
struct sgl_scope *sgl_scope_init(struct sgl_scope *s, struct sgl_scope *parent);
struct sgl_scope *sgl_scope_deinit(struct sgl_scope *s, struct sgl *sgl);
void sgl_scope_free(struct sgl_scope *s, struct sgl *sgl);
void sgl_scope_deref(struct sgl_scope *s, struct sgl *sgl);
void sgl_scope_end(struct sgl_scope *s, struct sgl *sgl);

#endif
