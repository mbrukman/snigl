#ifndef SNIGL_HASH_H
#define SNIGL_HASH_H

#include "snigl/int.h"
#include "snigl/list.h"
#include "snigl/vec.h"

#define _sgl_hash_slots_do(h, s)                \
  if (h->slots)                                 \
    for (struct sgl_ls *s = h->slots;           \
         s < h->slots + h->nslots;              \
         s++)                                   \
      
#define sgl_hash_slots_do(h, s)                 \
  _sgl_hash_slots_do((h), s)                    \

struct sgl_hash {
  sgl_int_t nslots, len;
  struct sgl_ls *slots;
  bool full;

  sgl_uint_t (*fn)(void *x);
  bool (*is)(void *x, void *y);
};

struct sgl_hash_node {
  struct sgl_ls ls;
  sgl_uint_t hkey;
  void *key, *val;
};

struct sgl_hash *sgl_hash_init(struct sgl_hash *h);
struct sgl_hash *sgl_hash_deinit(struct sgl_hash *h, struct sgl *sgl);
bool sgl_hash_init_slots(struct sgl_hash *h, struct sgl *sgl);
sgl_uint_t sgl_hash_key(struct sgl_hash *h, void *key);
struct sgl_ls *sgl_hash_slot(struct sgl_hash *h, sgl_uint_t hkey);

struct sgl_hash_node *sgl_hash_find(struct sgl_hash *h,
                                    struct sgl_ls *slot,
                                    sgl_uint_t hkey,
                                    void *key);

struct sgl_hash_node *sgl_hash_add(struct sgl_hash *h,
                                   struct sgl *sgl,
                                   struct sgl_ls *slot,
                                   sgl_uint_t hkey,
                                   void *key,
                                   void *val);

#endif
