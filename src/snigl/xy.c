#include "snigl/xy.h"

struct sgl_xy *sgl_xy_init(struct sgl_xy *xy, sgl_int_t x, sgl_int_t y) {
  xy->x = x;
  xy->y = y;
  return xy;
}
