#ifndef SNIGL_OP_H
#define SNIGL_OP_H

#include "snigl/config.h"

#include "snigl/ls.h"

struct sgl;
struct sgl_buf;
struct sgl_cemit;
struct sgl_form;
struct sgl_field;
struct sgl_trace;
struct sgl_val;

struct sgl_op_bench { struct sgl_op *end_pc; };
struct sgl_op_call { struct sgl_val *val; };
struct sgl_op_count { struct sgl_op *pc; };
struct sgl_op_curry { struct sgl_val *args; };
struct sgl_op_deref_beg { struct sgl_val *val; };
struct sgl_op_deref_end { struct sgl_val *val; };

struct sgl_op_defer {
  struct sgl_ls ls;
  struct sgl_op *beg_pc, *end_pc;
};

struct sgl_op_dispatch {
  struct sgl_func *func;
  struct sgl_fimp *fimp;
};

struct sgl_op_drop { sgl_int_t nvals; };
struct sgl_op_eval_beg { struct sgl_op *end_pc; };

struct sgl_op_field {
  struct sgl_type *struct_type;
  struct sgl_field *field;
  struct sgl_sym *id;
  struct sgl_val *val;
  bool use_reg;
};

struct sgl_op_fimp { struct sgl_fimp *fimp; };
struct sgl_op_fimp_call { struct sgl_fimp *fimp; };

struct sgl_op_fimp_rets {
  struct sgl_fimp *fimp;
  bool check[SGL_MAX_RETS];
};

struct sgl_op_for {
  struct sgl_op *end_pc;
  struct sgl_val *src;
};

struct sgl_op_get_var {
  struct sgl_sym *id;
  struct sgl_val *val;
};

struct sgl_op_idle { struct sgl_op *end_pc; };

struct sgl_op_if {
  bool neg;
  struct sgl_op *pc;
  struct sgl_val *cond, *val;
  bool pop;
};

struct sgl_op_iter { struct sgl_op *pc; };
struct sgl_op_jump { struct sgl_op *pc; };

struct sgl_op_let_var {
  struct sgl_sym *id;
  struct sgl_val *val;
};

struct sgl_op_lambda {
  struct sgl_op *beg_pc, *end_pc;
  struct sgl_type *type;
  bool push_val;
};

struct sgl_op_new { struct sgl_type *type; };
struct sgl_op_pop_reg { bool drop; };
struct sgl_op_push { struct sgl_val *val; };
struct sgl_op_recall { struct sgl_func *func; };
struct sgl_op_return { struct sgl_op *beg_pc; };
struct sgl_op_scope_beg { struct sgl_scope *parent; };
struct sgl_op_scope_end { struct sgl_op *beg_pc; };
struct sgl_op_stdout_beg { struct sgl_val *buf; };
struct sgl_op_str_put { struct sgl_val *val; };

struct sgl_op_stack_switch {
  sgl_int_t n;
  bool pop;
};

struct sgl_op_sync { sgl_int_t n; };
struct sgl_op_task_beg { struct sgl_op *end_pc; };
struct sgl_op_task_end { struct sgl_op *beg_pc; };
struct sgl_op_throw { struct sgl_val *val; };
struct sgl_op_times { struct sgl_val *reps; };
struct sgl_op_try_beg { struct sgl_op *end_pc; };
struct sgl_op_watch_beg { struct sgl_val *watcher; };

struct sgl_op;

struct sgl_op_type {
  const char *id;
  struct sgl_op *(*cinit_op)(struct sgl_op *, struct sgl *, struct sgl_cemit *);
  struct sgl_op *(*cemit_op)(struct sgl_op *, struct sgl *, struct sgl_cemit *);
  bool (*const_op)(struct sgl_op *);
  void (*deinit_op)(struct sgl_op *, struct sgl *);
  void (*deval_op)(struct sgl_op *, struct sgl *, struct sgl_val *);
  void (*dump_op)(struct sgl_op *, struct sgl_buf *);
  bool (*trace_op)(struct sgl_op *, struct sgl *, struct sgl_trace *);
};

struct sgl_op_type *sgl_op_type_init(struct sgl_op_type *type, const char *id);

extern struct sgl_op_type SGL_NOP,
  SGL_OP_BENCH,
  SGL_OP_CALL, SGL_OP_COUNT, SGL_OP_CURRY,
  SGL_OP_DEREF_BEG, SGL_OP_DEREF_END, SGL_OP_DEFER, SGL_OP_DISPATCH, SGL_OP_DROP,
  SGL_OP_DUP, SGL_OP_DUPL,
  SGL_OP_EVAL_BEG, SGL_OP_EVAL_END,
  SGL_OP_FIMP, SGL_OP_FIMP_CALL, SGL_OP_FIMP_RETS, SGL_OP_FOR,
  SGL_OP_GET_FIELD, SGL_OP_GET_VAR,
  SGL_OP_IDLE, SGL_OP_IF, SGL_OP_ITER,
  SGL_OP_JUMP,
  SGL_OP_LAMBDA, SGL_OP_LET_VAR, SGL_OP_LIST_BEG, SGL_OP_LIST_END,
  SGL_OP_NEW,
  SGL_OP_PAIR, SGL_OP_PEEK_REG, SGL_OP_POP_REG, SGL_OP_PUSH, SGL_OP_PUSH_REG,
  SGL_OP_PUT_FIELD,
  SGL_OP_RECALL, SGL_OP_REF, SGL_OP_RETURN, SGL_OP_ROTL, SGL_OP_ROTR,
  SGL_OP_SCOPE_BEG, SGL_OP_SCOPE_END, SGL_OP_STACK_LIST, SGL_OP_STACK_SWITCH,
  SGL_OP_STDOUT_BEG, SGL_OP_STDOUT_END, SGL_OP_STR_BEG, SGL_OP_STR_END,
  SGL_OP_STR_PUT, SGL_OP_SWAP, SGL_OP_SYNC,
  SGL_OP_TASK_BEG, SGL_OP_TASK_END,
  SGL_OP_THROW, SGL_OP_TIMES,
  SGL_OP_TRY_BEG, SGL_OP_TRY_END,
  SGL_OP_YIELD,
  SGL_OP_WATCH_BEG, SGL_OP_WATCH_END;

struct sgl_op {
  struct sgl_form *form;
  struct sgl_ls ls;
  struct sgl_op_type *type;
  struct sgl_op *prev, *next;
  sgl_int_t pc, cemit_pc;
  
  struct sgl_op *(*run)(struct sgl_op *, struct sgl *);

  union {
    struct sgl_op_bench as_bench;
    struct sgl_op_call as_call;
    struct sgl_op_count as_count;
    struct sgl_op_curry as_curry;
    struct sgl_op_deref_beg as_deref_beg;
    struct sgl_op_deref_end as_deref_end;
    struct sgl_op_defer as_defer;
    struct sgl_op_dispatch as_dispatch;
    struct sgl_op_drop as_drop;
    struct sgl_op_eval_beg as_eval_beg;
    struct sgl_op_fimp as_fimp;
    struct sgl_op_fimp_call as_fimp_call;
    struct sgl_op_fimp_rets as_fimp_rets;
    struct sgl_op_for as_for;
    struct sgl_op_field as_field;
    struct sgl_op_get_var as_get_var;
    struct sgl_op_idle as_idle;
    struct sgl_op_if as_if;
    struct sgl_op_iter as_iter;
    struct sgl_op_jump as_jump;
    struct sgl_op_lambda as_lambda;
    struct sgl_op_let_var as_let_var;
    struct sgl_op_new as_new;
    struct sgl_op_pop_reg as_pop_reg;
    struct sgl_op_push as_push;
    struct sgl_op_recall as_recall;
    struct sgl_op_return as_return;
    struct sgl_op_scope_beg as_scope_beg;
    struct sgl_op_scope_end as_scope_end;
    struct sgl_op_stack_switch as_stack_switch;
    struct sgl_op_stdout_beg as_stdout_beg;
    struct sgl_op_str_put as_str_put;
    struct sgl_op_sync as_sync;
    struct sgl_op_task_beg as_task_beg;
    struct sgl_op_task_end as_task_end;
    struct sgl_op_throw as_throw;
    struct sgl_op_times as_times;
    struct sgl_op_try_beg as_try_beg;
    struct sgl_op_watch_beg as_watch_beg;
  };
};

struct sgl_op *sgl_op_init(struct sgl_op *op,
                           struct sgl *sgl,
                           struct sgl_form *form,
                           struct sgl_op_type *type);

struct sgl_op *sgl_op_deinit(struct sgl_op *op, struct sgl *sgl);

struct sgl_op *sgl_op_new(struct sgl *sgl,
                          struct sgl_form *form,
                          struct sgl_op_type *type);

bool sgl_op_const(struct sgl_op *op);
void sgl_op_free(struct sgl_op *op, struct sgl *sgl);
bool sgl_op_is_val(struct sgl_op *op);

struct sgl_op *sgl_op_cinit(struct sgl_op *op,
                            struct sgl *sgl,
                            struct sgl_cemit *out);

struct sgl_op *sgl_op_cemit(struct sgl_op *op,
                            struct sgl *sgl,
                            struct sgl_cemit *cemit);

void sgl_op_dump(struct sgl_op *op, struct sgl_buf *out);
bool sgl_op_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t);

struct sgl_op *sgl_op_bench_new(struct sgl *sgl, struct sgl_form *form);

struct sgl_op *sgl_op_call_new(struct sgl *sgl,
                               struct sgl_form *form,
                               struct sgl_val *val);

void sgl_op_call_reuse(struct sgl_op *op, struct sgl *sgl);

struct sgl_op *sgl_op_count_new(struct sgl *sgl,
                                struct sgl_form *form,
                                struct sgl_op *pc);

struct sgl_op *sgl_op_curry_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_deref_beg_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_deref_end_new(struct sgl *sgl, struct sgl_form *form);

struct sgl_op *sgl_op_defer_new(struct sgl *sgl, struct sgl_form *form);

struct sgl_op *sgl_op_dispatch_new(struct sgl *sgl,
                                   struct sgl_form *form,
                                   struct sgl_func *func,
                                   struct sgl_fimp *fimp);

struct sgl_op *sgl_op_drop_new(struct sgl *sgl,
                               struct sgl_form *form,
                               sgl_int_t nvals);

struct sgl_op *sgl_op_dup_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_dupl_new(struct sgl *sgl, struct sgl_form *form);
void sgl_op_dupl_reuse(struct sgl_op *op, struct sgl *sgl);

struct sgl_op *sgl_op_eval_beg_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_eval_end_new(struct sgl *sgl, struct sgl_form *form);

struct sgl_op *sgl_op_fimp_new(struct sgl *sgl,
                               struct sgl_form *form,
                               struct sgl_fimp *fimp);

struct sgl_op *sgl_op_fimp_call_new(struct sgl *sgl,
                                    struct sgl_form *form,
                                    struct sgl_fimp *fimp);

struct sgl_op *sgl_op_fimp_rets_new(struct sgl *sgl,
                                    struct sgl_form *form,
                                    struct sgl_fimp *fimp);

struct sgl_op *sgl_op_get_field_new(struct sgl *sgl,
                                    struct sgl_form *form,
                                    struct sgl_sym *id);

void sgl_op_fimp_call_reuse(struct sgl_op *op,
                            struct sgl *sgl,
                            struct sgl_fimp *fimp);

struct sgl_op *sgl_op_for_new(struct sgl *sgl,
                              struct sgl_form *form,
                              struct sgl_op *end_pc);

struct sgl_op *sgl_op_get_var_new(struct sgl *sgl,
                                  struct sgl_form *form,
                                  struct sgl_sym *id);

struct sgl_op *sgl_op_idle_new(struct sgl *sgl, struct sgl_form *form);

struct sgl_op *sgl_op_if_new(struct sgl *sgl,
                             struct sgl_form *form,
                             struct sgl_op *pc);

struct sgl_op *sgl_op_iter_new(struct sgl *sgl,
                               struct sgl_form *form,
                               struct sgl_op *pc);

struct sgl_op *sgl_op_jump_new(struct sgl *sgl,
                               struct sgl_form *form,
                               struct sgl_op *pc);

struct sgl_op *sgl_op_lambda_new(struct sgl *sgl, struct sgl_form *form);

struct sgl_op *sgl_op_let_var_new(struct sgl *sgl,
                                  struct sgl_form *form,
                                  struct sgl_sym *id);

struct sgl_op *sgl_op_list_beg_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_list_end_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_new_new(struct sgl *sgl, struct sgl_form *form);

void sgl_op_deval(struct sgl_op *op, struct sgl *sgl, struct sgl_val *val);
void sgl_nop(struct sgl_op *op, struct sgl *sgl);
struct sgl_op *sgl_nop_new(struct sgl *sgl, struct sgl_form *form);

struct sgl_op *sgl_op_pair_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_peek_reg_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_pop_reg_new(struct sgl *sgl, struct sgl_form *form, bool drop);

struct sgl_op *sgl_op_push_new(struct sgl *sgl,
                               struct sgl_form *form,
                               struct sgl_val *val);

struct sgl_op *sgl_op_push_reg_new(struct sgl *sgl, struct sgl_form *form);
void sgl_op_push_reuse(struct sgl_op *op, struct sgl *sgl, struct sgl_val *val);

struct sgl_op *sgl_op_put_field_new(struct sgl *sgl,
                                    struct sgl_form *form,
                                    struct sgl_sym *id,
                                    struct sgl_val *val);

struct sgl_op *sgl_op_recall_new(struct sgl *sgl,
                                 struct sgl_form *form,
                                 struct sgl_func *func);

struct sgl_op *sgl_op_ref_new(struct sgl *sgl, struct sgl_form *form);

struct sgl_op *sgl_op_return_new(struct sgl *sgl,
                                 struct sgl_form *form,
                                 struct sgl_op *beg_pc);

struct sgl_op *sgl_op_rotl_new(struct sgl *sgl, struct sgl_form *form);
void sgl_op_rotl_reuse(struct sgl_op *op, struct sgl *sgl);

struct sgl_op *sgl_op_rotr_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_scope_beg_new(struct sgl *sgl, struct sgl_form *form);

struct sgl_op *sgl_op_scope_end_new(struct sgl *sgl,
                                    struct sgl_form *form,
                                    struct sgl_op *beg_pc);

struct sgl_op *sgl_op_stack_list_new(struct sgl *sgl, struct sgl_form *form);

struct sgl_op *sgl_op_stack_switch_new(struct sgl *sgl,
                                       struct sgl_form *form,
                                       sgl_int_t n,
                                       bool pop);

struct sgl_op *sgl_op_stdout_beg_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_stdout_end_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_str_beg_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_str_put_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_str_end_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_swap_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_sync_new(struct sgl *sgl, struct sgl_form *form, sgl_int_t n);
struct sgl_op *sgl_op_task_beg_new(struct sgl *sgl, struct sgl_form *form);

struct sgl_op *sgl_op_task_end_new(struct sgl *sgl,
                                   struct sgl_form *form,
                                   struct sgl_op *beg_pc);

struct sgl_op *sgl_op_throw_new(struct sgl *sgl, struct sgl_form *form);

struct sgl_op *sgl_op_times_new(struct sgl *sgl,
                                struct sgl_form *form,
                                struct sgl_val *reps);

struct sgl_op *sgl_op_try_beg_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_try_end_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_type_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_yield_new(struct sgl *sgl, struct sgl_form *form);

struct sgl_op *sgl_op_watch_beg_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_watch_end_new(struct sgl *sgl, struct sgl_form *form);

void sgl_setup_ops();

#endif
