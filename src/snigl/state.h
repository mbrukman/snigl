#ifndef SNIGL_STATE_H
#define SNIGL_STATE_H

struct sgl;
struct sgl_ls;

struct sgl_state {
  struct sgl_lib *lib;
  struct sgl_ls *call, *reg_stackp, *scope, *try;
};

struct sgl_state *sgl_state_init(struct sgl_state *state, struct sgl *sgl);
void sgl_state_restore(struct sgl_state *state, struct sgl *sgl);
void sgl_state_restore_calls(struct sgl_state *state, struct sgl *sgl);

#endif
