#ifndef SNIGL_SGL_H
#define SNIGL_SGL_H

#include "snigl/config.h"

#ifdef SGL_USE_ASYNC
#include <pthread.h>
#endif

#include <stdio.h>

#include "snigl/buf.h"
#include "snigl/hash.h"
#include "snigl/int.h"
#include "snigl/lib.h"
#include "snigl/ls.h"
#include "snigl/lset.h"
#include "snigl/op.h"
#include "snigl/pool.h"
#include "snigl/pos.h"
#include "snigl/scope.h"
#include "snigl/task.h"
#include "snigl/type.h"
#include "snigl/val.h"

#define sgl_use(sgl, pos, src, ...)                                 \
  _sgl_use(sgl, pos, src, (struct sgl_sym *[]){__VA_ARGS__, NULL})  \
  
#define _sgl_lift(_prev, _done, sgl, fn)                        \
  sgl_compile_t _prev = sgl->lift_compile;                      \
  bool _done = false;                                           \
  for (sgl->lift_compile = fn;                                  \
       !_done;                                                  \
       sgl->lift_compile = _prev, _done = true)                 \
    

#define sgl_lift(sgl, fn)                             \
  _sgl_lift(SGL_CUID(prev), SGL_CUID(done), sgl, fn)  \
  
extern const sgl_int_t SGL_VERSION[3];

struct sgl_io;
struct sgl_val;

typedef struct sgl_ls *(*sgl_compile_t)(struct sgl_form *,
                                        struct sgl *,
                                        struct sgl_ls *);
struct sgl {
  sgl_compile_t lift_compile;
  bool debug;
  sgl_int_t trace;
  
  struct sgl_pool call_pool,
    enum_type_pool, error_pool,
    fimp_pool, field_pool, form_pool,
    hash_node_pool, hash_slots_pool,
    lib_pool,
    macro_pool,
    op_pool,
    pmacro_pool,
    ref_type_pool,
    scope_pool, struct_type_pool, sym_pool,
    task_pool, trace_pool, try_pool, type_pool,
    val_pool;

  struct sgl_lset libs;
  struct sgl_hash syms;
  struct sgl_ls errors, ops, tasks;
  sgl_int_t nsyms, ntasks;
  
  struct sgl_lib home_lib, *abc_lib;
  struct sgl_task main_task, *task;
  struct sgl_scope main_scope;
  struct sgl_buf stdout;
  
  sgl_int_t type_tag;
  struct sgl_type *A,
    *Bool, *Buf,
    *Char, *Curry,
    *Enum, *EnumIter, *Error,
    *File, *FileLineIter, *FileMode, *FilterIter, *Fix, *Form, *Func,
    *HashMap,
    *Int, *IntIter, *Iter,
    *Lambda, *List, *ListIter, *ListSet,
    *Macro, *MapIter, *Meta,
    *Nil, *Num,
    *Pair,
    *RGB, *Ref,
    *Seq, *Str, *StrIter, *Struct, *StructMeta, *Sym,
    *T, *Time,
    *Undef,
    *XY;

  struct sgl_val argv, empty_str, nil, t, f;
  struct sgl_fimp *call_fimp, *clone_fimp, *eq_fimp, *iter_fimp;

  struct sgl_vec load_paths;
  char *load_path;
  
  struct sgl_op *beg_pc, *end_pc, *pc;
  struct sgl_fimp *c_fimp;
  
#ifdef SGL_USE_DL
  struct sgl_vec links;
#endif
  
#ifdef SGL_USE_ASYNC
  struct sgl_pool io_pool, io_thread_pool;
  sgl_int_t io_depth, sync_depth;
  
  struct sgl_ls io_threads;
  sgl_int_t max_io_threads, nio_threads;
  sgl_int_t _Atomic nio_idle_threads;
  
  struct sgl_ls io_queue;

  pthread_cond_t io_done;
  sgl_int_t _Atomic nio_done;
  
  pthread_mutex_t io_lock;
#endif
};

void sgl_setup();

struct sgl *sgl_init(struct sgl *sgl);
void sgl_init_args(struct sgl *sgl, sgl_int_t argc, const char *argv[]);
struct sgl *sgl_deinit(struct sgl *sgl);

struct sgl_error *sgl_error(struct sgl *sgl, struct sgl_pos *pos, char *msg);

struct sgl_op *sgl_throw(struct sgl *sgl,
                         struct sgl_pos *pos,
                         char *msg,
                         struct sgl_val *val);

struct sgl_op *sgl_throw_error(struct sgl *sgl, struct sgl_error *e);
struct sgl_op *sgl_throw_errors(struct sgl *sgl);

struct sgl_sym *sgl_nsym(struct sgl *sgl, const char *id, sgl_int_t len);
struct sgl_sym *sgl_sym(struct sgl *sgl, const char *id);
struct sgl_sym *sgl_gsym(struct sgl *sgl);

bool sgl_add_lib(struct sgl *sgl, struct sgl_pos *pos, struct sgl_lib *lib);

struct sgl_lib *sgl_find_lib(struct sgl *sgl,
                             struct sgl_pos *pos,
                             struct sgl_sym *id);

struct sgl_lib *sgl_lib(struct sgl *sgl);
void sgl_beg_lib(struct sgl *sgl, struct sgl_lib *lib);
struct sgl_lib *sgl_end_lib(struct sgl *sgl);

bool _sgl_use(struct sgl *sgl,
              struct sgl_pos *pos,
              struct sgl_lib *src,
              struct sgl_sym *defs[]);

bool sgl_add_const(struct sgl *sgl,
                   struct sgl_pos *pos,
                   struct sgl_sym *id,
                   struct sgl_val *val);

struct sgl_def *sgl_find_def(struct sgl *sgl,
                             struct sgl_pos *pos,
                             struct sgl_sym *id);

struct sgl_type *sgl_find_type(struct sgl *sgl,
                               struct sgl_pos *pos,
                               struct sgl_sym *id);

struct sgl_type *sgl_get_type(struct sgl *sgl,
                              struct sgl_pos *pos,
                              struct sgl_sym *id);

struct sgl_pmacro *sgl_find_pmacro(struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_sym *id);

struct sgl_macro *sgl_find_macro(struct sgl *sgl,
                                 struct sgl_pos *pos,
                                 struct sgl_sym *id);

struct sgl_func *sgl_find_func(struct sgl *sgl,
                               struct sgl_pos *pos,
                               struct sgl_sym *id);

struct sgl_func *sgl_get_func(struct sgl *sgl,
                              struct sgl_pos *pos,
                              struct sgl_sym *id);

struct sgl_scope *sgl_scope(struct sgl *sgl);
void sgl_beg_scope(struct sgl *sgl, struct sgl_scope *parent);
void sgl_end_scope(struct sgl *sgl);

struct sgl_call *sgl_call(struct sgl *sgl);

void sgl_beg_stdout(struct sgl *sgl, struct sgl_buf *buf);
struct sgl_buf *sgl_end_stdout(struct sgl *sgl);
struct sgl_buf *sgl_stdout(struct sgl *sgl);
bool sgl_flush_stdout(struct sgl *sgl);

bool sgl_is_async(struct sgl *sgl);

#ifdef SGL_USE_ASYNC
struct sgl_io_thread *sgl_io_thread(struct sgl *sgl);
bool sgl_run_io(struct sgl *sgl, struct sgl_io *io);
void sgl_stop_io_threads(struct sgl *sgl);
#endif
  
struct sgl_op *sgl_yield(struct sgl *sgl);

struct sgl_ls *sgl_reg_stack(struct sgl *sgl);
struct sgl_val *sgl_push_reg(struct sgl *sgl, struct sgl_type *type);
struct sgl_val *sgl_peek_reg(struct sgl *sgl, bool dup, struct sgl_ls *root);
struct sgl_val *sgl_pop_reg(struct sgl *sgl);

struct sgl_ls *sgl_stack(struct sgl *sgl);
struct sgl_val *sgl_push(struct sgl *sgl, struct sgl_type *type);
struct sgl_val *sgl_peek(struct sgl *sgl);
struct sgl_val *sgl_pop(struct sgl *sgl);
void sgl_reset_stack(struct sgl *sgl);

struct sgl_list *sgl_beg_stack(struct sgl *sgl);
struct sgl_list *sgl_end_stack(struct sgl *sgl);
struct sgl_list *sgl_switch_stack(struct sgl *sgl, sgl_int_t n);

struct sgl_val *sgl_get_var(struct sgl *sgl, struct sgl_sym *id);

bool sgl_let_var(struct sgl *sgl,
                 struct sgl_pos *pos,
                 struct sgl_sym *id,
                 struct sgl_val *val);

bool sgl_compile(struct sgl *sgl, struct sgl_ls *in);
void sgl_dump_ops(struct sgl *sgl, FILE *out);
bool sgl_cemit(struct sgl *sgl, struct sgl_op *pc, struct sgl_cemit *out);
char *sgl_load_path(struct sgl *sgl, const char *fname);
bool sgl_load(struct sgl *sgl, struct sgl_pos *pos, char *path);
sgl_int_t sgl_trace(struct sgl *sgl, struct sgl_op *pc);
struct sgl_pos *sgl_pos(struct sgl *sgl);

bool sgl_run(struct sgl *sgl, struct sgl_op *pc);

bool sgl_run_task(struct sgl *sgl,
                  struct sgl_task *task,
                  struct sgl_op *pc, struct sgl_op *end_pc);

void sgl_defer(struct sgl *sgl, struct sgl_op_defer *op);
void sgl_dump_errors(struct sgl *sgl, FILE *out);

#endif
