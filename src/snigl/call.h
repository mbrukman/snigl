#ifndef SNIGL_CALL_H
#define SNIGL_CALL_H

#include "snigl/ls.h"
#include "snigl/pos.h"
#include "snigl/state.h"

struct sgl;

struct sgl_call {
  struct sgl_ls ls;
  struct sgl_pos pos;
  struct sgl_state state;
  struct sgl_op *beg_pc, *return_pc;
};

struct sgl_call *sgl_call_init(struct sgl_call *c,
                               struct sgl *sgl,
                               struct sgl_pos *pos,
                               struct sgl_op *beg_pc,
                               struct sgl_op *return_pc);

struct sgl_call *sgl_call_new(struct sgl *sgl,
                              struct sgl_pos *pos,
                              struct sgl_op *beg_pc,
                              struct sgl_op *return_pc);

void sgl_call_free(struct sgl_call *call, struct sgl *sgl);

#endif
