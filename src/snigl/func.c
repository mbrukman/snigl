#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "snigl/cemit.h"
#include "snigl/fimp.h"
#include "snigl/func.h"
#include "snigl/lib.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"

struct sgl_def_type SGL_DEF_FUNC;

static enum sgl_cmp fimps_cmp(struct sgl_ls *lhs, const void *rhs, void *_) {
  return sgl_ptr_cmp(sgl_baseof(struct sgl_fimp, ls, lhs)->id, rhs);
}

static enum sgl_cmp fimp_queue_cmp(struct sgl_ls *lhs, const void *rhs, void *_) {
  return sgl_int_cmp(sgl_baseof(struct sgl_fimp, queue_ls, lhs)->weight,
                     *(sgl_int_t *)rhs);
}

struct sgl_func *sgl_func_init(struct sgl_func *func,
                               struct sgl *sgl,
                               struct sgl_pos *pos,
                               struct sgl_lib *lib,
                               struct sgl_sym *id,
                               sgl_int_t nargs) {
  sgl_def_init(&func->def, sgl, pos, lib, &SGL_DEF_FUNC, id);
  sgl_lset_init(&func->fimps, fimps_cmp);
  sgl_lset_init(&func->fimp_queue, fimp_queue_cmp);
  func->cemit_id = -1;
  func->nfimps = 0;
  func->nargs = nargs;
  func->nrets = -1;
  memset(func->rets, 0, sizeof(func->rets));
  return func;
}

struct sgl_func *sgl_func_deinit(struct sgl_func *func, struct sgl *sgl) {
  sgl_ls_do(&func->fimps.root, struct sgl_fimp, ls, f) {
    sgl_free(&sgl->fimp_pool, sgl_fimp_deinit(f, sgl));
  }
  
  return func;
}
                               
struct sgl_func *sgl_func_new(struct sgl *sgl,
                              struct sgl_pos *pos,
                              struct sgl_lib *lib,
                              struct sgl_sym *id,
                              sgl_int_t nargs) {
  struct sgl_ref_type *t = sgl_baseof(struct sgl_ref_type, type, sgl->Func);
  return sgl_func_init(sgl_malloc(&t->pool), sgl, pos, lib, id, nargs);
}

sgl_int_t sgl_func_cemit_id(struct sgl_func *f,
                            struct sgl *sgl,
                            struct sgl_pos *pos,
                            struct sgl_cemit *out) {
  if (f->cemit_id != -1) { return f->cemit_id; }
  sgl_int_t sym_id = sgl_sym_cemit_id(f->def.id, sgl, out);
  if (sym_id == -1) { return -1; }
  f->cemit_id = out->func_id++;

  sgl_cemit_line(out,
                 "struct sgl_func *func%" SGL_INT " = "
                 "sgl_get_func(sgl, sgl_pos_init(&p, %" SGL_INT ", %" SGL_INT "), "
                 "sym%" SGL_INT ");",
                 f->cemit_id, pos->row, pos->col, sym_id);

  sgl_cemit_line(out, "if (!func%" SGL_INT ") { return false; }", f->cemit_id);
  return f->cemit_id;
}

struct sgl_fimp *sgl_func_add_fimp(struct sgl_func *func,
                                   struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_arg args[],
                                   struct sgl_type *rets[]) { 
  struct sgl_sym *id = sgl_fimp_id(sgl, args, func->nargs);
  
  bool ok = false;
  struct sgl_ls *i = sgl_lset_find(&func->fimps, id, NULL, &ok);
  struct sgl_fimp *fimp = ok ? sgl_baseof(struct sgl_fimp, ls, i) : NULL;

  if (fimp) {
    sgl_error(sgl, pos,
              sgl_sprintf("Dup fimp: %s(%s)", func->def.id->id, fimp->id->id));
    
    return NULL;
  }
  
  fimp = sgl_fimp_new(sgl, pos, func, args, rets);
  sgl_ls_insert(i, &fimp->ls); 
  struct sgl_ls *qi = sgl_lset_find(&func->fimp_queue, &fimp->weight, NULL, NULL);
  sgl_ls_insert(qi, &fimp->queue_ls);

  if (func->nfimps) {
    func->nrets = sgl_min(func->nrets, fimp->nrets);

    for (struct sgl_type **src = fimp->rets, **dst = func->rets;
         src < fimp->rets + func->nrets;
         src++, dst++) { *dst = sgl_type_root(*dst, *src); }
  } else {
    func->nrets = fimp->nrets;

    for (struct sgl_type **src = fimp->rets, **dst = func->rets;
         src < fimp->rets + func->nrets;
         src++, dst++) { *dst = *src; }
  }
  
  func->nfimps++;
  return fimp;
}

struct sgl_fimp *sgl_func_get_fimp(struct sgl_func *func,
                                    struct sgl *sgl,
                                    struct sgl_pos *pos,
                                    struct sgl_sym *id) {
  bool ok = false;
  struct sgl_ls *i = sgl_lset_find(&func->fimps, id, NULL, &ok);

  if (!ok) {
    sgl_error(sgl, pos,
              sgl_sprintf("Unknown fimp: %s(%s)", func->def.id->id, id->id));
    
    return NULL;
  }

  return ok ? sgl_baseof(struct sgl_fimp, ls, i) : NULL;
}
                               
struct sgl_fimp *sgl_func_fimp(struct sgl_func *func) {
  struct sgl_ls *fs = &func->fimps.root;
  
  return (fs->next == fs || fs->next->next != fs)
    ? NULL
    : sgl_baseof(struct sgl_fimp, ls, fs->next);
}

struct sgl_fimp *sgl_func_dispatch(struct sgl_func *func,
                                   struct sgl *sgl,
                                   struct sgl_ls *stack) {
  sgl_ls_do(&func->fimp_queue.root, struct sgl_fimp, queue_ls, f) {
    if (sgl_fimp_match(f, sgl, stack)) { return f; }
  }

  return NULL;
}

struct sgl_fimp *sgl_func_dispatch_types(struct sgl_func *func,
                                         struct sgl *sgl,
                                         struct sgl_ls *stack) {
  sgl_ls_do(&func->fimp_queue.root, struct sgl_fimp, queue_ls, f) {  
    struct sgl_arg *a = f->args + func->nargs - 1;
    bool ok = true;
    
    for (struct sgl_ls *s = stack->prev;
         a >= f->args && s != stack;
         a--, s = s->prev) {
      struct sgl_val *av = a->val, *sv = sgl_baseof(struct sgl_val, ls, s);
      
      if (av) {
        if (sv->type == sgl->Undef) { return NULL; }

        if (!sgl_val_eq(av, sgl, sv)) {
          ok = false;
          break;
        }
        
        continue;      
      }
      
      struct sgl_type
        *at = a->type,
        *st = (sv->type == sgl->Undef) ? sv->as_type : sv->type;
      
      if (!sgl_derived(st, at)) {
        if (!sgl_derived(at, st)) {
          ok = false;
          break;
        }
        
        return NULL;
      }
    }

    if (ok) { return f; }
  }

  return NULL;  
}

struct sgl_op *_sgl_func_call(struct sgl_func *f,
                              struct sgl *sgl,
                              bool now,
                              struct sgl_val *args[]) {
  struct sgl_op *call_pc = sgl->pc;
  struct sgl_ls *s = sgl_stack(sgl);
  for (struct sgl_val **a = args; *a; a++) { sgl_val_dup(*a, sgl, s); }
  struct sgl_fimp *found = sgl_func_dispatch(f, sgl, s);

  if (!found) {
    sgl_error(sgl, sgl_pos(sgl),
              sgl_sprintf("Func not applicable: %s", f->def.id->id));
    
    return sgl->end_pc;
  }

  struct sgl_op *pc = sgl_fimp_call(found, sgl, sgl_pos(sgl), call_pc);
  if (!pc) { return sgl->end_pc; }
  if (!now || found->cimp) { return pc; }

  return sgl_run_task(sgl, sgl->task, pc, call_pc->next)
    ? call_pc->next
    : sgl->end_pc;
}

static void free_def(struct sgl_def *def, struct sgl *sgl) {
  struct sgl_ref_type *t = sgl_baseof(struct sgl_ref_type, type, sgl->Func);
  sgl_free(&t->pool, sgl_func_deinit(sgl_baseof(struct sgl_func, def, def), sgl));
}

void sgl_setup_funcs() {
  sgl_def_type_init(&SGL_DEF_FUNC, "func");
  SGL_DEF_FUNC.free_def = free_def;
}
