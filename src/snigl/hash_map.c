#include <assert.h>

#include "snigl/hash_map.h"
#include "snigl/pool.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"
#include "snigl/val.h"
#include "snigl/util.h"

static sgl_uint_t fn_imp(void *x) {
  return sgl_val_hash(x);
}

static bool is_imp(void *x, void *y) {
  return sgl_val_is(x, y);
}

struct sgl_hash_map *sgl_hash_map_new(struct sgl *sgl) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->HashMap);
  struct sgl_hash_map *m = sgl_malloc(&rt->pool);
  struct sgl_hash *h = &m->hash;
  sgl_hash_init(h);
  h->fn = fn_imp;
  h->is = is_imp;
  m->nrefs = 1;
  return m;
}

void sgl_hash_map_deref(struct sgl_hash_map *m, struct sgl *sgl) {
  assert(m->nrefs > 0);

  if (!--m->nrefs) {
    sgl_hash_slots_do(&m->hash, hs) {
      sgl_ls_do(hs, struct sgl_hash_node, ls, n) {
        sgl_val_free(n->key, sgl);
        sgl_val_free(n->val, sgl);
      }
    }
    
    sgl_hash_deinit(&m->hash, sgl);
    struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->HashMap);
    sgl_free(&rt->pool, m);
  }
}

bool sgl_hash_map_put(struct sgl_hash_map *m,
                      struct sgl *sgl,
                      struct sgl_val *key,
                      struct sgl_val *val) {
  struct sgl_hash *h = &m->hash;
  bool is_empty = sgl_hash_init_slots(h, sgl);
  sgl_uint_t hk = sgl_hash_key(h, key);
  struct sgl_ls *hs = sgl_hash_slot(h, hk);

  if (!is_empty) {
    struct sgl_hash_node *hn = sgl_hash_find(h, hs, hk, key);

    if (hn) {
      sgl_val_free(key, sgl);
      sgl_val_free(hn->val, sgl);
      hn->val = val;
      return false;
    }
  }

  sgl_hash_add(h, sgl, hs, hk, key, val);
  return true;
}
