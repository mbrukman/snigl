#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "snigl/buf.h"
#include "snigl/cemit.h"
#include "snigl/list.h"
#include "snigl/repl.h"
#include "snigl/sgl.h"
#include "snigl/str.h"
#include "snigl/timer.h"

enum sgl_mode { SGL_MODE_COMPILE,
                SGL_MODE_DEFAULT,
                SGL_MODE_REPL,
                SGL_MODE_RUN };

struct hist_node {
  const char *type;
  sgl_int_t count;
};

static enum sgl_cmp hist_type_cmp(const void *lhs, const void *rhs, void *_) {
  char *const *l = lhs, *const *r = rhs;
  return sgl_cs_cmp(*l, *r);
}

static int hist_count_cmp(const void *lhs, const void *rhs) {
  const struct hist_node *l = rhs, *r = lhs;
  if (l->count < r->count) { return -1; }
  return (l->count > r->count) ? 1 : 0;
}

int main(int argc, const char *argv[]) {
  sgl_setup();
  struct sgl sgl;
  sgl_init(&sgl);
  
  if (!sgl_use(&sgl, &SGL_NULL_POS, sgl.abc_lib, NULL)) {
    sgl_dump_errors(&sgl, stderr);
    return -1;
  }
          
  enum sgl_mode mode = SGL_MODE_DEFAULT;
  if (argc == 1) { goto exit; }
  
  sgl_int_t i = 1;
  const char *m = argv[i];
  
  if (strcmp(m, "compile") == 0) {
    mode = SGL_MODE_COMPILE;
    i++;
  } else if (strcmp(m, "repl") == 0) {
    mode = SGL_MODE_REPL;
    i++;
  } else if (strcmp(m, "run") == 0) {
    mode = SGL_MODE_RUN;
    i++;
  }

  if (mode == SGL_MODE_DEFAULT) { mode = SGL_MODE_RUN; } 
  bool cemit = false;
  
  while (*argv[i] == '-') {
    const char *a = argv[i++];
    
    if (strcmp(a, "--trace") == 0) {
      sgl.trace = strtoimax(argv[i++], NULL, 10);
    } else if (strcmp(a, "--cemit") == 0) {
      cemit = true;
    } else {
      printf("Error: Invalid flag: %s\n", a);
      return -1;
    }
  }
  
  if (i == argc) {
    puts("Error: Missing file");
    return -1;
  }

  sgl_init_args(&sgl, argc - i - 1, argv + i + 1);
  
  if (!sgl_load(&sgl, &SGL_NULL_POS, sgl_strdup(argv[i]))) {
    sgl_dump_errors(&sgl, stderr);
    return -1;
  }

  sgl_int_t trace_rounds = 0, trace_ms = 0;
  
  if (sgl.trace) {
    struct sgl_timer t;
    sgl_timer_init(&t);
    trace_rounds = sgl_trace(&sgl, sgl.beg_pc->next);
    trace_ms = sgl_timer_ms(&t);

    if (trace_rounds == -1) {
      sgl_dump_errors(&sgl, stderr);
      return -1;
    }
  }
  
  if (mode == SGL_MODE_RUN) {
    sgl_run(&sgl, sgl.beg_pc->next);
    sgl_dump_errors(&sgl, stderr);
  } else if (mode == SGL_MODE_COMPILE) {
    if (cemit) {
      struct sgl_cemit out;
      sgl_cemit_init(&out);
      sgl_cemit(&sgl, sgl.beg_pc->next, &out);
      fwrite(sgl_buf_cs(&out.buf), 1, out.buf.len, stdout);
      sgl_cemit_deinit(&out);    
      sgl_dump_errors(&sgl, stderr);
    } else {
      sgl_dump_ops(&sgl, stdout);
      
      struct sgl_vset hist;
      sgl_vset_init(&hist,
                    sizeof(struct hist_node),
                    hist_type_cmp,
                    offsetof(struct hist_node, type));

      sgl_int_t op_count = 0;
      sgl_ls_do(&sgl.ops, struct sgl_op, ls, op) {
        const char *tid = op->type->id;
        bool ok = false;
        sgl_int_t i = sgl_vset_find(&hist, &tid, NULL, &ok);

        if (ok) {
          struct hist_node *n = sgl_vec_get(&hist.vals, i);
          n->count++;
        } else {
          struct hist_node *n = sgl_vec_insert(&hist.vals, i);
          n->type = tid;
          n->count = 1;          
        }

        op_count++;
      }

      fputc('\n', stdout);
      
      qsort(sgl_vec_beg(&hist.vals),
            hist.vals.len,
            hist.vals.val_size,
            hist_count_cmp);
      
      sgl_vset_do(&hist, struct hist_node, n) {
        printf("%-15s %4" SGL_INT " %3" SGL_INT "%%\n",
               n->type, n->count,
               (sgl_int_t)((n->count /
                            (double)sgl_max(1, (n->count + op_count))) * 100));
      }

      sgl_vset_deinit(&hist);

      if (sgl.trace) {
        printf("\nTraced %" SGL_INT " rounds in %" SGL_INT "ms\n\n",
               trace_rounds, trace_ms);
      }
    }
  }
 exit:
  if (mode == SGL_MODE_DEFAULT || mode == SGL_MODE_REPL) {
    sgl_repl(&sgl, stdin, stdout);
  }
  
  sgl_deinit(&sgl);
  return 0;
}
