## No Comments
2019-01-16, 21:58 UTC

### Intro
One of the things that sets Snigl apart from most other languages is that it doesn't support code comments. The reasons boil down to separation of concerns. Despite not being very good at anything, comments are commonly used for several purposes: to temporarily hide code from the compiler, for documentation, tests etc. There are various more or less half baked approaches out there that introduce separate syntaxes inside comments, but precious few attempts at solving actual problems using first class language features.

### Code
Putting code into comments for whatever reason comes with its share of issues. To begin with, the code can't contain anything that looks like a comment. Secondly; the comment usually has no idea that it contains code, which means that you get no assistance with processing and maintaining it.

Snigl supports exempting expressions from compilation and evaluation using `_:`. The code is parsed to find the end, and as a consequence required to be syntactically correct; but has no effect on the system.

```
  'foo _:("Launching missiles" say) 'bar

['foo 'bar]
```

Line comments don't make much sense in denser languages like Snigl, `_:` may be combined with a bare string literal to get the same level of convenience without enforcing line breaks.

```
  'foo _:"TODO: Add 'bar already" 'baz
  
['foo 'baz]
```

### Documentation
Documentation may be appended to definitions using `doc:`. The expression may contain arbitrary code, and the contents of the stack are converted to string and concatenated after evaluation. `source:` evaluates the expression and returns its result followed by source code as string. `rotr` rotates the result to the top of the stack, past source code and newline; where `%_` picks it up for interpolation.

```
  func: my-add(42 T) drop
  func: my-add(T 42) (swap drop)
  func: my-add(T T) +

  doc: my-add (
    "Adds arguments and returns the result; except when any argument is 42, "
    "in which case it returns 42.\n\n"
    
    "Examples:\n"
    "  " source: (3 4 my-add)\n rotr
    "[%_]\n\n"
    "  " source: (42 7 my-add)\n rotr
    "[%_]"
  )
```

`docs` returns documentation for a definition as string.

```
  'my-add docs say

Adds arguments and returns the result; except when any argument is 42, in which case it returns 42.

Examples:
  3 4 my-add
[7]

  42 7 my-add
[42]
```

### Literate Style
Inverting the previous example leads to a more literate style.

```
  doc: _ "\
    %(source:,
      func: my-add(42 T) drop
      func: my-add(T 42) (swap drop)
      func: my-add(T T) +
    )

    Adds arguments and returns the result; except when any argument is 42,
    in which case it returns 42.
        
    Examples:
      %(source:, 3 4 my-add)
    [%()]\n
      %(source:, 42 7 my-add)
    [%()]"
```
```
  nil docs say
 
func: my-add(42 T) drop
func: my-add(T 42) (swap drop)
func: my-add(T T) +

Adds arguments and returns the result; except when any argument is 42,
in which case it returns 42.
        
Examples:
  3 4 my-add
[7]

  42 7 my-add
[42]
```

eof